---
has_dir: false
has_content: false
has_categories: false
has_tags: false
has_slug: false
has_permalink: false
has_layout: false
has_date: false
has_title: false
pre:
  "IMPORTANTE LEER ANTES DE INICIAR EL REGISTRO DE TU PELÍCULA:
  \n\n
  El Catálogo de Documental Mexicano funciona como un sitio interactivo en el que las realizadoras/realizadores o productoras/ productores son los que registran sus propias películas. 
  \n\n
Para ello, debes llenar la siguiente ficha de registro. Algunos de los campos de la ficha son obligatorios (marcados con un asterisco) y otros no. Sin embargo, los invitamos a llenar los campos que no son obligatorios ya que nos ofrecen información necesaria para diagnosticar las condiciones de producción y difusión del documental en México y nos permite ofrecer un mejor servicio. 
  \n\n
El Catálogo permite realizar búsquedas de los documentales por categorías. Entre más precisas sean las categorías ingresadas en la ficha, más fácil se podrá encontrar tu documental. 
  \n\n
Algunos de estos campos ofrecen opciones predeteminadas. Te pedimos que las leas con cuidado para clasificar tu documental. 
  \n\n
En otros casos, tú debes teclear la información, pero el sistema te ofrecerá también las opciones que han escrito otros usuarios. En medida de lo posible, usa los mismos parámetros para que las búsquedas sean iguales.
  \n\n
Es importante que cuides la ortografía y escribas los nombres propios de una sola manera ya que de lo contrario generarás demasiadas opciones u opciones inexistentes de búsqueda (Por ejemplo: si tecleas “yucatan” con mínúscula o sin acento, se creará esa opción de búsqueda y cuando alguien busque un documental de Yucatán, tu documental no aparecerá). 
  \n\n
En todos los casos, cuando ingreses un dato debes hacer ENTER para que el sistema reconozca la información.
  \n\n
Para quitar algúna opción equivocada, haz clic en la × que aparece del lado iquierdo de los parámetros ingresados. 
  \n\n
Se te solicitará el póster en posición vertical y una resolución míima de 600 x 800 píxeles (ancho por alto). También deberás subir al menos 3 stills de tu película con una resolución mínima de 1024 x 768 pixeles (ancho x alto), por lo que te sugerimos tenerlos a la mano. 
  \n\n
También se te solicitarán datos de contacto, los cuales no estarán visibles al público, pero nos permitirá comunicarnos contigo en caso de que alguien esté interesado en programar tu película.
  \n\n
Si no concluyes el llenado de todos los campos obligatorios de la ficha de registro, puedes guardar la información haciendo clic en el botón GUARDAR PARA DESPUÉS.
  \n\n
Una vez concluido el llenado de la ficha de registro, debes hacer clic en el botón ENVIAR. La información será revisada por nuestro equipo técnico, y una vez aprobada, tu película se publicará en el Catálogo de Documental Mexcano. "
post:
nombre_completo:
  title: '1. Datos personales'
  subtitle: 'Datos personales del usuario quien está haciendo el registro de la película.'
  label: 'Nombre completo'
  required: true
  value: string
  public: false
pais_de_la_persona:
  label: País
  required: true
  value: ['posts/pais_de_la_persona']
  multiple: false
  public: false
estado_de_la_persona:
  label: Estado de la República Mexicana
  required: true
  value: ['posts/estado_de_la_persona']
  multiple: false
  public: false
municipio_de_la_persona:
  label: Municipio
  required: true
  value: ['posts/municipio_de_la_persona']
  multiple: false
  public: false
consentimiento:
  label: "Acepto [políticas de privacidad y términos de uso](https://documentalmexicano.org/politicas/)"
  required: true
  value: false
  public: false
title:
  title: '2. Ficha Técnica'
  label: 'Título en español'
  required: true
  value: string
titulo_en_ingles:
  label: "Título en inglés"
  required: false
  value: string
poster:
  label: 'Póster'
  help: "La imágen debe tener una posición vertical y una resolución mínima de 600x800 píxeles (ancho x alto)"
  required: true
  value: image
stills:
  help: "Capturas representativas del documental, para mostrar en la portada de la ficha.  Al menos 3 imágenes, en posición horizontal y resolución mínima de 1024x768 píxeles (ancho x alto)."
  required: true
  multiple: true
  value: [image]
  min: 3
sitio_web_de_la_pelicula:
  label: 'Sitio web de la película'
  required: false
  public: true
  value: url
pais:
  label: País
  help: País donde radica la casa productora o la persona que tiene los derechos patrimoniales del documental.
  required: true
  value: ['posts/pais']
  multiple: false
estado:
  help: Estado de la República Mexicana donde radica la casa productora o la persona que tiene los derechos patrimoniales del documental.
  required: pais == 'México'
  value: ['posts/estado']
  multiple: false
municipio:
  help: Municipio donde radica la casa productora o la persona que tiene los derechos patrimoniales del documental.
  required: pais == 'México'
  value: ['posts/municipio']
  multiple: false
año_de_inicio_de_la_produccion:
  label: Año de inicio de la producción
  required: true
  public: false
  value: year
  min: 1900
año:
  label: Año
  required: true
  value: year
  min: 1900
  help: Año del estreno o finalización del documental
duracion:
  label: 'Duración'
  help: Minutos
  required: true
  value: number
idioma_original:
  required: true
  value: ['posts/idioma_original']
  multiple: true
idiomas_de_los_subtitulos:
  label: 'Idiomas de los subtítulos'
  required: true
  open: true
  public: true
  value:
    - posts/idiomas_de_los_subtitulos
    - "Sin subtítulos"
    - "Español"
    - "Inglés"
    - "Francés"
formatos:
  label: Formatos en el que la obra se encuentra disponible para su proyección
  required: true
  open: true
  public: true
  value:
    - posts/formatos
    - DCP
    - Blu Ray
    - DVD
    - Archivo digital
tipo_de_produccion:
  label: Tipo de producción
  help: Instancia que coordina la producción de la película (al margen de los inversionistas, coproductores o patrocinadores de la misma).
  required: true
  public: false
  multiple: true
  open: true
  value:
    - posts/tipo_de_produccion
    - "Estudiantil: Producido en el marco de una institución educativa, escuela o universidad"
    - "Independiente: Producido por una casa productora independiente constituida o no legalmente"
    - "Institucional: Producido por una institución pública o privada"
    - "Imcine: Producido por IMCINE"
    - "Televisivo: Producido por un canal televisivo"
    - "Comunitario: Producido por alguna comunidad, pueblo, colectivo social"
tematicas:
  label: 'Temáticas'
  required: true
  public: true
  open: true
  value:
    - posts/tematicas
    - "Medio Ambiente: Naturaleza, ecología, desarrollo sustentable"
    - "Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan generar un cambio social"
    - "Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes, situación actual"
    - "Género y sexualidad: Identidad, diversidad, prácticas y cuidados de la sexualidad"
    - "Musical: Artistas, agrupaciones, giras, fenómenos musicales"
    - "Biográfico e histórico: Sucesos y personajes históricos"
    - "Ciencia y tecnología: Descubrimientos y avances científicos, tecnológicos o médicos"
    - "Arte: Artes escénicas, plásticas, visuales, multimedia"
    - "Cultura: Tradiciones, patrimonio"
    - "Derechos humanos: Luchas o debates sobre los derechos humanos"
    - "Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes"
    - "Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales y simbólicas que enfrentan"
    - "Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio público y privado"
  min: 1
  max: 3
palabras_clave:
  help: Palabras que describan las temáticas más relevantes del documental y que faciliten su búsqueda
  required: true
  public: true
  open: true
  value:
    - posts/palabras_clave
    - Bosque de la Primavera
    - Música Tradicional
    - Territorio
    - Identidad
    - Zapatismo
    - Agua
    - Paisaje Sonoro
    - Transgénero
  max: 6
genero_y_estilo:
  label: Género y estilo
  required: true
  open: true
  public: true
  min: 1
  max: 3
  value:
    - posts/genero_y_estilo
    - "Ensayo / reflexivo: Narración reflexiva en primera persona"
    - "Etnográfico: Descripción fílmica del comportamiento humano."
    - "Found footage / Reapropiación: Utiliza material de archivo y lo resignifica para contar una nueva historia"
    - "Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción del mismo para proponer nuevas formas fílmicas"
    - "Autorreferencial / Diario filmado: Cuenta una historia a partir de la vida o experiencia propia."
    - "Road movie: Documentales de viaje y en movimiento"
    - "Observacional: Documentales que observan el mundo con la mínima intervención del cineasta"
    - "De autor /De creación: Su apuesta se basa en la mirada personal del autor."
    - "Transmedia / Doc Web: Su narrativa está diseñada para la web y/o diversas plataformas mediáticas, es interactivo y no es lineal."
    - "Colaborativo: Propicia procesos de producción en los que participan los personajes del documental."
    - "Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática."
    - "De investigación: El hilo conductor se sustenta en la investigación de un tema."
    - "Falso documental: Película que documenta una falsa hipótesis."
    - "Documental sensorial: Apuesta por la transmisión de experiencias subjetivas a nivel estético y sensorial."
locaciones:
  help: "Países y/o estado(s) de la República en donde se realizó la grabación"
  required: false
  public: false
  value:
    paises:
      label: Países
      required: true
      public: false
      value: ['posts/locaciones/paises']
    estados:
      estados: Estados de la República
      required: true
      public: false
      value: ['posts/locaciones/estados']
sinopsis_espaniol:
  label: Sinopsis en español
  required: true
  value: text
  max: 500
sinopsis_ingles:
  label: Sinopsis en inglés
  required: false
  value: text
  max: 500
link_al_trailer:
  required: false
  value: url
link_para_visionado:
  required: true
  value: url
link_para_visionado_publico:
  label: 'Link para visionado público'
  value: false
contrasena_para_visionado:
  label: 'Contraseña para visionado'
  required: false
  public: false
  value: password
festivales:
  label: "Festivales y muestras en los que ha participado la obra, con año o edición"
  required: false
  value:
    - festival:
        value: ['posts/festivales/festival']
        multiple: false
      año_de_edicion:
        label: 'Año o edición'
        value: string
premios_y_reconocimientos:
  label: "Premios y reconocimientos que ha recibido dentro y fuera de festivales"
  required: false
  value:
    - reconocimiento:
        value: ['posts/premios_y_reconocimientos/reconocimiento']
        multiple: false
      institución:
        value: ['posts/premios_y_reconocimientos/entidad']
        multiple: false
becas_y_apoyos:
  label: "Becas y/o apoyos recibidos para la realización del documental por parte de alguna institución o particular"
  required: false
  public: false
  value:
    - beca:
        value: ['posts/becas_y_apoyos/beca']
        multiple: false
        required: false
        public: false
      monto:
        value: number
        required: false
        public: false
      institución:
        value: ['posts/becas_y_apoyos/entidad']
        multiple: false
        required: false
        public: false
      tipo:
        value: ['Pública','Privada']
        required: false
        multiple: false
        public: false
equipo_de_produccion:
  title: '3. Equipo de producción'
  label: 'Equipo de producción'
  value:
    direccion:
      label: 'Dirección'
      required: true
      public: true
      value: ['posts/direccion']
    produccion:
      label: 'Producción'
      required: true
      public: true
      value: ['posts/produccion']
    casa_productora:
      required: false
      public: true
      value: ['posts/casa_productora']
    guion:
      label: 'Guión'
      required: true
      public: true
      value: ['posts/guion']
    investigacion:
      label: 'Investigación'
      required: false
      public: true
      value: ['posts/investigacion']
    fotografia:
      label: 'Fotografía'
      required: true
      public: true
      value: ['posts/fotografia']
    sonido:
      required: true
      public: true
      value: ['posts/sonido']
    edicion:
      label: 'Edición'
      required: true
      public: true
      value: ['posts/edicion']
    musica_original:
      label: 'Música original'
      required: false
      public: true
      value: ['posts/musica_original']
    otros:
      required: false
      public: true
      value:
        -
          nombre:
            required: false
            public: true
            value: ['posts/otros/nombre']
            multiple: false
          rol:
            required: false
            public: true
            value: ['posts/otros/rol']
            multiple: false
directorxs:
  title: '4. DATOS DEL LAS DIRECTORAS / DIRECTORES Y LAS PRODUCTORAS / PRODUCTORES'
  label: 'Director(a)'
  help: 'Si director(a) y productor(a) son la misma persona, selecciona ambos roles en el campo "Rol"'
  required: true
  public: true
  value:
    - rol:
        required: true
        value: ['Productor(a)','Director(a)']
        checkbox: true
      nombre:
        required: true
        public: true
        value: string
      nacionalidad:
        required: true
        public: false
        value: ['posts/directorxs/nacionalidad']
        multiple: false
      fecha_de_nacimiento:
        required: false
        public: false
        value: date
      telefono_fijo:
        label: 'Teléfono fijo'
        required: false
        public: false
        value: []
      telefono_celular:
        label: 'Teléfono celular'
        required: false
        public: false
        value: []
      correo_electronico:
        label: 'Correo electrónico'
        value: email
        required: true
      correo_electronico_publico:
        label: 'Correo electrónico público'
        value: false
      grados_academicos:
        label: 'Grados académicos'
        required: false
        public: false
        align: end
        value:
          -
            grado_academico:
              label: 'Grado académico'
              help: 'Especificar la disciplina, ej. Licenciatura en Antropología'
              value: ['posts/directorxs/grados_academicos/grado_academico']
              required: false
              public: false
              multiple: false
            escuela:
              value: ['posts/directorxs/grados_academicos/escuela']
              required: false
              public: false
              multiple: false
distribuidoras:
  title: '5. Datos de la distribuidora y de contacto para la distribución'
  public: true
  required: false
  value:
    - en_mexico:
        label: 'En México'
        required: false
        value: ['posts/distribuidoras/en_mexico']
        multiple: false
      otras_regiones:
        required: false
        value: ['posts/distribuidoras/otras_regiones']
        multiple: false
      nombre:
        required: false
        value: ['posts/distribuidoras/nombre']
        multiple: false
      nombre_publico:
        label: 'Nombre público'
        value: false
      telefono:
        label: 'Teléfono'
        public: false
        required: false
        value: string
      correo_electronico:
        label: 'Correo electrónico'
        required: false
        public: false
        value: email
      sitio_web:
        required: false
        value: url
      sitio_web_publico:
        value: false
        required: false
        label: 'Sitio web público'
datos_de_contacto:
  label: Datos de contacto para programar la película
  required: true
  public: true
  max: 2
  value:
    - nombre:
        required: true
        public: true
        value: string
      telefono:
        label: 'Teléfono de contacto'
        public: true
        required: true
        value: string
      correo_electronico:
        label: 'Mail de contacto'
        required: true
        public: true
        value: email
      sitio_web:
        required: false
        public: true
        value: url
ventanas_donde_se_ha_exhibido:
  label: "¿En qué ventanas se ha exhibido el documental?"
  public: false
  open: true
  value:
    - posts/ventanas_donde_se_ha_exhibido
    - "Salas de cine comercial"
    - "Salas de cine culturales y/o independientes"
    - "Centros culturales y cineclubes"
    - "Televisión comercial abierta"
    - "Televisión comercial de paga"
    - "Televisión cultural"
    - "Video On Demand (VOD)"
    - "Festivales / Muestras mexicanas"
    - "Festivales / Muestras internacionales"
    - "Plataformas digitales comerciales"
    - "Plataformas digitales gratuitas"
    - "Derivadas (aerolíneas, hoteles, etc.)"
    - "Escuelas y universidades"
licencia_de_uso:
  label: "Puedes escoger alguna de las siguientes combinaciones. Si quieres saber más sobre las licencias Creative Commons: https://creativecommons.org/licenses/?lang=es y https://www.youtube.com/watch?v=ZLP1wSkhg3Y&t=21s"
  multiple: false
  required: false
  public: true
  radio: true
  value:
    - "No: El documental sólo se puede reproducir, distribuir y modificar con permiso expreso."
    - "Atribución (CC/BY), permite compartir, explotar y transformar la obra siempre y cuando se crédito al autor original."
    - "Atribución / Compartir Igual (CC/BY/SA), permite compartir, explotar y transformar la obra siempre y cuando se de crédito al autor original y las obras derivadas tengan la misma licencia que la original."
    - "Atribución / No derivadas. (CC/BY/ND), permite compartir y explotar la obra, siempre y cuando no se transforme y se de crédito al autor original"
    - "Atribución /No comercial(CC/BY/NC), permite compartir la obra sin siempre y cuando no sea con fines de lucro y se de crédito al autor original "
    - "Atribución / No comercial / Compartir igual (CC/BY/NC/SA), permite compartir y transformar la obra sin siempre y cuando no sea con fines comerciales, se de crédito al autor original y las obras derivadas se tengan la misma licencia."
    - "Atribución / No comercial / No Derivadas (CC/BY/NC/ND), permite compartir la obra sin siempre y cuando no sea con fines comerciales, no se transforme y se de crédito al autor original."
restricciones_de_distribucion:
  label: "¿Qué restricciones de distribución tiene la película para ser
    programada en muestras, televisiones públicas, cineclubes, centros
    culturales, universidades? En todos los casos, serán exhibiciones sin
    fines de lucro."
  required: true
  public: true
  multiple: false
  value:
    - "Bajo los términos de la licencia anterior"
    - "Ninguna restricción: se puede programar, siempre y cuando se dé
      aviso al productor o director"
    - "Totalmente restringida: no se puede programar en ningún caso, ya que
      se tienen compromisos de distribución adquiridos"
    - "Contactar al productor para revisar cada caso (se puede programar en
      ciertos casos)"
financiacion:
  title: '6. Datos estadísticos sobre la financiación'
  label: "Costo total del proyecto (si no se tiene el dato exacto, poner un aproximado, procurar incluir en el cálculo el costo de la inversión propia)"
  required: true
  public: false
  multiple: false
  value:
    - "De 0-10 mil pesos"
    - "De 10-30 mil pesos"
    - "De 30-50 mil pesos"
    - "De 50-100 mil pesos"
    - "De 100-300 mil pesos"
    - "De 300-500 mil pesos"
    - "De 500 mil-1 millón de pesos"
    - "De 1-2 millones de pesos"
    - "De 2-3 millones de pesos"
    - "De 3-5 millones de pesos"
    - "De 5-10 millones de pesos"
    - "Más de 10 millones de pesos"
coproductores:
  required: false
  public: false
  value:
    -
      coproductor:
        value: string
        public: false
      pais:
        value: ['posts/coproductores/pais']
        multiple: false
        public: false
        label: 'País'
      estado:
        label: Estado de la República
        value: ['posts/coproductores/estado']
        multiple: false
        public: false
porcentaje_de_financiacion:
  label: 'Porcentaje de financiación'
  help: 'Establece la proporción de financiamiento según fuente, el total debe ser el 100%'
  required: false
  public: false
  cols: 3
  align: end
  value:
    estatal:
      label: "Financiación pública"
      help: "Fondos (en efectivo o en especie) del
      gobierno e instituciones públicas, ya sea federales, estatales o
      municipales, tales como Imcine, Conaculta, Fonca, Secretarías de
      cultura de los estados, universidades públicas, televisoras
      públicas, etc."
      required: false
      public: false
      value: number
      max: 100
      min: 0
    privada:
      label: "Financiación privada"
      help: "Fondos (en efectivo o en especie)
      provenientes de empresas o patrocinios privados, tales como
      productoras independientes, universidades privadas, crowdfounding,
      personas físicas, etc."
      required: false
      public: false
      value: number
      max: 100
      min: 0
    propia:
      label: "Financiación propia"
      help: "Fondos (en efectivo o en especie) que
      invirtieron los miembros del propio equipo de producción de la
      película."
      required: false
      public: false
      value: number
      max: 100
      min: 0
recaudacion:
  label: Recaudación económica
  help: "¿Cuánto dinero ha recaudado la película por distribución y exhibición? (Taquilla / Derechos de antena / DVD / Plataformas web)"
  required: false
  public: false
  multiple: false
  value:
    - "De 0 a mil pesos"
    - "De 1-3 mil pesos"
    - "De 3-5 mil pesos"
    - "De 5-10 mil pesos"
    - "De 10-30 mil pesos"
    - "De 30-50 mil pesos"
    - "De 50-100 mil pesos"
---
