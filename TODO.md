https://docs.google.com/document/d/1dQnFy9OVEg1Ggv35eokXiS4iQHLc5itFt9S8bm6fFPo/edit


# VERIFICAR Y NO QUEDÓ

* [no quedó] Formulario de noticias: incluir 2 cajas de texto para descripción y excerpt y especificarlas con un titulo
// No se entiende el formulario para crear y editar noticias. Aparece dos veces el campo título (al principio y al final) y el parámetro "cuerpo del artículo" debe decir "descripción" o "descripción completa". 


* [PENDIENTE] SECCION MORADO FICHA TECNICA
Ficha técnica ya es una sección aparte con fondo morado
Ficha técnica tiene este orden de parámetros:

Incluir " Sitio web de la pelicula " // link en blanco


* [no quedó] Conjugación de género en ficha
Coproductores » Coproductoras/es
Coproductor(a) 


# Para el 30 de mayo

* [30 de mayo] Usuarixs no deberian poder subir noticias
* [para el 30 de mayo] Opción de reestablecer contraseña
* [para el 30 de mayo] Admins no pueden eliminar posts.
* [para el 30 de mayo] Admins no pueden ver lista de usuarixs.
* [para el 30 de mayo] Usuarixs no deberían ver noticias como opción de post
* [para el 30 de mayo] resolver bug de que se ve la caja de texto después del título de la ficha de documental
* [para el 30 de mayo] Usuarias no pueden eliminar sus posts
* Que el mensaje de compilación finalizada llegue a todas las admins y que sea más claro el mensaje.


# Pendientes de afra y yaizu

* [NO QUEDÓ] Siempre debería cargar algun thumb en los cards
// PENDIENTE QUE ACTUALICEN IMÁGENES


* [PENDIENTE] Cambiar mensaje a la hora de crear cuenta donde te dice que te llegará un correo de confirmación -- pendiente que lo envíe Afra

* [NECESITAMOS TEXTO DE ELLAS] El primer mensaje de migas de pan lo vamos a cambiar. --- pendiente que nos lo envíen
* [no consigo replicar] Cuando escribes una palabra que comienza en mayusc, al dar enter te lo coloca en minuscula. Si le das un espacio después, si se guarda con mayuscula. Eso está mal así.
* [no consigo replicar] Faltan muchos acentos en los parámetros de llenado de la ficha

* [] Código hexagonal del naranja del logo


# Hecho
* [HECHO] Formulario de contacto » texto en negro
* [HECHO] En celular el fondo del menú desplegable es rojo en vez de magenta
* [HECHO] En celular, en pie de pagína: salen los logos sin centrar, los links salen justificados y queda feo
* [HECHO] Quitar de la ficha que haya teléfono y correo público. Aparece en la ficha, pero no es info pública. Incluir textito diciendo: 'si quieres contactar con este documental, envía un correo a catalogo@documentalmexicano.org'
// siguen apareciendo los checkboxes
// en la ficha dice info privada pero debería decir lo que aparece en el issue

* [HECHO] Layout de noticia individual:
imagen destacada
titulo h1
excerpt en rosa
fecha en rosa más pequeño p small
descripción

* [HECHO] Hamburger más grande y en magenta. 
* [ARREGLADO] En iphone parece que no carga menú.

* [ARREGLADO] En celular tiene que verse carta por vez en Chrome y Opera y Safari y en Firefox. Tambien necesitan margenes.
* [ARREGLADO] En celular el fondo se corta en Chrome y Opera y Safari 
* [ARREGLADO] En celular Android con Chrome y Firefox klr, no carga fondo y no cargan algunos thumbs. Véase signal convo con Omar.
* [ARREGLADO] En celular iphone no carga fondo y no cargan algunos thumbs. Véase signal convo con Mia.




//////////////////////////

# HECHO



* [hecho] Sustituir fondo de login de sutty

* [hecho] Cuenta de administradoras

* [hecho] Al crear una ficha nueva, no te carga el formulario desde el principio. Bug de focus.

* [hecho] Para usuarixs, el botón de guardar » enviar 


* [hecho] Texto explicativo en datos personales para explicar de qué son estos datos y si van a salir en la ficha
// Debajo del título "Datos personales" en el formulario agregamos un texto que dice " Estos datos personales los pedimos para dar seguimiento a quién creó la ficha. "


* [hecho] Acepto políticas de privacidad, debería salir link. Está en markdown.


* [hecho] En Ficha Técnica, primer parámetro debería ser título en castellano

* [hecho] Al guardar para después, lo envía a las admins también y te sale el mensaje que ya lo completaste cuando no lo completaste. No hay forma de distinguir entre primera y segunda versión de borrador. 

* [hecho] Cuando no colocas un título en español, no te lo guarda y te recarga la página en blanco en la zona del primer editor de texto, así que no ves el mensaje de error. Cambiar mensaje de error a algo más comprensible y que redireccione al listado de posts como está.	
// ahora te recarga la ficha y te muestra una indicación en rojo de campo obligatorio



* [hecho] Logo en quienes somos centrado
* [hecho] Seccion Sobre el catalogo, misma indicacion de encabezado. Título con acento: CATÁLOGO
// idem issue para f
* [hecho]  Checar que el formulario llegue al correo contacto@documentalme...

* [hecho] En seccion quienes somos
    [hecho] poner acento: QUIÉNES SOMOS // véase issue para f

* [hecho] thumb_default.png del logo de docred se ve cortado en cards de las noticias // edité la imagen, pero no sé si se va a ver

* [hecho] logo de docred al 80% en quiénes somos


* [hecho] Buscador
  * [hecho] Más angosto el input group y el div filters: al 80% //le meti "%article.container-700" al principio de todo
  * [hecho] Frase los resultados se mostrarán centrado y "No se encontró ningún documental para esta búsqueda" CENTRADO

 

* [hecho] En fichas de documentales
  * [hecho] caja de datos a la izquierda // le puse padding left 50px en vez de right
  * [hecho] Menos line height entre elementos de metadatos // le puse a texto-slider-claro menos margin top y bottom para sobre-escribir el margin de los headers
  * [hecho] Título en mayúscula // class titulo slider con propiedad   text-transform: uppercase;
  * [hecho] table th padding 0.1rem


* [hecho] No aparecen acentos en los títulos: Quiénes somos.

* [hecho]  Aceptar las polìticas de privacidad está mal: que salga toda la frase con hipervinculo con target blank
* [hecho] datos en tarjeta: título, fecha, resumen, LEER más, sin categorias // agregué un nuevo haml en _includes, pero no sé bien cómo hacer esto
* [hecho] En cards de cortos y largos y también en Estados de la página de inicio, sólo dejar título para que no salga repetido


* [hecho] En página de categoria mostrar subtitulo más pequeño

https://documentalmexicano.org/categoria/pueblos-ind%C3%ADgenas-pueblos-originarios-de-m%C3%A9xico-sus-tradiciones-lengua-saberes-situaci%C3%B3n-actual/


* [hecho] SECCION VERDE SINOPSIS
Que aparezca primero la sinopsis en español. No aparece ahora.
Texto en blanco

* [HECHO] SECCION ROSA

-CONTACTO PARA LA EXHIBICIÓN (DATOS DE CONTACTO)
-DISTRIBUCIÓN (EN CASO DE QUE HUBIERA ALGÚNA DISTRIBUIDORA (SI NO HAY DISTRIBUIDORA QUE NO APAREZCA) 
 
En México
En otras regiones del mundo

- LICENCIA Y RESTRICCION
En restricción quitar "Esta es la forma en que lxs realizadorxs permiten que se reproduzca el documental"
En licencia, solo sale el nombre de la licencia.


* [hecho] Sección PELIS RELACIONADAS
[hecho] sin franja de color 
[hecho] titulo en mayusculas y rosa

* [hecho] Incluir en formulario, en punto 5 de distribución opciones de distribución en méxico y en otras regiones


* [hecho] Al entrar en sesión, da error de Hubo un error al iniciar la sesion. ¿Escribiste bien las credenciales? -- esto pasa con la cuenta 
catalogo@documentalmexicano.org con contrasña catalogo, tal cual les enviaste los credenciales a lxs chicxs por correo
// ya no se va a entrar con esta cuenta sino 'admin@doc...' lo que "resuelve el problema"

* [hecho] Admins no pueden aprobar posts.
// lo hacen haciendo clic en 'guardar' al editar el post. véase documentación

* [hecho] Datos personales van señalados
* [hecho] Sinopsis en español e inglés van después de datos personales




////
## N


* [] Recomendaciones de tamaños de archivos
* [x] Integración de Matomo
* [x] Crear usuaria en Matomo

* [verificar] En celular, el menu lateral derecho, hacer iconos más grandes, respetando el margen para las cartas. Aumentar x3 veces de tamaño.
// puse un mediaquery en styles.css

* [x] Checar que privacidad@ redireccione a contacto
* [verificar] Encabezados principales h1 (mantener centrado) y h2 (mantener alineado a la izquierda) en rosa sin fondo en mayúsculas
// saqué el class .invertido de los layouts y pues text-transform uppercase en el css de los headers

* [verificar] El menú de más arriba, en vez de texto blanco con fondo rosa, que sea en rosa el texto, sin fondo, en mayúsculas y un poco más grande


* [verificar] Títulos de carousels en mayúscula
* [verificar] Menú lateral derecho: 
tooltip subir documental » incluir mi documental https://documentalmexicano.org/incluir-mi-documental/
incluir en tooltip de llave: Iniciar sesión / Crear cuenta
* [verificar] Incluir en el menú de franja rosa de la página de inicio 'Crear cuenta' en vez de login y que lleve a /sesion

* [a medias] Sección contacto
  Encabezado h1 centrado en rosa y mayúscula //ya
  Formulario más angosto: 70% //propiedad css container-700
  Aceptar las polìticas de privacidad está mal: que salga toda la frase con hipervinculo con target blank // para ver con f
  Checar que el formulario llegue al correo contacto@documentalme... // checar con f
  Botón enviar. Dar más padding abajo entre el botón y pie de página. //la agregué al form group el class 'aire'
 

* [verificar] Incluir mi documental
h2 en dos líneas: forme parte/del catálogo

* [verificar] En celular, flechas de carousel más grande
// le puse un media query en el css para que sea 4em

* [verificar] Cambiar orden de carrousel:
añadidos recientemente
tematicas
genero y estilos
estados
corto y largo
visibles en internet


* [verificar] En páginas de categorías, por ejemplo, https://documentalmexicano.org/tematicas/, que el encabezado sea como las otras: mayúscula, centrado



* [verificar] Pie de página: más padding 


////////////////////////////////////////////////////////////////////////

* [] un pelin más de margen izquierdo. texto desborda tantito


* f [] hacer que sutty le agregue un ?1123123 a styles.css para se salte las caches

// y esto?


* [ ] Poder hazer upload de imágenes en editor de noticias








* [x] pie de página licencia

* [x] Cambiar el color de texto en general a negro.

* [x] directamente entrar a 'https://sutty.kefir.red/sites/documentalmexicano.org/posts' al iniciar sesión



## Creación de cuenta nueva
https://sutty.kefir.red/sites/documentalmexicano.org/invitadxs/new

* [x] En la creación de cuenta nueva, el texto "El Catálogo tiene la finalidad de que sean los mismos directores y/o productores los que puedan ingresar los datos necesarios para que sus películas aparezcan en el Catálogo." no se lee bien por la falta de contraste. Ese azul no forma parte de la paleta de colores del sitio. Cambiar a negro.


## Sutty

* f [ ] Cuando confirmas tu correo e inicias sesión, la gente puede quedar muy perdida. Incluir un link a la página '/ayuda', post ya creado. 

  // no entiendo esto, agregué el botón de ayuda arriba en las migas -- f
  // no veo dónde está el botón de ayuda

* f [ ] Cambiar el texto del botón para que diga 'ver y editar fichas'.

// sigo viendo 'Empezar un artículo nuevo'


* f [x] No está integrada la identidad visual de docred acá 


* f [x] Cambiar 'artículos' por 'fichas' en el tooltip del botón.

// no lo veo



### Editor de artículos

https://sutty.kefir.red/sites/documentalmexicano.org/posts


* [] En celular, las migas de pan desbordan la pantalla
  // esto es una cuestión de bootstrap...
  // y? el caso es que se desborda y no debería. no se puede cambiar el tamaño de letra en caso de los celulares?

* [ ] Las usuarias no pueden crear 'noticias'. Sólo 'fichas'.
// sigo viendo la opción de crear una noticia

* [x] Más padding lateral. Igual que https://documentalmexicano.org/catalogo/

* [x] Cambiar el estilo de los botones para texto y borde rosa (de la paleta de colores)

* [x] Idem que comentario del texto al crear una cuenta: no se lee el texto blanco sobre ese azul que no forma parte de la paleta.

* [x] Botones blancos sobre fondo gris en el editor de texto no se ve. Cambiar a rosa.



# Issues f

## Backend

* [ ] Investigar bug de select2 en macosx -- pendiente para el 11/01

https://github.com/select2/select2/issues/4743

Parece que hay un arreglo que hay que probar, pero voy a necesitar que
me den feedback sobre esto.

https://github.com/select2/select2/pull/5211

// ¿Lo implementaste ya? ¿Qué proceso de feedback necesitas? Me falta contexto.


* [ ] js de carga multiple para archivos -- pendiente para el 11/01

// ¿cuándo vas a hacer esto?

* [x] Pie de página
	* [x] Quitar propiedad flotante. Verificar que el panel de registro de cuenta funcione después.
	* [x] Incluir licencia 
          // En desktop se ve deformado el logo de creative commons. En general los elementos están muy pegados. Necesita haber más espacio entre el logo, sitio bajo licencia y el resto del texo. Necesita un poco más de aire con respecto al menú de Política de Privacidad, etc.


* [ ] Checar por qué en Safari y Ópera carga el sitio como si fuera pantalla pequeña (f)
	* [ ] Replicar error

  Me pasaron un video mostrando como se ve en Safari, no es que se vea
  chiquito, sino que las pantallas de Mac son tan grandes que el sitio
  se ve chiquito porque el ancho máximo es mucho menor.  No es que se
  vea responsive.

	Pendiente de debuguear de nuevo.




## Frontend

* [x] 2019-01-09 Diseño CSS de ficha de captura de datos
  * [x] Aplicar mismo fondo que el sitio
  * [ ] Generar un id para cada sección
  * [ ] Aplicar un fondo diferente a cada sección
  * [ ] Más padding entre cada sección

// es dificil asi como esta generar secciones para cada seccion, por
ahora solo sale el titulo, pero es jodido incorporar logica en la
plantilla para que cada grupo este dentro de un mismo div

  * [x] Aplicar mismas tipografías y estilos css de headers y párrafo
  * [x] Estilo botones de agregar campos: más pequeños y mismo estilo que botones del sitio


* [x] 2019-01-09 Botones sutty: mismo estilo que el resto del sitio


* [ ] Volver a cargar posters de los siguientes posts «--- esto hay que pedir que lo hagan ellas
"Mirar, cantar, marchar"
"Del huipil a la chilaba"
"Dothefootage"
"Hermosa danza de la resistencia"


* [ ] Fondo 
	* [ ] Propiedad fija
	* [ ] Responsive


* [x] Fondo 
	* [x] Propiedad fija
	* [ ] Generar 3 versiones de imagen de fondo para los 3 diferentes breakpoints. Véase ejemplo de solidarity storms.
	* [ ] Quitar imagen como background image y colocarla como imagen que tenga propiedad "Z" para que lo envie al fondo.
	* [ ] Cargar las 3 imagenes y poner propiedad bootstrap de que se vea sólo en caso de que sea el tamaño de pantalla correspondiente.

* [ ] Menú "Inicio.. quiénes somos" etc fijo. Que aparezca al hacer scroll.
// no creemos que va a quedar bien.

* [verificar] Headers con franja rosado 100%. Texto centrado.

// no me acuerdo de qué querían aqui.


* [x] Centrar cards de carrousel largo/corto

* [ ] upload de imágenes en editor de noticias


# Issues N.

* [x] Políticas de privacidad
  * [x] Redactar
  * [x] Incluir en sitio
  * [x] Incluir link en el footer
  * [x] Incluir link en el formulario de captura y otras secciones donde aparece mención a las políticas

---


* [x] Cambiar el color de texto en general a negro.

* [x] Crear cuentas de correo y sala Mumble. Avisarles por correo.

* [x] Programar envío de reporte matomo al mail de contacto de docred. Avisarles por correo


* [x] Layout de sección noticias

* [x] Menú secundario de portada "subir mi documental" » fondo rosado, letras blancas, items centrados

* [x] EN "INCLUIR MI DOCUMENTAL" » INCLUIR LOS DOS BOTONES IGUAL QUE LA SECCIÓN DE /SESION al final

* [x] Buscador: más padding entre diferentes filtros
* [x] Buscador: incluir botón de buscar abajo de los filtros
* [x] Buscador: hacer más pequeño "no se encontraron resultados..."
* [x] En los thumbs de los carrousels, que tome cualquier imagen y no solo el poster. Queremos que siempre cargue un thumb y que no salga en blanco.
* [x] Al hacer clic en los títulos de los carrousels, que lleve a un category page con grid


* [x] Más padding-top del header para que no se pegue tanto al menú de arriba o de la sección anterior.

* [x] Más padding-bottom del header para que no se pegue tanto al texto que sigue.

* [x] Más padding en general entre los diferentes elementos de las páginas explicativas.

* [x] Texto justificado en todo el texto de las páginas explicativas.




* [x] Incluir texto "Búsqueda avanzada" a la derecha del icono de "filtros". 
* [x] Cambiar "tooltip" a "búsqueda avanzada" en vez de "filtros"

* [x] Texto justificado en todo el texto de las páginas explicativas.

* [x] Menú lateral con menos padding right. Más pegado al borde.

* [x] Invertir propiedad hover de menú lateral: rosado por defecto, blanco al hacer hover

* [x] Modificar logo: “DE DOCUMENTAL” DEL LOGO QUE SEA #D53F77 Y EN BOLD

* [x] En logos, usar docred logo versión negro en vez de blanco


* [x] Menú lateral con menos padding right. Más pegado al borde.

* [x] Invertir propiedad hover de menú lateral: rosado por defecto, blanco al hacer hover

* [x] JS porcentaje de financiación -- pendiente para el 11/01

* [x] QUE LAS OPCIONES DESPLEGABLES DE LAS LICENCIAS, AL SELECCIONARLAS, SE VEA TODO EL TEXTO DE LA LICENCIA. SIN PUNTOS SUSPENSIVOS, ES DECIR, QUE TENGA VARIAS LINEAS DE TEXTO Y NO SÓLO UNA -- pendiente para el 11/01

* [x] Campos que van al inicio de la ficha (datos de la persona que carga) 

* [x] Cambiar los textos a los que mandó Afra 2018-12-31


* [x] padding lateral a tarjetas en responsive

* [x] Algunas imágenes thumbs no se cargan. Por ejemplo, en el carousel de "Biográfico e histórico" (sí hay fichas que tienen thumbs y no carga ninguno). 

* [x] Cargar still como thumb si no hay cartel asignado. Ejemplo: "Biográfico e histórico", es "Aire en lugar inesperado" que, si entras en la ficha, tiene un still y no lo carga.

* [x] EL BOTÓN DE REGRESAR ARRIBA NO FUNCIONA BIEN, EN VEZ DE SUBIR ARRIBA DE LA PÁGINA, NOS LLEVA AL INICIO DE LA PÁGINA
* [x] Agregar opción "en blanco" en los parámetros desplegables y que salga por defecto en la ficha.

* [x] HACER QUE EL TITULO de ficha OBLIGATORIO SEA EL CASTELLANO Y EL OPCIONAL EL INGLES.

* [x] Aplicar condición que si el link de visionado no es público, no sale botón en ficha.

* [x] Agregar opción "en blanco" en los parámetros desplegables y que salga por defecto en la ficha.


* [x] Checar que todos los parámetros de teléfono sean opcionales

* [x] En grados académicos, "grados académicos" y "escuela" no están a la misma altura. Corregir. Véase apartado de "financiación"

* [x] Incluir "y" en redacción del punto 5: 5. DATOS DE LA DISTRIBUIDORA Y DE CONTACTO PARA LA EXHIBICIÓN

* [x] Texto antes de llenar ficha. Poner arriba y quitar texto en recuadro azul.

ANTES DE INICIAR EL LLENADO DE LA FICHA:

El Catálogo tiene la finalidad de que sean los mismos directores y/o
productores los que puedan ingresar los datos necesarios para que sus
películas aparezcan en el Catálogo.

Así mismo, ustedes podrán definir qué datos de contacto estarán visibles
para que se comuniquen con ustedes los posibles exhibidores o las
personas interesadas en la película.

Como podrán ver, algunos de los campos son obligatorios y otros no. Aún
así, los invitamos a llenar los campos que no son obligatorios ya que
nos ofrecen datos necesarios para diagnosticar las condiciones de
producción y difusión del documental en México.

El Catálogo funciona como un sitio interactivo en el que se pueden
realizar búsquedas de los documentales por diversas categorías, las
cuales solicitaremos que también ustedes ingresen en la siguiente ficha.

Algunos de estos campos se autocompletan. Si ya sabes qué quieres poner,
solo empieza a escribir y el autocompletado te sugerirá las opciones
disponibles. Si no existe lo que quieres poner, termina de escribir y
presiona Entrar para agregar opciones que aun no existen. Para vaciar
las opciones, usa el botón × a la derecha.

Si no concluyen el llenado de todos los datos de la ficha, pueden
guardar la información y continuar más tarde.

Una vez concluido el llenado de la ficha, la información será revisada
por nuestro equipo técnico, y una vez aprobada, la ficha de su película
se publicará en el Catálogo de Documental Mexcano.


* [x] Feature nuevo: incluir checkboxes en ficha sutty

* [x] En carga de roles, aparecen todos los roles como parámetros check box y sin marcar.

* [x] Feature nuevo: guardado sin verificar de un borrador de la ficha.
  * [x] Incluir botón de guardado en la ficha con una indicación de cómo volver a editar el formulario.
  * [x] Post es asignado estado "incompleto" hasta que usuarix envie el formulario

* [x] Incluir mensaje de enviando "Espera un momento. La información del formulario se está guardando…"

* [x] Incluir mensaje de error "Te faltan algunos campos obligatorio por
  llenar por lo que no se pudo enviar la información para su revisión y
  publicación. Puedes continuar llenado los campos obligatorios del
  formulario o continuar más tarde con tu usuario y contraseña."

* [x] Incluir indicación de cómo subir varias imágenes a la vez: "puedes
  usar ctrl/cmd para seleccionar varias imágenes de una sola vez".

* [x] Agregar validador de formatos para obligar a escribir fechas en
  cuatro dígitos para que todos tengan el mismo formato.



* [x] Cambiar "Subir mi documental" por "Incluir mi documental"
* [x] Item de menú "Log in" » hipervincular a /sesion
* [x] Quitar la S mayúscula en Somos: somos  EL ERROR CONTINÚA.
* [x] Link roto de imagen de logo en Quiénes somos

* [x] Aplicar condición que si el link de visionado no es público, no sale botón en ficha.
* [x] Checar que todos los parámetros de teléfono sean opcionales

* [x] En grados académicos, "grados académicos" y "escuela" no están a la misma altura. Corregir. Véase apartado de "financiación"

* [x] Incluir "y" en redacción del punto 5: 5. DATOS DE LA DISTRIBUIDORA Y DE CONTACTO PARA LA EXHIBICIÓN

* [x] En el apartado 3. Datos de lx directorx y/o productorx, cambiar
  "Realizadorxs" por "Directorxs"

* [x] En apartado "Becas y/o apoyos recibidos para la realización del
  documental por parte de alguna institución o particular", cambiar
  sub-parámetro "Premio" por "Beca".

* [x] En apartado "Becas y/o apoyos recibidos para la realización del
  documental por parte de alguna institución o particular", cambiar
  sub-parámetro "Entidad" por "Institución".

* [x] En las instrucciones vez de decir BECA dice PREMIO y Cambiar ENTIDAD por INSTITUCIÓN (es más claro)
BECA / MONTO (OPCIONAL) / ENTIDAD  INSTITUCIÓN / PÚBLICA-PRIVADA
+ RENGLONES

* [x] Cambiar a campos opcionales: teléfono, teléfono celular «-- estos valores aparecen varias veces, cambiar en todos los casos

* [x] Cambiar redacción (incluir 'Y') "5. DATOS DE LA DISTRIBUIDORA Y DE CONTACTO PARA LA EXHIBICIÓN"


* [x] Cambiar mensaje de éxito de envío de formulario


* [x] Sólo poner disponible el layout de ficha_doc para usuarias. Si haces clic en crear artículo nuevo, te lleva al layout 'archive'. Escoger ficha desde el dropdown no es algo intuitivo (f)


* [x] Modificar logo a turquesa oscuro. Voltear horizontalmente y colocar a la izquierda (g)
	* [x] Incluir texto como imagen
	* [ ] Buscar

* [x]	Checar tipografias (f)
	* [x]	Para títulos y menú principal: Neutra Text.
	* [x]	Para menú secundario y textos en general: Century Gothic.

* [ ] Fondo (n)
	* [x] Checar dimensiones necesarias: que "sprockets" sean más pequeños. Menos opacidad.
	* [x] Que no sea fijo. Checar el tema de que se repita la imagen verticalmente.

    La imagen no está hecha para repetirse infinitamente y quedan
    franjas en las uniones

	* [ ] Tendrán que diseñar versión para pantalla pequeña.

* [x] Paleta de colores (n)
	* [x] En ficha de documental poner colores opacos.
    * [x] Iconos de menu fijo » cambiar a morado y darles un poco menos margen.


* [x] Logo de DocRed (n)
	* [x] Quitar de arriba
	* [x] Sustituir por versión buena

* [x] Menú superior
	* [x] Cambiar orden de items (n)
	* [x] Incluir sección "noticias" (n)
	* [x] Alinear a la derecha (f)
	* [x] S mayusc en Somos (n)
  * [ ] Definir si las noticias se ven como tarjetas




* [x] Texto antes de llenar ficha. Poner arriba y quitar texto en recuadro azul.

ANTES DE INICIAR EL LLENADO DE LA FICHA:

El Catálogo tiene la finalidad de que sean los mismos directores y/o
productores los que puedan ingresar los datos necesarios para que sus
películas aparezcan en el Catálogo. 

Así mismo, ustedes podrán definir qué datos de contacto estarán visibles
para que se comuniquen con ustedes los posibles exhibidores o las
personas interesadas en la película. 

Como podrán ver, algunos de los campos son obligatorios y otros no. Aún
así, los invitamos a llenar los campos que no son obligatorios ya que
nos ofrecen datos necesarios para diagnosticar las condiciones de
producción y difusión del documental en México. 

El Catálogo funciona como un sitio interactivo en el que se pueden
realizar búsquedas de los documentales por diversas categorías, las
cuales solicitaremos que también ustedes ingresen en la siguiente ficha.

Algunos de estos campos se autocompletan. Si ya sabes qué quieres poner,
solo empieza a escribir y el autocompletado te sugerirá las opciones
disponibles. Si no existe lo que quieres poner, termina de escribir y
presiona Entrar para agregar opciones que aun no existen. Para vaciar
las opciones, usa el botón × a la derecha. 

Si no concluyen el llenado de todos los datos de la ficha, pueden
guardar la información y continuar más tarde. 

Una vez concluido el llenado de la ficha, la información será revisada
por nuestro equipo técnico, y una vez aprobada, la ficha de su película
se publicará en el Catálogo de Documental Mexcano. 


* [x] Noticias (f)
	* [x] Habilitar nueva categoría "Noticias"
	* [x] Artículo mismo layout que las secciones informativas
	* [x] Layout category page


* [x] Poner mismo fondo en la página de aviso de envío de confirmación una vez que envias tu solicitud de cuenta nueva.
* [x] Mail de confirmación tiene un asunto nada que ver y el cuerpo de mensaje no explica nada. No se entiende.
* [x] La página está en inglés. Cuando abres el confirmation token URL ya está en castellano, pero antes en inglés.
* [x] Veo todos los sitios de Sutty cuando sólo debería ver el de documentalmexicano.org
* [x] Tengo todos los permisos para editar el sitio cuando sólo debería poder subir borradores de ficha.
* [x] Cambiar título de portada; darle más estilo
* [x] Títulos destacados tienen padding arriba
* [x] Reducir tamaño de texto en botón de /subir
* [x] Cambiar botones a clase 'btn'
* [x] Botones con href a donde corresponde en /subir y /sesion
* [x] Mensaje de bienvenida en \_config.yml
* [x] Comportamiento de collapse y desplegar de filtros en buscador
* [x] Cambiar fondo
* [x] Cambiar tipografía
* [x] Cambiar paleta de colores: magenta como color destacado
* [x] Cambiar paleta de colores: magenta como color destacado
* [x] Solo en pantalla md y grande: padding en el encabezado de "Catálogo de Documental Mexicano" para que quede alineado con el lente. En celular dejar como está.
* [x] Cambiar mensaje barra búqueda
* [x] Barra de búsqueda quedaría debajo del título y logo. Poner centrado.
* [x] Incluir, al lado de icono de búsqueda en buscador de la portada, un icono a búsqueda avanzada con hipervínculo a /buscador.
* [x] Imagen (thumb) de los cards también hipervinculado al post.
* [x] Añadir, después de "Temas", los siguientes carousels:
* Corto y largo » serían 2 tarjetas: corto y largo
* Géneros y estilos: cada tarjeta es un tipo de género y estilo ej. Expositivo / Descriptivo
* Estados de la República: orden alfabético
* Visibles en internet
* [x] Hipervincular título de carousels a página de categoría correspondiente
* [x] Secciones informativas con más margen a los laterales y texto justificado.
* [x] Pie de página
* [x] Incluir botón para subir arriba de la página en menú fijo
* [x] Incluir tooltips en iconos
* [x] Título de cards en mayúscula y en negrita
* [x] Más margen entre título y descripción de cards
* [x] Ajustar contenido en cards. Truncate.
* [x] Establecer un máximo de tres tags en cards
* [x] Título (año) en cards
* [x] Incluir Director // Duración
* [x] Banner still ancho completo
* [x] Datos banner docu
  * [x] Recuadro sin esquinas redondeadas
  * [x] Posición abajo a la derecha
  * [x] Tamaño de fuente más pequeña
  * [x] Agregar año y duración
  * [x] Cuando pantalla pequeña, mostrar abajo de still
* [x] Rediseñar ficha: diferentes secciones de diferentes colores con diferentes tablas
* [x] Incluir carousel de películas relacionadas.
* [x] Corregir logo
* [x] Colocar el logo de docred arriba a la izquierda; también en el pie de página
* [x] Integrar el titulo en el logo: texto en diferentes colores
* [x] 2 links: icono camara de cine "Trailer" + icono ojito "Ver en línea"
* [x] Link público/privado de video
* [x] Incluir texto de Colaboradores dentro de Quiénes Somos
* [x] Incluir logo en Quiénes Somos
* [x] Reorganzar items de los menús
* [x] Grid de página de categorías debería verse igual que la portada
* [x] Formulario subida de documentales
  * [x] Incluir texto «--- pendiente que Afra lo envíe
  * [x] Incluir parámetro corto/largo, visible en internet
  * [x] Caja de "acepto política de privacidad y uso de datos" antes de enviar
  * [x] Integrar identidad visual
  * [x] Mensaje de exito al enviar ficha
* “Felicidades, haz concluido exitosamente el proceso de llenado del formulario para que tu documental sea publicado dentro del CATÁLOGO DE DOCUMENTAL MEXICANO. La información que enviaste será verificada por el equipo de revisión del del Catálogo,  lo cual puede llevar algunos días. Si hubiera algún error nos pondremos en contacto contigo para corregirlo a través del mail de contacto”.
* [x] FAQ
* [x] Arreglar sección de contacto
   * [x] Más margen lateral
   * [x] Incluir aceptar política de datos
