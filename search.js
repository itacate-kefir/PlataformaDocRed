---
---
{% comment %}
Este archivo es una mezcla de Liquid y JS
{% endcomment %}
{% node_module lunr/lunr.js %}
{% node_module lunr-languages/min/lunr.stemmer.support.min.js %}
{% node_module lunr-languages/min/lunr.es.min.js %}
{% node_module lunr-languages/min/lunr.multi.min.js %}
{% node_module liquidjs/dist/liquid.min.js %}
{% node_module query-string-parser/query_string_parser.min.js %}

{% comment %}
Tomamos la plantilla que genera HAML pero le pasamos un item que imita
un post pero cuyos valores son variables que luego se pueden reemplazar
con JS, de forma que la plantilla se vuelve dinámica en lugar de
estática y le podemos asignar valores según los resultados de búsqueda.

Luego lo capturamos en una variable que le podemos pasar a JS.

Ver _plugins/sample_filter.rb
{% endcomment %}
{% assign item='' | fake_post %}
{% capture template %}{% haml _card.haml %}{% endcapture %}

$(document).ready(function() {

  function queryFromObject(object) {
    // Clonamos el objeto porque estamos modificándolo luego
    var object = JSON.parse(JSON.stringify(object));
    var query = [];
    var desde = $('#desde').val();
    var hasta = $('#hasta').val();
    Object.keys(object).forEach(function(k) {
			if (object[k].length == 0) return;
      if (k == 'filters') return;

      if (k === 'q') {
        query.push(object[k]);
      } else if (k === 'desde') {
        delete object[k];
      } else if (k === 'hasta') {
        delete object[k];
      } else {
        query.push('+'+k+':'+object[k].replace(/(?=[: ])/g, '\\'));
      }
    });

		if (desde.length > 0) {
			if (hasta.length == 0) {
        hasta = $('#hasta').children().last().val();
      }
    }

    if (hasta.length > 0) {
      if (desde.length == 0) {
        desde = $('#desde').children().eq(1).val();
      }
    }

    if (desde.length > 0 && hasta.length > 0) {
      desde = parseInt(desde);
      hasta = parseInt(hasta);

      for (i = desde; i <= hasta; i++) {
        query.push('año:'+i.toString());
      }
    }

    return query.join(' ');
  }

  function displaySearchResults(results) {
    var searchResults = $('#search-results');
    searchResults.empty();

    if (results.length > 0) {
      var liquid = new Liquid();
      results.forEach(function (result) {
        var item = window.data.filter(function(a) { return a.id == result.ref })['0'];
        var resultsContainer = $('<div class="col-md-{{ 12 | divided_by: site.data[site.lang].archives.cols }}"></div>');
        liquid.parseAndRender("{{ template }}", item).then(html => resultsContainer.append(html));
        searchResults.append(resultsContainer);
      });
    } else {
      $('<h1/>').text("{{ site.data[site.lang].busqueda.sin_resultados }}").appendTo(searchResults);
    }

  }

  function queryStringToForm(qs) {
    Object.keys(qs).forEach(function(name) {
      if (name == 'filters') {
        $('.filters').removeClass('d-none');
      } else {
        $('#'+name).val(qs[name]);
      }
    });
  }

  $.ajax({
    type: 'GET',
    url: '/data.json{% asset %}',
    dataType: 'json',
    success: function(data) {
      window.data = data;

      $.ajax({
        type: 'GET',
        url: '/idx.json{% asset %}',
        dataType: 'json',
        success: function(idx) {
          window.index = lunr.Index.load(idx);

          var qs = fromQuery(location.search);
          var query = queryFromObject(qs);
          queryStringToForm(qs);
          if (query.length > 0) {
            displaySearchResults(window.index.search(query));
          }
        }
      });
    }
  });

  $('#buscador').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var url_query = query;
    var desde = $('#desde').val();
    var hasta = $('#hasta').val();
    var fields = $('.search-field');
    var query_string = $('#buscador :input').filter(function() { return !!this.value; }).serialize();
    var qs = fromQuery(query_string);
    var query = queryFromObject(qs);

    history.pushState({}, "", window.location.pathname + '?' + query_string);
    var results = window.index.search(query);
    displaySearchResults(results);
  });

  $(window).on('popstate', function(e) {
		var qs = fromQuery(location.search);
		var query = queryFromObject(qs);
		queryStringToForm(qs);
		if (query.length > 0) {
			displaySearchResults(window.index.search(query));
		}
  });

  $('#filters').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('.filters').toggleClass('d-none');
  });

});
