# Registra un thumb por defecto para la tarjeta
Jekyll::Hooks.register :site, :post_read do |site|
  site.posts.docs.each do |post|
    next if post.data.dig('draft') || post.data.dig('incomplete')

    case post.data['layout']
    when 'ficha_documental'
      stills = post.data.dig('stills')
      post.data['thumb'] = post.data.dig('poster')

      # Si no hay poster, usar un still al azar
      unless post.data['thumb'] && !stills.empty?
        post.data['thumb'] = stills.sample
      end

      # Convertir en thumb si encontramos una imagen
      if post.data['thumb']
        parts = post.data['thumb'].split('/')
        parts.push "thumb_#{parts.pop}"
        post.data['thumb'] = parts.join('/')
      end

      # Si todo falla mostramos una imagen por defecto
      post.data['thumb'] ||= '/public/images/thumb_default.png'
    else
      unless post.data['poster']
        post.data['poster'] = '/public/images/default.png'
      end

      parts = post.data['poster'].split('/')
      parts.push "thumb_#{parts.pop}"
      post.data['thumb'] = parts.join('/')
    end

    # Agregar la imagen a jekyll-seo-tag
    if post.data['thumb']
      post.data['image'] = post.data['thumb'].gsub('thumb_', '')
    end

    unless File.exists?(post.data['thumb'].gsub(/^\//, ''))
      post.data['thumb'] = '/public/images/thumb_default.png'
    end
  end
end
