module Jekyll
  module SampleFilter
    # Obtiene un elemento al azar de un array
    def sample(input)
      input.sample
    end

    # Recibe un array y devuelve los primeros elementos hasta el límite
    # especificado
    #
    # También funciona con hashes
    def limit(input, limited_to)
      if input.is_a? Array
        input[0..(limited_to - 1)]
      elsif input.is_a? Hash
        input.slice(*input.keys[0..(limited_to - 1)])
      end
    end

    # Recibe una colección de ítems y devuelve un array con las filas y
    # las columnas, para que podamos ponerlos en una grilla
    #
    # XXX Tal vez debería llamarse `grid`
    def rows(input, cols)
      input.each_slice(cols).to_a
    end

    # Recibe un número y obtiene un array del 1 a la cantidad
    def to_array(input)
      return [] unless input
      (1..(input || 1)).to_a
    end

    # Un post falso para usar en JS con mustache
    # TODO obtener todos los valores de la plantilla
    def fake_post(input)
      {
        'title' => "{{ title | truncate: 35 }}",
        'thumb' => '{{ thumb }}',
        'url' => '{{ url }}',
        'anio' => '{{ anio }}',
        'duracion' => '{{ duracion }}',
        'directorx' => "{{directorx|join:', '|truncate: 35}}"
      }
    end

    # Obtener un array de todos los valores únicos de un campo en todos los
    # posts
    def collect_field(input, field)
      input.map do |i|
        i[field]
      end.flatten.compact.uniq.sort
    end

    # Obtener una cantidad de posts basados en el valor de un campo
    #
    # Similar a Jekyll::Site.post_attr_hash() pero más elegante y no
    # falla si el campo no es un array.
    def attr_hash(input, field)
      collect_field(input, field).map do |f|
        { f => @context.registers[:site].posts.docs.select do |p|
          [p.data.dig(field)].flatten.include? f
        end.compact }
      end.reduce({}, :merge)
    end

    def filter_by(input, key, value)
      input.select do |p|
        if p.data[key].is_a? Array
          p.data[key].include? value
        elsif p.data[key].is_a? Hash
          p.data[key].keys.include? value
        else
          p.data[key] == value
        end
      end
    end
  end

  class AssetTag < Liquid::Tag
    def render(context)
      "?#{Time.now.to_i.to_s}"
    end
  end

  class NodeModuleTag < Liquid::Tag
    def initialize(tag, markup, tokens)
      super

      @file = File.join('node_modules', markup.strip)
    end

    def render(context)
      File.read(@file)
    end
  end
end


Liquid::Template.register_filter(Jekyll::SampleFilter)
Liquid::Template.register_tag('asset', Jekyll::AssetTag)
Liquid::Template.register_tag('node_module', Jekyll::NodeModuleTag)
