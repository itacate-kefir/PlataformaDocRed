require 'json'
require 'open3'

# Este plugin procesa todos los artículos y genera un índice de Lunr
Jekyll::Hooks.register :site, :post_write do |site|
  # Usar el idioma de la configuración.  `lang:` en `_config.yml` o si
  # estamos usando el plugin de I18n, se configura solo.
  lang = site.config.dig('lang')
  # TODO: Informar y/o hacer el Stemmer opcional si falta el idioma
  break unless lang

  indexable = site.config.dig('index', 'fields') || []

  # Indexar cada artículo por separado
  lunr = site.posts.docs.map do |post|
    next unless post.data['layout'] == 'ficha_documental'
    post
      .data
      .dup
      .keep_if { |k,_| indexable.include? k }
      .merge(content: post.content, url: post.url, id: post.url, year: post.date.year)
  end.compact

  file = 'idx.json'
  data = 'data.json'

  # Escribir el archivo de datos
  d = File.new(File.join(site.dest, data), 'w')
  d.write(lunr.to_json)
  d.close

  site.keep_files << data

  # Simplificar la data porque lunr no puede indexar hashes ni arrays
  lunr = lunr.map do |data|
    data.transform_values do |v|
      if v.is_a? Array
        if v.first.is_a? Hash
          v = v.map(&:values).join(', ')
        else
          v.join(', ')
        end
      elsif v.is_a? Hash
        v.values.join(', ')
      else
        v.to_s
      end
    end
  end

  # Indexar todos los artículos a través del indexador de lunr
  Open3.popen2("NODE_PATH=./node_modules ./bin/indexer #{lang}") do |stdin, stdout, wait|
    stdin.puts lunr.to_json
    stdin.close

    index = File.new(File.join(site.dest, file), 'w')
    index.write(stdout.read)
    index.close
    site.keep_files << file

    wait.value
  end
end
