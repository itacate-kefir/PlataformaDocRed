# Convierte palabras clave y temáticas en tags y categorías, para que
# estén disponibles en el sitio.
Jekyll::Hooks.register :site, :post_read do |site|
  site.posts.docs.each do |post|
    next unless post.data['layout'] == 'ficha_documental'
    post.data['tags'] = post.data['palabras_clave']
    cat = post.data.fetch('tematicas', []).dup

    # Para poder generar una página con todas las películas por estado
    cat << post.data['estado']
    cat << post.data['genero_y_estilo']

    # Para saber si están disponibles en Internet o no
    cat << 'Disponible en Internet' if post.data['link_para_visionado_publico']

    # Para poder buscar luego
    post.data['directorx'] = post.data.dig('equipo_de_produccion', 'direccion')
    post.data['anio'] = post.data['año']

    # Decidir el metraje en base a la duración en minutos
    cat << post.data['metraje'] = if post.data['duracion'].to_i > 40
      'Largo'
    else
      'Corto'
    end

    post.data['categories'] = cat.flatten.uniq
  end
end
