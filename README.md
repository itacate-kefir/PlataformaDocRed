# DocRed 

## SASS

* https://0xacab.org/itacate-kefir/sutty/blob/rails/app/assets/stylesheets/application.scss
* https://sass-lang.com/guide

## Editar localmente el sitio Jekyll

* Prepara tu entorno ruby: https://0xacab.org/kefir/sake/wikis/Entorno-en-Ruby-on-Rails
* Clonar con ssh
* cd en el dir de la repo
* Instalamos las gemas: bundle install --path=~/.gem
* Instalamos el paquete 'yarn' para instalar las dependencias de css y js. Lo vamos a hacer a mano para tener la versión 1.7.

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn

* Instalamos las dependencias con yarn: yarn install
* Generamos el sitio: bundle exec jekyll build (si estás usando rbenv) o 'make build'
* Levantamos el sitio en el servidor local: bundle exec jekyll serve (si estás usando rbenv) o 'make serve'

## Deployear los cambios del sitio jekyll

* Entrar en miso sin entrar en tmux: ssh hola@miso; ssh -Ap 1863 root@localhost (sin hacer 'su -' y 'tmux a' porque necesitas poder pullear con las llaves de ssh-agent y la sesion de tmux no las tiene)
* cd /srv/http/sutty.kefir.red/shared/_sites/f@kefir.red/documentalmexicano.org
* checa si hay cosas por commitear: 'tig status', 'git commit', 'git push'
* git pull
* chown -R app:app
* Compilar el sitio desde sutty o desde el servidor: 'sudo -u app touch /srv/http/sutty.kefir.red/shared/_sites/f@kefir.red/documentalmexicano.org/.generate'

## HAML

Las plantillas se escriben en [HAML](https://haml.info) en lugar de
HTML.  Esto permite escribir más rápido HTML, con menos errores.  Pero
Jekyll no soporta nativamente HAML, sino que usa
HTML+[Liquid](https://github.com/Shopify/liquid/wiki/Liquid-for-Designers).
El plugin `jekyll-haml` se encarga de hacer esta conversión, aunque las
plantillas quedan en HAML+Liquid.

Entonces, en lugar de hacer esto:

```liquid
<a href="{{ post.url }}">{{ post.title }}</a>
```

Hacemos:

```haml
%a{href: "{{ post.url }}"} post.title
```

Y un loop es un poco más desprolijo, pero más legible que en HTML igual:

```liquid
<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{ post.title }}</a>
    </li>
  {% endfor %}
</ul>
```

Queda:

```haml
%ul
  {% for post in site.posts %}
  %li
    %a{href: "{{ post.url }}"} {{ post.title }}
  {% endfor %}
```

La regla es que HAML no se tiene que enterar que hay cosas en Liquid.
Para HAML, toda la lógica y valores en Liquid son pedacitos de texto tal
cual los escribimos.

Entonces hay que respetar los niveles de indentación.  En este caso, el
`%li` de HAML está al mismo nivel que el `for` de Liquid, porque la
conversión se hace en dos pasos y una no sabe de la otra.  Primero se
convierte el HAML en HTML+Liquid:


```liquid
<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{ post.title }}</a>
    </li>
  {% endfor %}
</ul>
```

Y luego en HTML:

```html
<ul>
  <li>
    <a href="#">Un artículo</a>
  </li>
  <li>
    <a href="#">Otro artículo</a>
  </li>
</ul>
```


**Esto es haml en una sola linea**
%elemento.clase contenido

**Lo mismo en varias lineas**
%elemento.clase
  contenido

**Al tener varias lineas, cada linea se convierte en contenido de 'elemento', si estan en el mismo nivel**
%elemento.clase
  contenido
  contenido2
  %small contenido mas pequeño

**o lo mismo en varias lineas**
%elemento.clase
  contenido
  contenido2
  %small
    contenido
    mas
    pequeño
 
**# en el caso puntual, pasarías de esto**
%h1.centrado {{ site.data.es.index.recientes.title }}

-# a esto
%h1.centrado
  {{ site.data.es.index.recientes.title }}
  {{ cat | split: ':' | first }}


### Directorios

Todos los componentes por separado se encuentran en el directorio
`_includes` y luego se pueden incluir como parciales así:

```haml
%div {% haml _un_componente.haml %}
```

Este **ejemplo** crea un `<div>` en HAML y luego el filtro `{% haml %}`
de Liquid busca otro HAML llamado `_un_componente.haml` dentro de
`_includes/`.
