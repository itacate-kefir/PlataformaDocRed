install:
	test -f .bundle/config || bundle install --path=~/.gem
	test -f .bundle/config && bundle
	yarn

build:
	bundle exec jekyll build --trace

serve:
	ruby -r webrick \
	     -e "s = WEBrick::HTTPServer.new(Port: 4567, DocumentRoot: '_site'); trap('INT') { s.shutdown }; s.start" 

sync:
	rsync -av _site/ app@miso:/srv/http/sutty.kefir.red/shared/_deploy/documentalmexicano.org/
	ssh app@miso chown -R app:http /srv/http/sutty.kefir.red/shared/_deploy/documentalmexicano.org/

images := $(shell find public/images -type f ! -name 'thumb_*')
thumbs := $(patsubst public/images/%,public/images/thumb_%,$(images))

public/images/thumb_%: public/images/%
	convert -strip -resize 300x300\> $< $@

thumbnails: $(thumbs)
