---
nombre_completo: Alana Simoes
pais_de_la_persona: México
estado_de_la_persona: Jalisco
municipio_de_la_persona: Guadalajara
consentimiento: 'true'
titulo_en_ingles: My Brother
poster: "/public/images/b4f0bd2e-1868-40b8-b0bf-b9a009b3aa51.png"
stills:
- "/public/images/00089b8e-6101-42c1-b0ce-3bf67871f36b.jpg"
sitio_web_de_la_pelicula: http://cinematropodos.com
pais: México
estado: Jalisco
municipio: Guadalajara
año_de_inicio_de_la_produccion: '2009'
año: '2018'
duracion: '70'
idioma_original:
- Español
- Ruso
idiomas_de_los_subtitulos:
- Inglés
formatos:
- Blu Ray
- DCP
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Familia
- Danza
- Infancia
- Adpcion
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - España
  - Rusia
  estados:
  - Madrid
  - Siberia
  - Moscu
sinopsis_espaniol: Alexey nace en Rusia y es adoptado por Gabriela, una mujer soltera.
  Más tarde, Gabriela y Alexey, adoptan otro niño en Siberia, Mateo. A lo largo de
  nueve años Alexey y Mateo nos muestran lo que significa construir vínculos recíprocos
  de confianza a pesar de las dificultades internas que enfrentan.
sinopsis_ingles: 'Alexey was born in Russia and adopted by Gabriela, an unmarried
  woman. Later, Gabriela and Alexey adopted Mateo, another child from Siberia. Over
  the course of nine years, Alexey and Mateo demonstrate what it means to build bonds
  of reciprocal trust despite the internal conflicts they face. '
link_al_trailer: https://vimeo.com/240420656
link_para_visionado: https://vimeo.com/256827040
link_para_visionado_publico: 'false'
contrasena_para_visionado: cinematropodossello
festivales:
- festival: SEMINCI Semana Internacional de Cine en Valladolid
  año_de_edicion: '2018'
- festival: Festival Internacional de Guadalajara
  año_de_edicion: '2018'
- festival: DocsMX
  año_de_edicion: '2018'
- festival: Maine International Film Festival
  año_de_edicion: '2018'
- festival: FICG in L.A
  año_de_edicion: '2018'
- festival: Festival Zanate de Cine y Video Documental de Colima
  año_de_edicion: '2018'
- festival: Festival Internacional Baja California
  año_de_edicion: '2018'
- festival: Alternative Film Festival
  año_de_edicion: '2018'
- festival: Festival Internacional de Cine Oaxaca
  año_de_edicion: '2018'
- festival: Festival Mundial Veracruz
  año_de_edicion: '2018'
- festival: ''
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: José Rovirosa UNAM
- reconocimiento: Mención Especial
  institución: Festival Mundial de Veracruz
- reconocimiento: Mejor Documental
  institución: Baja California International Film Festival
- reconocimiento: Premio Marcel Sisniegas
  institución: Festival Mundial de Cine Veracruz
becas_y_apoyos:
- beca: Fomento y Coinversiones
  monto: '250000'
  institución: FONCA
  tipo: Pública
- beca: Premio Post producción
  monto: '150000'
  institución: DocsMX Lab
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Alana Simoes
  produccion:
  - Alana Simoes
  casa_productora:
  - Cinematropodos
  guion:
  - Alana Simoes
  investigacion:
  - Alana Simoes
  fotografia:
  - Daniela Cajias
  sonido:
  - ''
  - Christian Tapia
  edicion:
  - Aldo Alvarez
  musica_original:
  - ''
  - Diego Benlliure
  otros:
  - nombre: Hector Quintanar
    rol: Diseño Sonoro
  - nombre: Juan Andres Vergara
    rol: Musica original
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Alana Simoes
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1979-08-10'
  telefono_fijo:
  - '3324722001'
  telefono_fijo_publico: 'true'
  telefono_celular_publico: 'true'
  correo_electronico: alanasi@cinematropodos.com
  correo_electronico_publico: 'true'
  grados_academicos:
  - grado_academico: Licenciatura Ciencias Comunicación
    escuela: Havana-Ceneval
  - grado_academico: Mestría en Teoría Crítica
    escuela: Instituto de Estudios Críticos
  - grado_academico: Curso Regular Cine Documental
    escuela: EICTV, Cuba
  - grado_academico: Curso Regular Cinematrografía
    escuela: Séptima Ars, Madrid
distribuidoras:
- nombre: Cinematrópodos
  nombre_publico: 'true'
  telefono: '3324722001'
  telefono_publico: 'true'
  correo_electronico: alanasi@cinematropodos.com
  correo_electronico_publico: 'true'
  sitio_web: http://cinematropodos.com
  sitio_web_publico: 'true'
datos_de_contacto:
- nombre: Alana Simoes
  telefono: '5551963793'
  correo_electronico: alanasi@cinematropodos.com
  sitio_web: http://cinematropodos.com
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Festivales / Muestras internacionales
- Amazon Prime
- Filmin.es
- Escuelas
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 2-3 millones de pesos
coproductores:
- coproductor: Nicolas Celis
  pais: México
  estado: Ciudad de México
- coproductor: Alejandro Duran
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '20'
  privada: '60'
  propia: '20'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
title: Mi hermano
dir: ltr
tags:
- novedad
categories:
- Noticias
incomplete: true
draft: true
author: alanasi@cinematropodos.com
order: 89
---

