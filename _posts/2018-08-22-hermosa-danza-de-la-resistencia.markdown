---
title: Hermosa danza de la resistencia
poster: "/public/images/24b6bf3a-e090-428d-a193-929b237e7df0.png"
stills:
- "/public/images/c4a0ba81-873a-4e44-af06-978d4ca9c347.png"
pais: México
estado: Puebla
municipio: Cholula
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '6'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- Chiapas
- Pueblos Indígenas
- Danza
- insurgente
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Chiapas
sinopsis_espaniol: Pasos, roces, cuerpos, movimiento. Una danza en silencio de indígenas
  encapuchados, el resurgimiento del movimiento insurgente en los altos de Chiapas.
sinopsis_ingles: "Steps, friction, bodies, movement. A silent dance of hooded Indians,
  the resurgence of the insurgent movement in the highlands of Chiapas.\r\n"
link_para_visionado: https://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Gabriela Domínguez Ruvalcaba
  produccion:
  - Gabriela Domínguez Ruvalcaba
  casa_productora:
  - Bosque Negro
  guion:
  - Gabriela Domínguez Ruvalcaba
  investigacion:
  - Gabriela Domínguez Ruvalcaba
  fotografia:
  - Gabriela Domínguez Ruvalcaba
  sonido:
  - Gabriela Domínguez Ruvalcaba
  edicion:
  - Gabriela Domínguez Ruvalcaba
  otros:
  - nombre: Gabriela Domínguez Ruvalcaba
    rol: Postproducción de imagen
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Gabriela Domínguez Ruvalcaba
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - 2221 186 430
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2221 186 430
  telefono_celular_publico: 'false'
  correo_electronico: gabsdomru@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Gabriela Domínguez Ruvalcaba
  telefono: 2221 186 430
  correo_electronico: gabsdomru@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: Hermosa danza de la resistencia
order: 52
---

