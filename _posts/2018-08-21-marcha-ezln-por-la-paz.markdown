---
title: Marcha EZLN por la Paz
poster: "/public/images/504bfcde-9235-4f01-b355-ebc55c0e24cf.JPG"
stills:
- "/public/images/b70fc407-4a58-4610-93ca-71744df420b6.JPG"
- "/public/images/72e189aa-f485-4a98-a12b-581d34aade7d.JPG"
- "/public/images/c4831f4a-9543-4a8d-9961-9a6c47555970.JPG"
pais: México
estado: Chiapas
municipio: San Cristobal de las Casas
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '8'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- EZLN
- Movimientos Sociales
- Pueblos Indígenas
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - Chiapas
sinopsis_espaniol: Más de 15 mil indígenas del Ejército Zapatista de Liberación Nacional
  (EZLN) se movilizaron el siete de mayo en San Cristobal de Las Casas, Chiapas, en
  atención al llamado de la Marcha por la Paz, Justicia y Dignidad, lanzado por el
  poeta Javier Sicilia, cuyo hijo fue asesinado en la guerra supuestamente contra
  el narcotráfico, que ya se cobró mas de 40 mil vidas en México.
sinopsis_ingles: More than 15 thousands indigenous people from EZLN marched on May
  7 on San Cristobal de las Casas, Chiapas, answering the March for Peace, Justice
  and Dignity launched by poet Javier Sicilia, who's son was murdered in the so called
  war on narcotrafic, which has already killed more than 40 thousands lives on Mexico.
link_al_trailer: https://www.youtube.com/watch?v=DI4zh7EVxZs
link_para_visionado: https://vimeo.com/23479770
festivales:
- festival: Los Angeles Movie Awards
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Los Angeles Movie Awards 2011
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Ezequiel Sánchez
  produccion:
  - Mónica Parra
  guion:
  - Federico Denegri
  investigacion:
  - Proyecto Chakana
  fotografia:
  - Marina Nerin Cuevas
  sonido:
  - Marina Nerin Cuevas
  edicion:
  - Federico Denegri
  - Ezequiel Sanchez
  otros:
  - nombre: Proyecto Chakana
    rol: Producción de campo
directorxs:
- rol:
  - Productor(a)
  nombre: Mónica Parra
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1990-01-01'
  telefono_fijo:
  - 222 504 9982
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+52 1 222 504 9982"
  telefono_celular_publico: 'false'
  correo_electronico: mparrah17@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Ezequiel Sánchez
  nacionalidad: Argentina
  fecha_de_nacimiento: '1980-01-01'
  telefono_fijo:
  - 9 11 4087 1121
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+54 9 11 4087 1121"
  telefono_celular_publico: 'false'
  correo_electronico: ezequielsanchez@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Mónica Parra\t\t\t\t\t"
  telefono: "+52 1 222 504 9982\t\t"
  correo_electronico: mparrah17@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Marcha EZLN por la Paz
order: 15
---

