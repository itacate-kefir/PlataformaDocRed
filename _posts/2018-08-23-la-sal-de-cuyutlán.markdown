---
title: "La sal de Cuyutlán\t\t\t\t\t\t"
poster: "/public/images/9a6410f9-4925-4656-b678-d1caf4176cdc.jpg"
stills:
- "/public/images/b0f0151a-012e-43b2-83f9-00898de870e7.jpg"
- "/public/images/b0f0151a-012e-43b2-83f9-00898de870e7.jpg"
- "/public/images/b0f0151a-012e-43b2-83f9-00898de870e7.jpg"
pais: México
estado: Colima
municipio: Colima
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '59'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
palabras_clave:
- trabajo
- sal
- proceso industrial
- producción de sal
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - colima
sinopsis_espaniol: "El documental narra la historia de la producción de sal en Cuyutlán,
  Colima, México, asimismo, se dan a conocer cada una de las formas de extracción
  de la sal, desde la más antigua hasta la más innovadora que no deja de ser artesanal.
  Cabe señalar que en el video documental se destaca la importancia histórica que
  ha tenido la producción de sal para el Estadode Colima, México, así como el esfuerzo
  mal remunerado que realizan los salineros para lograr su extracción.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: Festival Pantalla de Cristal
  año_de_edicion: '2010'
- festival: Festival Internacional de Cine y Video Indígena "Mirando desde nuestra
    raíz"
  año_de_edicion: '2010'
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Bogart Rodríguez
  produccion:
  - Bogart Rodríguez / Alejandra Santana
  guion:
  - Bogart Rodríguez
  fotografia:
  - Bogart Rodríguez
  - alejandra santana
  sonido:
  - Bogart Rodríguez
  edicion:
  - Bogart Rodríguez / Alejandra Santana
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Bogart Rodríguez\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-04-22'
  telefono_fijo:
  - "(045) 3123199255"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(045) 3125508283"
  telefono_celular_publico: 'false'
  correo_electronico: bogart22@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Bogart Rodríguez\t\t\t\t\t"
  telefono: "CEL (045) 3125508283\t\t"
  correo_electronico: bogart22@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: "Secretaría de Cultura del Gobierno de Colima\t\t\t\t\t"
  pais: México
  estado: Colima
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "La sal de Cuyutlán\t\t\t\t\t\t"
order: 59
---

