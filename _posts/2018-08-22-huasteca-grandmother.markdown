---
title: Abuela huasteca
poster: "/public/images/e205a073-d4ac-416b-bcc7-bfffb2e1b027.JPG"
stills:
- "/public/images/86dc9f60-d1f5-4c96-94ac-2447f692e645.JPG"
- "/public/images/123f02f7-4d10-43a7-bb13-a4063e1973ea.JPG"
pais: México
estado: Oaxaca
municipio: Oaxaca
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '9'
idioma_original:
- Castellano
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Huasteca
- Aves
- Vida
- Historia
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - San Luis Potosí
sinopsis_espaniol: "Historia de vida de una abuela Huasteca en el Sótano de las Golondrinas.\r\nDespués
  de admirar el espectáculo natural brindado por las aves, Doña Concepción cuenta
  cómo se crió, cómo era la vida cuando era jóven, que tanto cambió el pueblo y cómo
  se las rebusca hoy en día para seguir adelante junto a la niña que adoptó."
sinopsis_ingles: "Life of an old huasteca woman from Sotano de las Golondrinas.\r\nAfter
  admiring the natural show made by the birds, Miss Concepción tells how she was raised,
  how life was when she was young, how much the town changed and how she survives
  now a day with the adopted girl she lives with."
link_para_visionado: https://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: Ninguno
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Ezequiel Sanchez
  produccion:
  - Mónica Parra
  guion:
  - Federico Denegri
  - Federico Barra
  investigacion:
  - Proyecto Chakana
  fotografia:
  - Karin Rabinovich
  sonido:
  - Manuel Baraldo
  - Ezequiel Sanchez
  edicion:
  - Ezequiel Sanchez
  - Manuel Barando
directorxs:
- rol:
  - Productor(a)
  nombre: Mónica Parra
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - "+52 1 222 504 9982"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+52 1 222 504 9982"
  telefono_celular_publico: 'false'
  correo_electronico: mparrah17@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Ezequiel Sanchez
  nacionalidad: Argentina
  fecha_de_nacimiento: ''
  telefono_fijo:
  - "+54 9 11 4087 1121"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+54 9 11 4087 1121"
  telefono_celular_publico: 'false'
  correo_electronico: ezequielsanchez@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Mónica Parra
  telefono: "+52 1 222 504 9982"
  correo_electronico: mparrah17@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: Abuela huasteca
order: 21
---

