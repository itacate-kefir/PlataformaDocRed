---
titulo_en_ingles: Política de privacidad y uso de datos
layout: post
permalink: "/politicas/"
order: 11
---

_Última actualización: 16 de Febrero de 2019. Documento producido por Kéfir_

## Contexto

Antes de entrar en detalles concretos sobre cómo utilizamos los datos generados en esta plataforma y qué puede hacer para ser agente de este proceso, vamos a introducir un contexto general.

### Sobre el proyecto

Documentalistas Mexicanos en Red (DocRed) surge en el año 2012 como un red nacional de realizadores, productores, guionistas, investigadores y gestores culturales interesados en impulsar el cine documental mexicano. Desde nuestros primeros encuentros, al compartir nuestras experiencias provinientes de los diferentes estados de la República, nos dimos cuenta que gracias a las revolución digital se han abaratado costos y se ha transformado la manera como producimos, distribuimos y exhibimos el cine documental, por lo que la producción documental ha crecido signficativamente. Sin embargo, paradójicamente, los espacios de exhibición son muy pocos y es muy difícil conocer cuál es la producción documental que se realiza en todos los confines de nuestro país. El Instituto Mexicano de Cinematografía (IMCINE) realiza un anuario con estadísticas entorno a los datos que arroja la industria cinematográfica, pero como ellos mismos lo mencionan, es muy difícil darle seguimiento a la producción documental de todos los estados de la República, ya que muchas de estas películas se realizan con apoyos locales y sin gran infraestructura industrial.

Para más información, visita la sección ["Sobre el catálogo"](https://documentalmexicano.org/catalogo/).

### Quiénes somos

Documentalistas Mexicanos en Red (DocRed) surge en el año 2012 como un red nacional de realizadores, productores, guionistas, investigadores y gestores culturales interesados en impulsar el cine documental mexicano.

Ahora, justamente a través de los medios independientes, de individuos y colectivos ciudadanos nos acercamos desde muchos ángulos y puntos de vista a lo que ocurre en distintos lugares de nuestro país. Lo estamos viendo, lo estamos viviendo, lo estamos registrando de manera audiovisual y desde ahí, también se está construyendo y haciendo memoria.

Para más información, visita la sección ["Quiénes somos"](https://documentalmexicano.org/quienes-somos/).


### El Derecho a la Privacidad

El Derecho a la Privacidad se define como un derecho humano, explícitamente enunciado en el Artículo 12 de la Declaración Universal de Derechos Humanos de 1948.

"Nadie será objeto de injerencias arbitrarias o ilegales en su vida privada, su familia, su domicilio o su correspondencia, ni de ataques ilegales a su honra y reputación. Toda persona tiene derecho a la protección de la ley contra esas injerencias o esos ataques."

### Regulaciones

Aparte del marco de derechos humanos, existen regulaciones específicas en torno a los datos. ¿Quizás te suene familiar las siglas GPDR?

El ["Reglamento General de Protección de Datos"](https://es.wikipedia.org/wiki/Reglamento_General_de_Protecci%C3%B3n_de_Datos) (en inglés “General Data Protection Regulation”) entró en rigor a partir del 25 de Mayo de 2018. 

Esta regulación europea está diseñada para proteger mejor a las personas de brechas de seguridad e incumplimientos contra la privacidad. La nueva ley, entre otros aspectos, estipula cómo las empresas deben manejar los datos de sus clientes.

Desafortunadamente, estas regulaciones no se aplican en todos los contextos y tampoco son suficientes en si mismas. Ciertas jurisdicciones albergan una comprensión y abarcan la privacidad de una manera relativamente buena; otras se quedan bastante atrás.

Existen personas y grupos que, desde el ámbito de la promoción de políticas públicas están luchando por una equidad al acceso a la privacidad.

Les invitamos a leer [el comunicado de la Asociación para el Progreso de las Comunicaciones Progresistas sobre el PGPD](https://www.apc.org/fr/node/34716).


### Cumplimiento

Todos los sitios y plataformas visitadas por personas ciudadanas de jurisdicciones que regulan la privacidad de datos deben proporcionar acceso a un documento legal obligatorio que explica cómo recolectan, almacenan, procesan y comparten información de identificación personal.

Información de identificación personal (en inglés "Personal Identifiable Information -PII-): cualquier información relacionada con una persona identificada o identificable ('persona afectada o interesada'/'sujeto de datos'); una persona identificable es aquella que puede ser identificada, directa o indirectamente. Ejemplos de estos datos personales incluyen, pero no se limitan a: nombre, número de seguridad social, cédula de conducir, otros identificadores del Estado; ciudadanía, estátus legal, género, origen étnico/racial, opiniones políticas, creencias religiosas o filosóficas, afiliación sindical, datos genéticos y biométricos (que pueden identificar a una persona concreta si son procesados); datos de contacto de emergencia, etc.

Recolectar y usar datos no genera daños necesariamente. Los datos se pueden utilizar para cumplimientos legales o relacionados con rendir cuentas a financiadoras, además de obtener retroalimentación y mejorar herramientas. Lo importante es la transparencia en cómo estos datos son recolectados, almacenados, procesados y compartidos.


## Qué estamos haciendo

### Cookies y código de terceros

Cuando visitas un sitio web, sus contenidos se cargan desde diferentes fuentes (dominios y servidores). Este funcionamiento es lo que caracteriza al hipertexto y es cómo navegamos la red, pero también puede suponer un problema de privacidad. Hoy en día, muchas imágenes y código embebido utiliza cookies y otros métodos para rastreaer nuestro comportamiento de navegación. Muchas veces para mostrar anuncios. Estos dominios se llaman "rastreadores de terceros".

Los cookies de internet son, esencialmente, archivos de texto que un sitio almacena en tu computadora para, en posibles visitas posteriores, "recuerda" información como tu idioma preferido o tus datos de inicio de sesión.

La plataforma de DocRed contiene cookies de sesión que usamos para identificar si una navegante ingresa en su cuenta de DocRed para subir o editar fichas de documentales. 

Las librerías de código utilizada en la programación y desarrollo de la plataforma son software libre.


### Comunicaciones

Los sitios web que incluyen formularios de contacto deben describir por qué están pidiendo la información que piden y qué van a hacer con él después. Por ejemplo, si se va a utilizar para un boletín o una base de datos.

La plataforma de DocRed contiene varios formularios como modo de contacto y también para gestionar la creación y edición de fichas de documentales. Entre la información que solicitamos, pedimos algunos datos personales. Les contamos para qué fines.

En [la sección de contacto](https://documentalmexicano.org/contacto/) pedimos tu nombre (puedes utilizar un pseudónimo; no es necesario que sea el nombre vinculado a tu identidad civil/oficial) y un correo electrónico. Estos datos son necesarios para responderte y se almacenan tomando en cuenta implementaciones de seguridad. No utilizaremos esta información para otros fines. 

A la hora de crear y editar fichas de documentales, la plataforma DocRed te redirecciona a Sutty, una plataforma desarrollada por [Kéfir](https://kefir.red). Cuando creas una ficha nueva, pedimos algunos datos personales tuyos. Esta información es para el equipo de DocRed para conocer mejor las personas que interactúan con la plataforma, aportando perspectivas en seguir abonando al proyecto. Si tienes preguntas, puedes enviar un correo a 'privacidad@documentalmexicano.org'. 

En todos los casos, la transmisión de datos de tu navegador al servidor se realiza a través de un protocolo criptográfico (SSL). Puedes ver que la URL comienza con un 'https' y suele aparecer una especie de candado que indica que existe un certificado de seguridad asociada a la plataforma. Sin embargo, esto no significa que no haya otros intermediarios (como tu proveedor de internet o algun tercero que está accediendo a tu dispositivo debido a un ataque malicioso) que estén interceptando estos datos. Esto no lo decimos con el ánimo de asustarte sino para explicar que por nuestra parte hacemos lo posible, pero no somos las únicas responsables en tu seguridad en línea.

El servicio de correo de las cuentas @documentalmexicano.org es administrado por Kéfir, proyecto que se compromete a implementar, día a día, medidas de seguridad, mantener -durante el menos tiempo posible, en este caso, una semana - historiales de la menor cantidad de información des-identificada (no asociada a individuos específicos) necesaria para que funcione el servicio.

[Conoce más sobre las políticas de privacidad y datos de Kéfir](https://wiki.kefir.red/Políticas_de_Privacidad)

### Historiales y estadísticas web

Un 'log' es un registro. Los servicios y aplicaciones que se ejecutan en un dispositivo tienden a mantener algun tipo de registro. Esto brinda información para mejorar herramientas y solucionar potenciales errores. Generalmente, esta información es útil, pero puede llegar a incluir información personal como la dirección IP y nombres de usuarixs que puede ser usada para crear perfiles bastantes prcesisos acerca del comportamiento de personas. Por ello, es importante anonimizar los 'logs' de una manera segura. 

Lxs servidorxs de Kéfir no mantienen registro de direcciones IP, únicamente de visitas anonimizadas, los cuales borramos después de una semana.

La plataforma de DocRed guarda estadísticas a través de https://sinapssis.kefir.red, una instalación propia de Kéfir de [Piwik/Matomo](https://matomo.org/). Esto implica que sólo IWPR y Kéfir tienen acceso a estos datos. Hemos configurado Matomo para que no registre datos que pueda identificar a lxs navegantes, como las direcciones IP. Todos las visitas individuales se agregan para creatos datos genrealizados y se eliminan después de un mes. Matomo respeta la funcionalidad "Do-Not-Track" (no me rastrees) del navegador si unx navegante si no quiere que Matomo recolecte los datos no-personales de su visita.

### Javascript

La plataforma documentalmexicano.org y sutty.kefir.red utiliza Javascript para mostrarte descripciones de ciertos elementos al colocar el cursor sobre ellos (tooltips) y para la funcionalidad de poder seleccionar opciones en los formularios.

No rastreamos a las navegantes.

Si deshabilitas javascript (por ejemplo, usando Tor Browser Bundle o cambiando la configuración de tu navegador), el sitio seguirá funcionando. En pantallas pequeñas, el menú aparecerá en el pie de página.

### Cambios en esta política de datos

Este documento puede ser actualizado en el futuro. Regresa a esta página para ver cambios.

### Contacto

Puedes remitir cualquier pregunta o duda relacionada con esta política de uso de datos a privacidad@documentalmexicano.org

## Qué puedes hacer

Tú también puedes contribuir a tu privacidad. El hecho que, de nuestra parte, no recolectamos datos sin tu consentimiento, que cuando lo hacemos es por un tiempo limitado y de manera anonimizada y que no los compartimos con terceros más allá de dar un marco general por fines de rendición de cuentas a nuestras financiadoras, no quiere decir que haya otros intermediarios que estén vulnerando tu privacidad.

* Lee documentación en torno a seguridad digital, por ejemplo, [la currícula de Ciber Mujeres](https://cyber-women.com/es/#modulos) e implementa prácticas más seguras ;) 
* Instala el plugin [Privacy Badger](https://www.eff.org/privacybadger/) en tu navegador web
* [Configura tu navegador Firefox para deshabilitar el rastreo de navegación](https://support.mozilla.org/en-US/kb/how-do-i-turn-do-not-track-feature)


### Cómo puedo cambiar mi configuración de cookies

La mayoría de los navegadores web te dan un poco de control sobre la mayoría de los cookies a través de la configuración del navegador. Para aprender sobre cookies, incluyendo cómo ver las que se habilitan y no, entra en [https://aboutcookies.org](https://aboutcookies.org) o [http://www.allaboutcookies.org/](http://www.allaboutcookies.org/)

Averigua cómo manejar cookies en los navegadores más conocidos:

* [Google Chrome](https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=en);
* [Microsoft Edge](https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy);
* [Mozilla Firefox](https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences);
* [Microsoft Internet Explorer](https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies);
* [Opera](https://www.opera.com/help/tutorials/security/privacy/);
* [Apple Safari](https://support.apple.com/kb/ph21411?locale=en_US).

Para más información relacionada con otros navegadores, visita el sitio web del equipo desarrollador del navegador. Para deshabilitar el rastreo de las Analíticas de Google, entra en [https://tools.google.com/dlpage/](https://tools.google.com/dlpage/).

