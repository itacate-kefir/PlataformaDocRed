---
title: "Semana santa indígena en Suchitlán\t\t\t\t\t\t"
poster: "/public/images/489638f9-6275-40d8-9219-5b21421f3f03.jpg"
stills:
- "/public/images/fd515092-8027-413d-8469-371eecac238b.jpg"
- "/public/images/fd515092-8027-413d-8469-371eecac238b.jpg"
- "/public/images/fd515092-8027-413d-8469-371eecac238b.jpg"
pais: México
estado: Colima
municipio: Colima
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '50'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- Fiesta
- tradiciones
- semana santa
- celebración
- liturgia
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - colima
sinopsis_espaniol: "El documental muestra la forma en que Suchitlán, una de las tres
  comunidades indígenas del estado de Colima, lleva a cabo su tradicional festejo
  de semana santa de manera independiente a las celebraciones litúrgicas, la población
  se organiza para realizar rituales en torno a la muerte y resurrección de Jesucristo,
  realizando de manera artesanal hermosos y coloridos adornos de papel para celebrar
  el triunfo Cristo ante la muerte.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: Festival Pantalla de Cristal
  año_de_edicion: '2013'
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: Festival Pantalla de Cristal
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Bogart Rodríguez
  produccion:
  - Bogart Rodríguez / Alejandra Santana
  guion:
  - Bogart Rodríguez
  fotografia:
  - Bogart Rodríguez / Alejandra Santana
  sonido:
  - Bogart Rodríguez
  edicion:
  - Bogart Rodríguez / Alejandra Santana
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Bogart Rodríguez\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-04-22'
  telefono_fijo:
  - "(045) 3123199255"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(045) 3125508283"
  telefono_celular_publico: 'false'
  correo_electronico: bogart22@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Productor(a)
  nombre: Alejandra Santana
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1986-05-24'
  telefono_fijo:
  - "(045) 3123199255"
  telefono_fijo_publico: ''
  telefono_celular:
  - "(045) 3125508283"
  telefono_celular_publico: ''
  correo_electronico: alesantamo@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Bogart Rodríguez / Alejandra Santana\t\t\t\t\t"
  telefono: "(045) 3125508283\t\t"
  correo_electronico: bogart22@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 100-300 mil pesos
coproductores:
- coproductor: "Secretaría de Cultura del Gobierno de Colima\t\t\t\t\t"
  pais: México
  estado: Colima
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: "Semana santa indígena en Suchitlán\t\t\t\t\t\t"
order: 56
---

