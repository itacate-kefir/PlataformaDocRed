---
title: "Entre la tierra y la neblina\t\t\t\t\t\t"
poster: "/public/images/23f90dd0-5ace-4249-94a9-20e6371bb49b.jpg"
stills:
- "/public/images/0259a342-3013-408a-b132-015b6bb1656a.jpg"
- "/public/images/c9d6827d-6a01-4861-9f1a-b440e6258c25.jpg"
- "/public/images/5c6ed45c-33ac-4d23-b1cf-3118a2117a32.jpg"
- "/public/images/052815d1-ccc3-4681-bb19-5cf9f584b243.jpg"
- "/public/images/9d50ecc8-e340-4306-930c-1bdcaebb7022.jpg"
pais: México
estado: Morelos
municipio: Temixco
año_de_inicio_de_la_produccion: '2010'
año: '2009'
duracion: '80'
idioma_original:
- Español
- Tlapaneco
idiomas_de_los_subtitulos:
- Español
formatos:
- DVD
- Archivo digital
- MiniDV
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Pueblos indigenas
- Policias comunitarias
- impartición de justicia
- comunidades Indígenas
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Guerrero
sinopsis_espaniol: " Amador, un joven indígena Tlapaneco trabaja como Comandante de
  la Policía Comunitaria y forma parte de la figura jurídica de la Coordinadora Regional
  de Autoridades Comunitarias (CRAC). Amador pasa sus días entre la sierra y la costa
  supervisando el buen funcionamiento del sistema de impartición de justicia que tiene
  más de 10 años de haberse establecido en los pueblos de la región. Amador nos explica
  como las comunidades indígenas, mestizas y afromestizas unen fuerzas."
sinopsis_ingles: "Among earth and fog the in the villages of Guerrero, justice has
  been sowed in mexico’s soudheast . Amador, a native young Tlapaneco , works as Commander
  in the communitarian police, he monitors the tasks derived from the justice impartition
  system developed in the region by the communities since more than 10 years. Amador
  in his daily life explain how the different ethnic groups makes force to harvest
  future.\t\t\t\t\t\t\t\r\n"
link_al_trailer: https://www.youtube.com/watch?v=dKaVLfhLOEo
link_para_visionado: https://www.youtube.com/watch?v=dKaVLfhLOEo
festivales:
- festival: Festival Internacional de Cine Documental de la Ciudad de México
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pablo Gleason González
  produccion:
  - Pablo Gleason González
  casa_productora:
  - Push and Play
  guion:
  - Pablo Gleason González
  investigacion:
  - Pablo Gleason González
  fotografia:
  - Pablo Gleason González
  sonido:
  - Pablo Gleason González
  edicion:
  - Pablo Gleason González
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Pablo Gleason González\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1978-04-11'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '00000000000'
  telefono_celular_publico: 'false'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Pablo Gleason González\t\t\t\t\t"
  telefono: '0000000000'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "Entre la tierra y la neblina\t\t\t\t\t\t"
order: 44
---

