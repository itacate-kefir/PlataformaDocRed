---
title: Del huipil a la chilaba
poster: "/public/images/cfabc9dc-83b6-42f6-b98a-0419cd12ac65.png"
stills:
- "/public/images/322d1b5c-42e0-49d2-8cc9-e3d5ceca1405.png"
pais: México
estado: Puebla
municipio: Cholula
año_de_inicio_de_la_produccion: '2005'
año: '2005'
duracion: '21'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- Chiapas
- Huipil
- Musulmanes
- Tzotziles
- alá
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Chiapas
sinopsis_espaniol: "En las montañas del sureste mexicano se escucha un canto en árabe,
  es el llamado a la oración al que cientos de indígenas tzotziles acuden para alabar
  a Alá.\r\nEn Chiapas, esta comunidad de musulmanes - tzotziles han cambiado el huipil
  por la chilaba."
link_para_visionado: https://www.youtube.com
festivales:
- festival: Festival Internacional de Cine de Guanajuato
  año_de_edicion: '2006'
- festival: Contra el silencio todas las voces
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Gabriela Domínguez Ruvalcaba
  produccion:
  - Gabriela Domínguez Ruvalcaba
  casa_productora:
  - Bosque negro
  guion:
  - Gabriela Domínguez Ruvalcaba
  investigacion:
  - Gabriela Domínguez Ruvalcaba
  - Gaspar Morquecho
  fotografia:
  - Pedro Jiménez
  sonido:
  - Nallely Cáceres
  edicion:
  - Enrique Zapata
  musica_original:
  - Enrique Zapata
  otros:
  - nombre: Enrique Zapata
    rol: Postproducción de Imagen
  - nombre: Karina Luna
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Gabriela Domínguez Ruvalcaba
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - 2221 186 430
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2221 186 430
  telefono_celular_publico: 'false'
  correo_electronico: gabsdomru@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Gabriela Domínguez Ruvalcaba
  telefono: 2221 186 430
  correo_electronico: gabsdomru@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: Del huipil a la chilaba
order: 75
sinopsis_ingles: "In the mountains of the Mexican southeast a song is heard in Arabic,
  it is the call to prayer to which hundreds of Tzotzil Indians come to praise Allah.\r\nIn
  Chiapas, this community of Tzotzil-Muslims has changed the huipil for the djellaba."
---

