---
title: Mirar, cantar, marchar
poster: "/public/images/5d6256d9-ba8e-4b81-b198-2bbe43c2ca85.png"
stills:
- "/public/images/7389940b-a5ee-46c4-8229-8a4c2b068cb0.png"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '17'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- PRI
- Marcha
- Política
- gobierno
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: El PRI ha vuelto a ser el partido que gobierna al país, con más
  de la mitad de los votantes en contra, México alza la voz. Este es un registro de
  los que marchan, los que observan y los que cantan.
sinopsis_ingles: The PRI has once again been the part that governs the country, with
  more than half of the voters against, Mexico raises its voice. This is a record
  of those who march, those who observe and those who sing
link_para_visionado: https://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Gabriela Domínguez Ruvalcaba
  produccion:
  - Gabriela Domínguez Ruvalcaba
  casa_productora:
  - Bosque Negro
  guion:
  - Gabriela Domínguez Ruvalcaba
  fotografia:
  - Gabriela Domínguez Ruvalcaba
  sonido:
  - Gabriela Domínguez Ruvalcaba
  edicion:
  - Gabriela Domínguez Ruvalcaba
  otros:
  - nombre: Gabriela Domínguez Ruvalcaba
    rol: Postproducción de Imagen
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Gabriela Domínguez Ruvalcaba
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - 2221 186 430
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2221 186 430
  telefono_celular_publico: 'false'
  correo_electronico: gabsdomru@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Gabriela Domínguez Ruvalcaba
  telefono: 2221 186 430
  correo_electronico: gabsdomru@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Mirar, cantar, marchar
order: 55
---

