---
title: El destino de la piel
poster: "/public/images/9a496ca9-61db-4311-b94b-bf5a24539806.jpg"
stills:
- "/public/images/49262c8c-d1d2-4c8a-a343-0493997930cf.jpg"
- "/public/images/cefe16b8-3bb3-48a7-a557-4fae93131d41.jpg"
- "/public/images/c0c2142e-1bfe-471c-a75f-f73c7e18eb3f.jpg"
pais: México
estado: Baja California Norte
municipio: Tijuana
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '10'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- Violencia
- frontera
- Found footage
genero_y_estilo:
- 'Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción
  del mismo para proponer nuevas formas fílmicas'
- 'Falso documental: Película que documenta una falsa hipótesis.'
locaciones:
  paises:
  - México
  estados:
  - Baja California Norte
sinopsis_espaniol: Skin Destination plantea una revisión al estado de emergencia que
  se vive en México a partir  del cuerpo como agente, utilizando la recuperación y
  reapropiación de archivo. En Skin Destination transcurren y se cruzan de manera
  fronteriza (al igual que en la ciudad de Tijuana) diferentes formatos, estrategias
  narrativas y de puesta en escena.
sinopsis_ingles: Skin Destination proposes a review of the state of emergency that
  exists in Mexico from the body as an agent, using the recovery and reappropriation
  of the file. In Skin Destination, different formats, narrative and staging strategies
  take place and cross border (as in the city of Tijuana).
link_al_trailer: https://www.youtube.com/watch?v=dEGPzRTvpdU
link_para_visionado: https://www.youtube.com/watch?v=dEGPzRTvpdU
festivales:
- festival: Festival Internacional de Cine de Morelia
  año_de_edicion: '10 '
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Adriana Trujillo
  produccion:
  - José Inerzia
  casa_productora:
  - Polen Audiovisual
  guion:
  - Adriana Trujillo
  investigacion:
  - Adriana Trujillo
  fotografia:
  - José Inerzia
  sonido:
  - José Inerzia
  edicion:
  - Adriana Trujillo
  musica_original:
  - Paul Morales
directorxs:
- rol:
  - Productor(a)
  nombre: José Inerzia
  nacionalidad: Española
  fecha_de_nacimiento: '1980-02-01'
  telefono_fijo:
  - '6647008192'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6646699378'
  telefono_celular_publico: 'false'
  correo_electronico: inerzia@polenaudiovisual.org
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Adriana Trujillo
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1982-01-02'
  telefono_fijo:
  - '6647008192'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6643295166'
  telefono_celular_publico: 'false'
  correo_electronico: adriana@polenaudiovisual.org
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Iván Díaz Robledo
  telefono: 664-7008192
  correo_electronico: info@polenaudiovisual.org
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Skin destination
order: 40
---

