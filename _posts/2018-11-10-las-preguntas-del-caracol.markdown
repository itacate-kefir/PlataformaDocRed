---
consentimiento: 'true'
titulo: Las preguntas del caracol
poster: "/public/images/31913667-4da8-45f1-95ea-af68fe0e1291.jpg"
stills:
- "/public/images/9185103a-6f00-4de8-aabd-e1e58e0b123d.jpg"
sitio_web_de_la_pelicula: https://www.facebook.com/doculaspreguntasdelcaracol/
pais: México
estado: Jalisco
municipio: Guadalajara
año_de_inicio_de_la_produccion: '1998'
año: '2011'
duracion: '94'
idioma_original:
- Español
- Catalan
idiomas_de_los_subtitulos:
- Inglés
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Chiapas
- EZLN
- Movimientos Sociales
genero_y_estilo:
- 'De autor /De creación: Su apuesta se basa en la mirada personal del autor.'
locaciones:
  paises:
  - México
  - España
  estados:
  - Chiapas
  - Ciudad de México
sinopsis_espaniol: Teníamos veinte años y cambiar el mundo estaba pasado de moda.
  De pronto, el alzamiento de un ejército indígena irrumpió en nuestras vidas y como
  miles de jóvenes de todo el mundo acudimos a su llamado y viajamos a Chiapas. Lo
  que inició como un viaje de solidaridad se transformó en un cuestionamiento a nuestro
  mundo y nuestras propias contradicciones. Pronto descubrimos que aquella travesía
  no terminaría al regresar a nuestra ciudad. La película es un viaje, un viaje que
  nos ha cambiado la vida
link_al_trailer: https://www.youtube.com/watch?v=r9HSIKbEXtU
link_para_visionado: https://vimeo.com/288099500
link_para_visionado_publico: 'true'
festivales:
- festival: Contra el Silencio de todas las Voces
  año_de_edicion: '2011'
- festival: Muestra Internaiconal de Cine Documental DocsTown
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  entidad: Ninguna
becas_y_apoyos:
- premio: Ninguno
  monto: '50000'
  entidad: Universidad de Guadalajara
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Afra Mejia
  produccion:
  - Araceli Velazquez
  casa_productora:
  - Neo O'Kay producciones
  - Martini Shot
  guion:
  - Afra Mejia
  fotografia:
  - Afra Mejia
  sonido:
  - Airy Mejia
  - Ines Mestre
  - Berenice Morales
  - Natalia Zamudio
  - Rocio Rea
  edicion:
  - Carlos Cardenas Aguilar
  musica_original:
  - Santiago Maisterra
  - Aldo Acuña
  otros:
  - nombre: Alexis de la Peña
    rol: Animación
realizadorxs:
- rol:
  - Director(a)
  nombre: Afra Mejía
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1943-07-26'
  telefono_fijo:
  - '3331216990'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '3312807455'
  telefono_celular_publico: 'false'
  correo_electronico: afra.citlalli@gmail.com
  correo_electronico_publico: 'false'
  grados_academicos:
  - grado_academico: Maestría
    escuela: Máster en Documental de Creació / Universidad Pompeu Fabra
  - grado_academico: Maestría
    escuela: Instituto Tecnológico y de Estudios Superiores de Occidente
  - grado_academico: Licenciatura
    escuela: Instituto Tecnológico y de Estudios Superiores de Occidente
- rol:
  - Productor(a)
  nombre: Araceli Velásquez
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '3331216990'
  telefono_fijo_publico: ''
  telefono_celular:
  - '3312807455'
  telefono_celular_publico: ''
  correo_electronico: araceli.martinishot@gmail.com
  correo_electronico_publico: ''
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Afra Mejía
  telefono: 013312807455
  correo_electronico: afra.citlalli@gmail.com
  sitio_web: https://www.facebook.com/doculaspreguntasdelcaracol/
ventanas_donde_se_ha_exhibido:
- Salas de cine culturales y/o independientes
- Televisión cultural
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
- Escuelas y universidades
licencia_de_uso: 'Atribución-CompartirIgual-NoComercial: Igual que BY-SA y BY-ND'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: Universidad de Guadalajara
  pais: México
  estado: Guadalajara
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '0'
recaudacion: De 0 a mil pesos
layout: ficha_documental
title: Las preguntas del caracol
author: afra.citlalli@gmail.com
order: 83
---

