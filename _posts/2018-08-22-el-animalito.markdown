---
title: El animalito
poster: "/public/images/591fe7fb-ace7-4c1b-8405-90d7e816b6b3.jpg"
stills:
- "/public/images/49482fc5-787e-476d-a483-a4f2c1eb84d7.jpg"
- "/public/images/49482fc5-787e-476d-a483-a4f2c1eb84d7.jpg"
- "/public/images/49482fc5-787e-476d-a483-a4f2c1eb84d7.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '13'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Estudiantil: Producido en el marco de una institución educativa, escuela o universidad'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- Huamantla
- Noviembre
- otoño
- fieles difuntos
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - tlaxcala
sinopsis_espaniol: Cada otoño, en la fecha marcada por el 1 de noviembre en todo rincón
  de México se celebra a los muertos, particularmente durante este día en una comunidad
  de Huamantla (Tlaxcala) El animalito ronda por el pueblo para mantener despiertos
  a los pobladores y de esta manera poder esperar a sus fieles difuntos.
sinopsis_ingles: "Every autumn, on the date marked by November 1 in every corner of
  Mexico, the dead are celebrated, particularly\r\nduring this day in a community
  of Huamantla (Tlaxcala) El animalito roams the town to keep the\r\nsettlers and
  in this way to be able to wait for their faithful departed."
link_al_trailer: http://www.youtube.com/watch?v=tiCwwo2wGis
link_para_visionado: http://www.youtube.com/watch?v=tiCwwo2wGis
festivales:
- festival: Festival de Cine y Video Indígena
  año_de_edicion: '2013'
- festival: Festival Internacional de Cortometrajes El Vagon
  año_de_edicion: '2013'
- festival: Festival de cine México Alemania
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Maximiliano Valdes Lima
  produccion:
  - Maximiliano Valdes Lima
  casa_productora:
  - ICE Instituto de Comunicación Especializada A.C.
  guion:
  - Maximiliano Valdes Lima
  investigacion:
  - Maximiliano Valdes Lima
  fotografia:
  - Maximiliano Valdes Lima
  sonido:
  - Maximiliano Valdes Lima
  edicion:
  - Maximiliano Valdes Lima
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Maximiliano Valdes Lima
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1992-11-11'
  telefono_fijo:
  - '2223693569'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2223693569'
  telefono_celular_publico: 'false'
  correo_electronico: max_terattzi@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Maximiliano Valdes Lima
  telefono: '2223693569'
  correo_electronico: max_terattzi@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Televisión cultural
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '50'
  propia: '50'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: El animalito
order: 50
---

