---
title: Agrimensura y Cartografía en México, 1720-1920
poster: "/public/images/71d2b469-5df9-473c-bef8-0b383f7a26b9.BMP"
stills:
- "/public/images/70a3f772-fa4a-4595-93d6-09c094cd6478.BMP"
sitio_web_de_la_pelicula: "https://www.ceiich.unam.mx\t\t\t\t\t"
pais: México
estado: Ciudad de México
municipio: Coyoacán
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '72'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Ciencia y tecnología: Descubrimientos y avances científicos, tecnológicos o médicos'
palabras_clave:
- Moda Lid
- Etapas del territorio mexicano
- topográfico
- cartografía
- geografía
genero_y_estilo:
- 'De investigación: El hilo conductor se sustenta en la investigación de un tema.'
locaciones:
  paises:
  - México
  estados:
  - Ciudad de México
sinopsis_espaniol: Las formas de vida. Los espacios, la maquinaria, los vestigios
  y la imagen de la hacienda mexicana desde la época colonial hasta la actualidad
  encuentran en Herbert J. Nickel, un apasionado estudioso intérprete. En esta ocasión,
  el autor nos acerca de manera sintética y atractiva a las diversas etapas de la
  representación del territorio mexicano, a las técnicas de levantamiento topográfico,
  así como a los géneros de visualización cartográfica, mezclando rigor analítico
  y asombro estético.
link_al_trailer: http://livestre.am/1hdkj
link_para_visionado: http://livestre.am/1hdkj
contrasena_para_visionado: catalogo en desarrollo
festivales:
- festival: Festival Internacional de Guadalajara
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Victor Manuel Méndez Villanueva
  produccion:
  - Victor Manuel Méndez Villanueva
  - Sara García
  casa_productora:
  - CEIICH
  guion:
  - Herbert J. Nickel
  - Sergio Niccolai
  investigacion:
  - Herbert J. Nickel
  fotografia:
  - Herbert J. Nickel
  sonido:
  - Victor Manuel Méndez Villanueva
  - Sara García
  edicion:
  - Sara García Méndez
  otros:
  - nombre: Victor Manuel Méndez Villanueva
    rol: Postproducción de imagen y sonido
  - nombre: Ariadna Gallardo Campos
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Victor Manuel Méndez Villanueva
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1973-01-10'
  telefono_fijo:
  - "(52 55) 5623 0206"
  telefono_fijo_publico: 'true'
  telefono_celular:
  - 044 5517015755
  telefono_celular_publico: 'false'
  correo_electronico: vicmend@yahoo.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Victor Manuel Méndez Villanueva
  telefono: "(52 55) 56230206"
  correo_electronico: vicmend@yahoo.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Salas de cine culturales y/o independientes
- Televisión cultural
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
- Escuelas y universidades
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: " CEIICH - UNAM"
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '80'
  privada: '0'
  propia: '20'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Agrimensura y Cartografía en México, 1720-1920
order: 76
---

