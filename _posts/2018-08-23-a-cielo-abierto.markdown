---
title: A cielo abierto
poster: "/public/images/16eca6cc-50ec-44c5-bc43-d67e0053ce8d.BMP"
stills:
- "/public/images/c44cd0dc-0cad-408a-a8f5-0302195bddf6.BMP"
sitio_web_de_la_pelicula: https://www.youtube.com
pais: México
estado: Ciudad de México
municipio: Coyoacán
año_de_inicio_de_la_produccion: '2014'
año: '2014'
duracion: '59'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
tipo_de_produccion:
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
palabras_clave:
- mineria
- devastación
- despojo
- contaminación
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Chiapas
  - Veracruz
  - sonora
  - San Luis Potosí
  - Morelos
  - CDMX
  - Zacatecas
sinopsis_espaniol: Los graves impactos de la minería a cielo abierto han sido poco
  advertidos. El despojo, devastación de la tierra, la contaminación del agua y del
  aire, los daños a la salud, la descomposición del tejido social, represión y asesinatos
  son resultado del incremento voraz en el consumo de metales preciosos, que en gran
  medida tienen el objetivo banal de hacer joyas.
link_para_visionado: http:no la tengoaun
contrasena_para_visionado: catalogo en desarrollo
festivales:
- festival: Contra el Silencio de todas las Voces
  año_de_edicion: '2014'
- festival: Festival Internacional de Guadalajara
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Victor Manuel Méndez Villanueva
  produccion:
  - Victor Manuel Méndez Villanueva
  - Gian Carlo Delgado Ramos
  - Wendy Ariadna Gallardo Campos
  - Sara García  Méndez
  - Polette Rivero Villaverde
  casa_productora:
  - CEIICH - UNAM
  guion:
  - Victor Manuel Méndez Villanueva
  investigacion:
  - Gian Carlo Delgado Ramos
  fotografia:
  - Victor Manuel Méndez Villanueva
  sonido:
  - Victor Manuel Méndez Villanueva
  - Wendy Ariadna Gallardo Campos
  edicion:
  - Victor Manuel Méndez Villanueva
  musica_original:
  - Oscar Millán
  - Victor Manuel Méndez Villanueva,
  otros:
  - nombre: Wendy Ariadna Gallardo Campos
    rol: Animación
  - nombre: Miriam Lucero González Bucio
    rol: Animación
  - nombre: Aura Correa Faviel
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Victor Manuel Méndez Villanueva
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1973-01-10'
  telefono_fijo:
  - "(52 55) 56230206"
  telefono_fijo_publico: 'true'
  telefono_celular:
  - 044 5517015755
  telefono_celular_publico: 'false'
  correo_electronico: vicmend@yahoo.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Victor Manuel Méndez Villanueva
  telefono: "(52 55) 56230206"
  correo_electronico: victormm@unam.mx
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Televisión cultural
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '100'
  privada: '0'
  propia: '0'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: A cielo abierto
order: 70
---

