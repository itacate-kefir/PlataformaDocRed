---
title: Nadie es libre
poster: "/public/images/dd09f5f9-d9ac-4c0c-ada3-3baabc2fdadf.JPG"
stills:
- "/public/images/8c3c6e6a-022a-4549-bbd4-77a75fd5c47f.JPG"
- "/public/images/dc1aa4f5-2ba8-4be6-b5a6-adbcfbf422fa.JPG"
- "/public/images/219163a1-8370-4995-b48b-c8e688c4ddd7.JPG"
pais: México
estado: Coahuila
municipio: Saltillo
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '109'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
- 'Género y sexualidad: Identidad, diversidad, prácticas y cuidados de la sexualidad'
palabras_clave:
- Identidad
- Activismo social
- Derechos humanos,
- Homosexualidad
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Coahuila
  - Ciudad de México
sinopsis_espaniol: 'Nancy Cárdenas, la primera activista por los derechos de los homosexuales
  en México. Intelectuales y activistas del México actual se cuestionan sobre el legado
  de Nancy en una sociedad en la que si bien ya se permiten los matrimonios entre
  personas del mismo sexo hay todavía muchos obstáculos que coartan ese acto de libre
  albedrío que nos remite al lema que utilizó la también dramaturga para su activismo:
  Nadie es libre, hasta que todos seamos libres.'
sinopsis_ingles: 'Mexico, the late 60s. The nation faces juvenile riots and Nancy
  Cardenas, playwright and stage director, initiates as a gay activist fighting for
  their rights. A struggle wrapped by her motto: Nobody is free until we all are free.
  Friends, actors, intellectuals and personalities from Mexico share their thougths
  about Nancy Cardenas´s legacy in modern Mexico, where same sex marriage is a reality
  but still many wonder: are we still free?'
link_al_trailer: https://www.youtube.com/watch?v=1Xn4Ns15J1I
link_para_visionado: https://www.youtube.com/watch?v=1Xn4Ns15J1I
festivales:
- festival: Festival MIX, México DF.
  año_de_edicion: XVII (2013)
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Los Angeles Movie Awards 2011
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Alfredo de Jesús Galindo Gómez
  produccion:
  - Alfredo de Jesús Galindo Gómez
  casa_productora:
  - Leo Media Productions S.A. de C.V
  guion:
  - Alfredo de Jesús Galindo Gómez
  investigacion:
  - Alfredo de Jesús Galindo Gómez
  fotografia:
  - Ross Rodríguez
  sonido:
  - Carlos Mesta
  edicion:
  - Orlando Sifuentes
  musica_original:
  - Angel Villarreal
  otros:
  - nombre: Pablo Flores
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Alfredo de Jesús Galindo Gómez
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1967-08-16'
  telefono_fijo:
  - "(844) 415 84 20"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(844) 415 84 20"
  telefono_celular_publico: 'false'
  correo_electronico: alfredogalindo@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Alfredo de Jesús Galindo Gómez
  telefono: "(844) 415 84 20"
  correo_electronico: alfredogalindo@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Nadie es libre
order: 13
---

