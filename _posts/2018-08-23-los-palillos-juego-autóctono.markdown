---
title: "Los palillos, juego autóctono\t\t\t\t\t\t"
poster: "/public/images/82df4022-5800-487d-9a31-e8c4ce3b94ff.jpg"
stills:
- "/public/images/2156a606-b52f-4546-98f7-57029e0a5eb3.jpg"
- "/public/images/2156a606-b52f-4546-98f7-57029e0a5eb3.jpg"
- "/public/images/2156a606-b52f-4546-98f7-57029e0a5eb3.jpg"
pais: México
estado: Colima
municipio: Colima
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '26'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- Cultura
- juegos
- tradición
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - colima
sinopsis_espaniol: "El video documental muestra la práctica de un juego que pierde
  sus orígenes en el pasar de los años, se trata de un juego autóctono de destreza
  mental que actualmente sólo se practica en un ejido que lleva por nombre La Caja,
  en el municipio de Comala, Colima, México.  El juego se practica con herramientas
  que provee la misma naturaleza, el elemento principal son cuatro palitos de madera
  con una marca diferente cada uno, los cuales fabrican los mismos jugadores.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: Festival Pantalla de Cristal
  año_de_edicion: '2012'
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: ''
- festival: Festival Internacional de Cine y Video Indígena Mirando desde nuestra
    raíz
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: Festival Pantalla de Cristal
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Alejandra Santana
  produccion:
  - Alejandra Santana  /  Bogart Rodríguez
  guion:
  - Alejandra Santana
  fotografia:
  - Alejandra Santana  /  Bogart Rodríguez
  sonido:
  - Alejandra Santana  /  Bogart Rodríguez
  edicion:
  - Alejandra Santana  /  Bogart Rodríguez
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: 'Alejandra Santana '
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1986-05-24'
  telefono_fijo:
  - "(045) 3123199784"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(045) 3125508283"
  telefono_celular_publico: 'false'
  correo_electronico: alesantamo@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Alejandra Santana
  telefono: "(045) 3125508283\t\t"
  correo_electronico: alesantamo@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: "Secretaría de Cultura del Gobierno de Colima\t\t\t\t\t"
  pais: México
  estado: Colima
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "Los palillos, juego autóctono\t\t\t\t\t\t"
order: 57
---

