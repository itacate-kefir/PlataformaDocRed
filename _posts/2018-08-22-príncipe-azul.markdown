---
title: Príncipe Azul
poster: "/public/images/611a4a31-4be0-43b8-8efc-d7e202f5de47.jpg"
stills:
- "/public/images/7c9b3de3-ab34-4722-90e2-648952043c8c.jpeg"
- "/public/images/ecfb069d-f223-4449-b329-442508780c75.jpeg"
- "/public/images/7f41f3c6-fa44-43d7-b4c7-839c65fdde8f.jpg"
pais: México
estado: Oaxaca
municipio: Oaxaca
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '16'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
palabras_clave:
- Hombre
- Pierna
- Apoyo
- Enfermedad
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Oaxaca
sinopsis_espaniol: Es la historia del señor Gonzalo Rojas Jiménez. Originario de la
  Finca Mercedes, en Pluma Hidalgo, zona cafetalera del estado de Oaxaca. A la edad
  de un año sufre problemas de poliomielitis, sus padres por temor a las agujas, lo
  escondieron y no fue vacunado, la enfermedad avanzo hasta que su pierna derecha
  fue secándose lo cual tuvo problemas para caminar, a los 5 o 6 años puedo ponerse
  de pie y dar los primeros pasos con dificultad, ya que perdió la fuerza en la pierna
  derecha.
sinopsis_ingles: It is the story of Mr. Gonzalo Rojas Jiménez. Originally from Finca
  Mercedes, in Pluma Hidalgo, coffee zone of the state of Oaxaca. At the age of one
  year he suffers from polio problems, his parents fearing needles, hid him and he
  was not vaccinated, the disease progressed until his right leg went dry, which he
  had trouble walking, at 5 or 6 years old stand up and take the first steps with
  difficulty, as he lost strength in his right leg.
link_para_visionado: https://www.youtube.com
festivales:
- festival: Festival Cinematográfico de Poza Rica Veracruz
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Emmanuel Antonio López
  produccion:
  - Emmanuel Antonio López
  guion:
  - Emmanuel Antonio López
  investigacion:
  - Emmanuel Antonio López
  fotografia:
  - Emmanuel Antonio López
  - Christian Yamurith Gallegos Macario
  sonido:
  - Emmanuel Antonio López
  edicion:
  - Emmanuel Antonio López
  - Christian Yamurith Gallegos Macario
  otros:
  - nombre: Emmanuel Antonio López
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Emmanuel Antonio López
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1986-02-26'
  telefono_fijo:
  - 951 121 0988
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 951 121 0988
  telefono_celular_publico: 'false'
  correo_electronico: emmantloz@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Emmanuel Antonio López
  telefono: 951 121 09 88
  correo_electronico: emmantloz@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Príncipe Azul
order: 29
---

