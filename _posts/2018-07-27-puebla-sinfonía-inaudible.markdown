---
title: Puebla Sinfonía Inaudible
poster: "/public/images/d012a097-fbe1-4511-aeee-ad631ae9adfe.jpg"
stills:
- "/public/images/db3bd021-c3f1-4630-8881-43f6c99b97ad.jpg"
- "/public/images/d24c2d88-29db-4d7c-a81f-a1382a7ce7f3.jpg"
- "/public/images/f12c6706-1bc8-480d-b9bb-b490e3621e82.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2009'
año: '2010'
duracion: '67'
idioma_original: Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Cultura: Tradiciones, patrimonio'
- 'Biográfico e histórico: Sucesos y personajes históricos'
palabras_clave:
- Ciudad
- CineOjo
- Sinfonía
- Puebla
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Documental sensorial: Apuesta por la transmisión de experiencias subjetivas a nivel
  estético y sensorial.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: A las cinco de la madrugada, las calles aún se sienten vacías;
  Puebla parece una ciudad fantasma. Los edificios de oficinas aguardan silenciosos,
  las fábricas en aparente quietud, las iglesias están cerradas, las máquinas dormidas,
  las tiendas con las persianas echadas hasta que, a manera de despertador y recalcando
  la religiosidad profunda de la ciudad, los campanarios repican como si se respondieran
  unos a otros despertando a los habitantes. Aquí empieza la sinfonía.
sinopsis_ingles: It is five o clock in the morning. The streets are empty; Puebla
  seems like a ghost city. The office buildings await silently, the factorys are quite,
  the churchs closed, the machines sleep, untill the bells of the churches ring as
  if all answer each other, and awakes the inhabitants of the city.
link_al_trailer: http://www.youtube.com/watch?v=J_DeMHkNOTA
link_para_visionado: http://www.youtube.com/watch?v=J_DeMHkNOTA
festivales:
- festival: Festival Internacional de Cine de Puebla
  año_de_edicion: '2010'
- festival: Contra el Silencio de todas las Voces
  año_de_edicion: '2011'
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: '2011'
- festival: Festival Internacional de Cine Documental de la Ciudad de México
  año_de_edicion: '2010'
- festival: Festival Internacional Cinematográfico de Madrid El Ojo Cojo
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Premio del Público
  institución: Festival Internacional Cinematográfico de Madrid El Ojo Cojo
becas_y_apoyos:
- beca: Apoyo a la Producción y Postproducción de Cortometraje
  monto: '200000'
  institución: CONACULTA
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Juan Manuel Barreda Ruíz
  produccion:
  - Yaizu Vázquez Aburto
  casa_productora:
  - Juana Films
  guion:
  - Juan Manuel Barreda Ruíz
  - Ana Mary Ramos Rugerio
  investigacion:
  - Juan Manuel Barreda Ruíz
  - Yaizu Vázquez Aburto
  - Ana Mary Ramos Rugerio
  fotografia:
  - Juan Manuel Barreda Ruíz
  - José Miguel Romero
  sonido:
  - Ana Mary Ramos Rugerio
  - Fernando Robredo
  edicion:
  - Juan Manuel Barreda Ruíz
  - Luiz Zerón
  musica_original:
  - Federico González
  - Raskoba
directorxs:
- rol:
  - Productor(a)
  nombre: Yaizu Vázquez Aburto
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1982-02-25'
  telefono_fijo:
  - '2225836830'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2223249163'
  telefono_celular_publico: 'true'
  correo_electronico: izu.vazquez@gmail.com
  correo_electronico_publico: 'true'
  grados_academicos:
  - grado_academico: Licenciatura en Comunicación
    escuela: Buap
- rol:
  - Director(a)
  nombre: Juan Manuel Barreda Ruíz
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1984-11-11'
  telefono_fijo:
  - '2226360746'
  telefono_fijo_publico: ''
  telefono_celular:
  - '2226360746'
  telefono_celular_publico: ''
  correo_electronico: capitan_von_trapp@hotmail.com
  correo_electronico_publico: ''
distribuidoras:
- nombre: Juan Manuel Barreda Ruíz
  nombre_publico: 'false'
  telefono: '2226360746'
  telefono_publico: 'false'
  correo_electronico: capitan_von_trapp@hotmail.com
  correo_electronico_publico: 'true'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Juan Manuel Barreda Ruíz
  telefono: '2226360746'
  correo_electronico: capitan_von_trapp@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 300-500 mil pesos
coproductores:
- coproductor: Juana Films
  pais: México
  estado: Puebla
porcentaje_de_financiacion:
  estatal: '60'
  privada: '10'
  propia: '30'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Puebla Sinfonía Inaudible
order: 3
---

