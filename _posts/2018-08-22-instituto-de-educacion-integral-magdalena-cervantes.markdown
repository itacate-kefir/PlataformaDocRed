---
title: "Instituto de Educacion Integral Magdalena Cervantes\t\t\t\t\t\t"
poster: "/public/images/208dc162-b43b-4279-9705-852a86bb19d3.png"
stills:
- "/public/images/396ccfad-ee01-43a2-866d-2e86632bf4ea.png"
- "/public/images/314b3f99-d773-40be-b29b-25e6e1ba1fcf.png"
- "/public/images/7c75afa1-6e1a-44f8-b510-568f7470a1ed.png"
pais: México
estado: Tlaxcala
municipio: Tlaxcala
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '12'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- escuela
- medio ambiente
- ecología
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - tlaxcala
sinopsis_espaniol: "Documental acerca de una pequeña escuela ecológica ubicada en
  el municipio de Tlaxco, Tlaxcala, Mexico.\t\t\t\t\t\t\t"
sinopsis_ingles: "Documentary about a small ecological school located on Mexico\t\t\t\t\t\t\t"
link_al_trailer: "http://www.youtube.com/watch?v=G7hQo3TrRxg\t\t\t\t\t\t"
link_para_visionado: "http://www.youtube.com/watch?v=G7hQo3TrRxg\t\t\t\t\t\t"
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - René López Caballero
  produccion:
  - René López Caballero
  guion:
  - René López Caballero
  fotografia:
  - René López Caballero
  sonido:
  - René López Caballero
  edicion:
  - René López Caballero
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "René López Caballero\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1980-03-21'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2411171225'
  telefono_celular_publico: 'false'
  correo_electronico: renelc@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "René López Caballero\t\t\t\t\t"
  telefono: "2411171225\t\t\t\t"
  correo_electronico: renelc@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 10-30 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: tlaxcala
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: "Instituto de Educacion Integral Magdalena Cervantes\t\t\t\t\t\t"
order: 25
---

