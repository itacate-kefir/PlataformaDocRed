---
title: Nemi rockercoatl
poster: "/public/images/f0aaae17-d1c1-4525-8f75-08f70278d651.jpg"
stills:
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
- "/public/images/ab2fcd4c-840b-4a81-8044-281353f2dc80.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '42'
idioma_original:
- Español
- Náhuatl
idiomas_de_los_subtitulos:
- Inglés
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Musical: Artistas, agrupaciones, giras, fenómenos musicales'
palabras_clave:
- Rock
- Náhuatl
- Campesino
- Trascender
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: "Retrato de un hombre que hace canciones de rock en náhuatl, lengua
  indígena casi en extinción. Juan Manuel Sánchez, de origen campesino es, al mismo
  tiempo, padre de familia, productor de sus propios discos, esposo…\r\nEl documental
  muestra cómo este personaje lucha por trascender haciendo canciones que pocos entienden."
sinopsis_ingles: "Portrait about a man who makes rock songs in Nahuatl, a native Mexican
  language that is almost extinct. Juan Manuel Sánchez, rural at his beginnigs is,
  at same time, musician and father, producer of his own records and husband…\r\nThis
  documentary shows how this particular character fights for success making songs
  that just a few people understand."
link_al_trailer: https://www.youtube.com/watch?v=D8UzC3sTiY4
link_para_visionado: https://www.youtube.com/watch?v=D8UzC3sTiY4
festivales:
- festival: Festival Internacional de Cine Oaxaca
  año_de_edicion: '2010'
- festival: Muestra Internacional de Cine y Video Indígena  Puebla
  año_de_edicion: '2011'
- festival: Muestra Internaiconal de Cine Documental DocsTown
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Oscar Flores Solano
  produccion:
  - Francisco González Quijano
  casa_productora:
  - Mediátika
  guion:
  - Oscar Flores Solano
  investigacion:
  - Oscar Flores Solano
  fotografia:
  - Oscar Flores Solano
  sonido:
  - Oscar Ulloa Durand
  edicion:
  - Oscar Flores Solano
  musica_original:
  - Juan Manuel Sánchez "Rockercoatl"
  otros:
  - nombre: Oscar Flores Solano
    rol: Postproducción de Imagen
  - nombre: Oscar Ulloa Durand
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Director(a)
  nombre: Oscar Flores Solano
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1976-07-21'
  telefono_fijo:
  - '2221697356'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2222437918'
  telefono_celular_publico: 'false'
  correo_electronico: ossols@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Oscar Flores Solano
  telefono: '12221697356'
  correo_electronico: ossols@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: Nemi rockercoatl
order: 71
---

