---
title: Contigo al norte, Guadalupe
poster: "/public/images/810c7aac-d12c-4305-bebf-22665a5cdb69.jpg"
stills:
- "/public/images/db154770-b889-4356-862c-93a38fbfa5af.jpg"
- "/public/images/43c21da9-0006-4a28-8f80-cdd3054e6024.jpg"
- "/public/images/f5a3eb96-5c6d-472c-800d-db28275648f0.jpg"
- "/public/images/33f12b09-577c-40e8-8ff9-520ce67fc757.jpg"
- "/public/images/030cfaec-8937-4d74-a98a-cdc67bbda86b.jpg"
- "/public/images/7523e97f-1ed8-4b40-8faa-66ea0facfeea.jpg"
- "/public/images/a534e71b-8877-4ea0-aee8-448c25040dd4.jpg"
- "/public/images/65c9889c-1461-4d6b-adbb-444c1b060e57.jpg"
- "/public/images/6094b96c-c6bd-442e-afd6-92f6d4effde4.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '101'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
- MiniDV
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- Migrantes
- indocumentado
- carrera
- virgen
- guadalupe
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Ciudad de México
sinopsis_espaniol: Martín Luna, un mexicano indocumentado que vive enNueva York, regresa
  a México para encabezar una carrera desde la Basílica de Guadalupe hasta la Catedral
  de San patricio, llevando una imagen de la virgen de Guadalupe y una antorcha a
  manera de reclamo por los derechos de los trabajadores indocumentados en Estados
  Unidos. El recorrido nos muestra el éxodo migrante en el rostro de Martín, quien
  de nuevo tendrá que enfrentar el cruce ilegal de la frontera y la incertidumbre
  por su futuro.
sinopsis_ingles: Martin Luna an undocumented Mexican immigrant living in New York
  returns to Mexico to lead a race that goes from the Basilica of Guadalupe in Mexico
  City up to the St. Patrick´s Cathedral in New York, carrying an image of the Virgin
  of Guadalupe and a torch, as a way of enforcing the rights of undocumented workers
  in the United States. His way to the north shows the plight of immigrants in the
  person of Martin, who will face again the illegal crossing of the border and the
  uncertainty about his.
link_al_trailer: https://www.youtube.com/watch?v=ATbthHD0nx0
link_para_visionado: https://www.youtube.com/watch?v=ATbthHD0nx0
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Melchor Morán
  produccion:
  - Sergio Mastretta
  casa_productora:
  - Fundación para la Investigación Social y Ambiental de México, S.C.
  guion:
  - Melchor Morán
  - Sergio Mastretta
  investigacion:
  - Sergio Mastretta
  fotografia:
  - Melchor Morán
  sonido:
  - Ives de Vataire
  edicion:
  - Melchor Morán
  - Leonardo Rodriguez
  musica_original:
  - Christian Petersen
  otros:
  - nombre: Ing. Julián Lezama
    rol: Postproducción de imagen
  - nombre: Ing. Daniel Sánchez
    rol: Postproducción de sonido
directorxs:
- rol:
  - Productor(a)
  nombre: Sergio Mastretta Guzmán
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1955-01-11'
  telefono_fijo:
  - 22 24 52 34 80
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 22 24 52 34 80
  telefono_celular_publico: 'false'
  correo_electronico: smastret@yahoo.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: " Melchor Morán Mayllen"
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - 222 2304400
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 222 2304400
  telefono_celular_publico: 'false'
  correo_electronico: melchor@ensamblaideas.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Sergio Mastretta Guzmán
  telefono: "22 24 52 34 80\t"
  correo_electronico: smastret@yahoo.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Contigo al norte, Guadalupe
order: 46
---

