---
title: El caminar por la vigencia del derecho a la consulta de los pueblos
poster: "/public/images/dcbba0c8-2a4b-47ce-83f0-60b8ef595f45.jpg"
stills:
- "/public/images/c136a92f-f5e9-49cc-bc70-a47b0457dded.jpg"
pais: México
estado: Guerrero
municipio: Chilpancingo
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '37'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- Indígenas
- Resistencia social
- Derecho a la consulta
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Guerrero
sinopsis_espaniol: 'En el año 2007, la Comisión Nacional para el desarrollo de los
  Pueblos Indígenas (CDI), quiso imponer a un delegado sin  consultar a los pueblos
  originarios de Guerrero, tal como lo dicta el Art. 2º de nuestra carta magna, el
  convenio 169 de la OIT y la Declaración de Naciones Unidas, sobre los derechos de
  los Pueblos Indígenas. Ante esto, los pueblos y organizaciones indígenas se organizaron
  y lucharon para detener esta imposición. '
sinopsis_ingles: In 2007, the National Commission for the Development of Indigenous
  Peoples (CDI), wanted to impose a delegate without consulting the indigenous peoples
  of Guerrero, as dictated by Article 2 of our constitution, Convention 169 of the
  ILO and the United Nations Declaration on the Rights of Indigenous Peoples. Given
  this, the indigenous peoples and organizations organized and fought to stop this
  imposition.
link_al_trailer: https://www.youtube.com/
link_para_visionado: https://www.youtube.com/
festivales:
- festival: Festival Internacional de Cine y Video de los Pueblos Indígenas
  año_de_edicion: X edición (2010)
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - José Luis Matías Alonso
  produccion:
  - José Luis Matías Alonso
  casa_productora:
  - Ojo de Tigre/Comunicación comunitaria
  guion:
  - José Luis Matías Alonso
  investigacion:
  - José Lus Matías Alonso
  fotografia:
  - Gilberto Tecolapa Casarrubias
  sonido:
  - Gilberto Tecolapa Casarrubias
  edicion:
  - Gilberto Tecolapa Casarrubias
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: José Luis Matías Alonso
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1969-01-08'
  telefono_fijo:
  - 01 756 47 33195
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '7561009551'
  telefono_celular_publico: 'false'
  correo_electronico: ojodetigrevideo@yahoo.com.mx
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: José Luis Matías Alonso
  telefono: "01 756 47 33195\t"
  correo_electronico: ojodetigrevideo@yahoo.com.mx
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: El caminar por la vigencia del derecho a la consulta de los pueblos
order: 65
---

