---
title: Cuanajillo La Historia Sin Agua
stills:
- "/public/images/471e0f59-0e41-46aa-a737-9943a81ab67e.png"
- "/public/images/73932005-c157-4239-a267-efb9a7571b0e.png"
- "/public/images/a4a5037f-941f-42e4-824f-7bd82d6d3a46.png"
pais: México
estado: Michoacán
municipio: Morelia
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '20'
idioma_original: Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Agua
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Michoacán
sinopsis_espaniol: "La comunidad de Cuanajillo del Toro, ubicada a 20 minutos de
  la\r\ncapital del Estado de Michoacán, es un pequeño pueblo en donde la lucha por
  la obtención del agua es un odisea para todos los ciudadanos de la localidad. Cuanajillo
  tiene la esperanza de cavar un pozo y encontrar agua."
sinopsis_ingles: "The community of Cuanajillo del Toro, located 20 minutes from the\r\nMichoacan
  state capital, is a small town where the struggle for obtaining water is an odyssey
  for all citizens of the town. Cuanajillo hopes to dig a well and finding water."
link_al_trailer: https://www.youtube.com/watch?v=-5FpY3CkzMM
link_para_visionado: https://www.youtube.com/watch?v=-5FpY3CkzMM
festivales:
- festival: Festival Internacional de Cine de Morelia
  año_de_edicion: '2011'
- festival: Cinema Planeta
  año_de_edicion: '2012'
- festival: Rack Focus Tabasco
  año_de_edicion: ''
- festival: Ecofilm Fest
  año_de_edicion: ''
- festival: Festival Internacional de Cine Acapulco
  año_de_edicion: ''
- festival: Rencontres Internationales Eau et Cinéma (RIEC)
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Mejor Obra Michoacana
  institución: Festival Internacional de Cine de Morelia
- reconocimiento: Premio del Público
  institución: RackFocus Tabasco
- reconocimiento: Mención Honorífica
  institución: RackFocus Tabasco
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Stefan Guzmán Sotelo
  produccion:
  - Santiago Ortiz Monasterio
  casa_productora:
  - Crea
  guion:
  - Stefan Guzmán Sotelo
  fotografia:
  - Jerry Barrera
  sonido:
  - Adrian Bolaños
  edicion:
  - Fatima Vega
  musica_original:
  - Lorena Aragon
directorxs:
- rol:
  - Productor(a)
  nombre: Santiago Ortiz Monasterio
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '5549544301'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '5549544301'
  telefono_celular_publico: 'false'
  correo_electronico: zantiago.monasterio@gmail.com
  correo_electronico_publico: 'false'
- rol:
  - Director(a)
  nombre: Stefan Guzman Sotelo
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1990-09-04'
  telefono_fijo:
  - '4431469152'
  telefono_fijo_publico: ''
  telefono_celular:
  - '4431469152'
  telefono_celular_publico: ''
  correo_electronico: sgs_050@hotmail.com
  correo_electronico_publico: ''
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '4431469152'
  telefono_publico: 'false'
  correo_electronico: sgs_050@hotmail.com
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Stefan Guzman Sotelo
  telefono: '4431469152'
  correo_electronico: sgs_050@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Cuanajillo La Historia Sin Agua
order: 9
---

