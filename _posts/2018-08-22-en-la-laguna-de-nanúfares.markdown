---
title: En la laguna de Nanúfares
poster: "/public/images/b9be3564-9cac-4554-aa5e-2a97cde1268b.jpg"
stills:
- "/public/images/fb925631-d0bb-432c-ad2e-a69cb3756923.jpg"
- "/public/images/2494298a-7a02-47e4-9e5e-337d3d7229a1.jpg"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '8'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- Migrantes
- Pueblos Originarios
- Pescador
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Morelos
sinopsis_espaniol: "Tras ocho años y sin fortuna alguna, Ángel regresa de los Estados
  Unidos a México. Retomando así, su antiguo oficio\r\ncomo pescador de la bella laguna
  a las afueras de su pueblo."
sinopsis_ingles: "After eight years and without any fortune, Angel returns from the
  United States to Mexico. Returning like this, his old office as a fisherman of the
  beautiful lagoon on the outskirts of his town.\t\t\t\t\t\t\t\r\n"
link_al_trailer: https://vimeo.com/56571676
link_para_visionado: https://vimeo.com/56571676
festivales:
- festival: Festival Internacional de Cine de Puebla
  año_de_edicion: '2012'
- festival: Festival de Cine y Artes Audiovisuales Travelling Fest Celaya
  año_de_edicion: '2012'
- festival: Festival Internacional de Cine y Medio Ambiente de México
  año_de_edicion: '2013'
- festival: Muestra oficial del documental DOCANT
  año_de_edicion: '2013'
- festival: Festival Internacional de Cine FENAVID
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Premio del Público
  institución: Muestra de cine digital de Morelos CODEC
- reconocimiento: Primer lugar
  institución: Muestra de cine digital de Morelos CODEC
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Yair Ponce
  produccion:
  - Yair Ponce
  casa_productora:
  - Without a trace filmmakers
  - Dulce Noviembre Producciones
  guion:
  - Yair Ponce
  investigacion:
  - Yair Ponce
  fotografia:
  - Yair Ponce
  sonido:
  - Hugo Sanchez Mejía
  edicion:
  - Yair Ponce
  musica_original:
  - Akajules
  - Harold Azmed
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Yair Ponce
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1980-01-01'
  telefono_fijo:
  - '000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '00000000000'
  telefono_celular_publico: 'false'
  correo_electronico: wat.filmmakers@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Yair Ponce
  telefono: '0000000000'
  correo_electronico: wat.filmmakers@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: En la laguna de Nanúfares
order: 42
---

