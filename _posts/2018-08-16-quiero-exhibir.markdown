---
titulo_en_ingles: Quiero exhibir
layout: post
permalink: "/exhibir/"
order: 11
---

Uno de los principales objetivos del Catálogo de Documental Mexicano es la generación de vínculos entre los posibles exhibidores y los productores o realizadores de las películas. Si estás interesado en exhibir una película documental, ya sea en circuitos comerciales, documentales o educativos, queremos que nuestro catálogo te sea útil, por lo que te sugerimos tomar en cuenta que:

* En la búsqueda avanzada del Catálogo puedes utilizar los diversos filtros que se ofrecen para poder encontrar específicamente lo que estás buscando.

* Algunas películas están en línea para su visionado. Si fuera el caso, podrás encontrar el link de la misma en las ficha técnica de las película. 

* Algunas películas cuentan con un tráiler en línea. Si fuera el caso, podrás encontrar el link de la misma en las ficha técnica de las película. 

* En las fichas técnicas de cada película se especifica si las películas NO TIENEN NINGUNA RESTRICCIÓN para que se realicen exhibiciones de su película, si cuentan con una licencia TOTALMENTE RESTRINGIDA -como el Copyright-, o si prefieren que los CONTACTEN PARA REVISAR CADA CASO.

* Algunas películas cuentan con licencias Creative Commons (generar un hyperlink a una explicacion de lo que es el Creative Commons), por lo que te sugerimos consultar las restricciones de cada una de las licencias: 
  * Attribution / Atribución (BY), requiere la referencia al autor original.
  * Share Alike / Compartir Igual (SA), permite obras derivadas bajo la misma licencia o similar (posterior u otra versión por estar en distinta jurisdicción).
  * Non-Commercial / No Comercial (NC), obliga a que la obra no sea utilizada con fines comerciales.
  * No Derivative Works / No Derivadas (ND), no permite modificar la obra de ninguna manera.

* Si las películas no tienen especificado el tipo de restricción para su exhibición, deberán entrar en comunicación directa con los realizadores, productores o distribuidores de la película que aparecen en los datos de contacto de la ficha técnica. 

En una segunda etapa del Catálogo de Documental Mexicano ofreceremos curadurías de películas sugeridas por tema, duración, Estados de la República, etcétera. Así mismo, pretendemos ofrecer un servicio de cuentas específicas para exhibidores que permitan el visionado completo de las películas con fines de distribución, así como un espacio para anunciar su cartelera. 

No olvides dejarnos tus comentarios o sugerencias para ofrecerte un mejor servicio. 
