---
title: Del Maguey al Pulque
poster: "/public/images/cd87b786-7fb9-4692-a9ee-6e1856f4886f.jpg"
stills:
- "/public/images/d8db8550-001f-4b57-a02f-cf71d2067cfd.jpg"
- "/public/images/54fae286-29e1-4f15-9a70-64708d0dfd53.jpg"
- "/public/images/c867d445-5e2e-416f-bb5f-ae7401fab027.jpg"
pais: México
estado: Tlaxcala
municipio: Tlaxcala
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '60'
idioma_original:
- Español
- Náhuatl
idiomas_de_los_subtitulos:
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- pulque
- tradiciones
- bebida
- maguey
- producción pulquera
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - tlaxcala
sinopsis_espaniol: "Del maguey al pulque, es un acercamiento a la cultura pulquera.
  Se presentan algunos de\r\nlos pocos tinacales que aún quedan en el Estado. Dichos
  tinacales, han sido parte\r\nimportante en la producción pulquera desde tiempos
  de la revolución y el apogeo del\r\nferrocarril hasta la actualidad. La bebida ancestral
  se presenta en esta película, como una\r\nrevelación, la cual parece ser clave en
  los lazos que unen a cualquier comunidad pese a su\r\ndistancia."
sinopsis_ingles: "From the maguey to pulque , is an approach to the pulque culture
  , presents some of the few tinacales remaining within the state, which has been
  important part for pulque production. Not to mention the importance they had in
  times of revolution and heyday of the railroad.\r\nThe pulque is presented closely
  related to the ritual which has merged with Catholicism within the towns of Tlaxcala
  , Hidalgo, Puebla and Mexico City , so that they worship the cross or a saint ,
  which is used to celebrate."
link_para_visionado: http://youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: CONACULTA
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Víctor Lara Torres
  produccion:
  - Víctor Lara Torres
  casa_productora:
  - Ágrafa films
  - PACMYC
  guion:
  - Víctor Lara Torres
  investigacion:
  - Víctor Lara Torres
  fotografia:
  - Víctor Lara Torres
  - Donato Fragoso Mejía
  sonido:
  - Víctor Lara Torres
  - Viridiana Barrón
  - Miguel Ángel Aleman
  edicion:
  - Víctor Lara Torres
  musica_original:
  - Sangre Maíz, Son de Tierra
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Víctor Lara Torres
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1988-01-14'
  telefono_fijo:
  - '012464662454'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '0452461257035'
  telefono_celular_publico: 'false'
  correo_electronico: vfoonty@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Víctor Lara Torres
  telefono: '012464662454'
  correo_electronico: agrafa_films@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 1-2 millones de pesos
coproductores:
- coproductor: PACMYC
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '30'
  privada: '30'
  propia: '30'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Del Maguey al Pulque
order: 18
---

