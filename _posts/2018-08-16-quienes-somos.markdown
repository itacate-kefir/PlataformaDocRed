---
titulo_en_ingles: Quiénes somos
layout: post
permalink: "/quienes-somos/"
order: 11
title: Quiénes Somos
dir: ltr
author: g@kefir.red
---

<img src="{{ site.baseurl }}/assets/images/red-docred.png"
class="img-fluid mx-auto" alt="Logo del catálogo" width="60%">

Documentalistas Mexicanos en Red (DocRed) surge en el año 2012 como un
red nacional de realizadores, productores, guionistas, investigadores y
gestores culturales interesados en impulsar el cine documental mexicano.

Ahora, justamente a través de los medios independientes, de individuos y
colectivos ciudadanos nos acercamos desde muchos ángulos y puntos de
vista a lo que ocurre en distintos lugares de nuestro país. Lo estamos
viendo, lo estamos viviendo, lo estamos registrando  de manera
audiovisual y desde ahí, también se está construyendo y haciendo
memoria. 

A través de lo que nos muestra un creciente número de piezas
audiovisuales nos conmovemos, reflexionamos y nos movemos en direcciones
específicas. La realidad de hoy, sin duda es la materia prima de mil
documentales, de mil preguntas y de mil maneras de ver  la realidad y de
contarla. 

Nos parece evidente que como documentalistas tenemos un papel cada vez
más interesante y desafiante, pues nuestro oficio nos permite indagar,
comunicar y transmitir muchas voces y realidades.

Por ello, nuestro objetivo principal es la visibilización de la
producción cinematográfica realizada en los estados de la República, y a
partir de ésta, aportar a la realización de diversos esfuerzos que
promuevan la descentralización de la producción, la formación, la
distribución y la exhibición del cine documental en el país.
Paralelamente, DocRed busca consolidar un gremio de profesionales
articulados con presencia en cada uno de las entidades federativas. 


<h1 class="invertido">Colaboradores</h1>

## Comisión de Catálogo

### 2017-2018

#### Coordinación:

* Afra Mejía (Jalisco)
* Yaizu Vázquez (Puebla)

#### Colaboradores:

* Benjamín Contreras (Zacatecas)
* Fernando Valencia (Jalisco)
* Itzmalin Benítez (Jalisco)
* Jetzael Rincón (Colima)

#### Prestadores de Servicio Social 2018

* Geovanny Mota (Puebla)
  Escuela de Artes Plásticas y Audiovisual BUAP

* Jacqueline Molina (Puebla)
  Escuela de Artes Plásticas y Audiovisual BUAP

* Sergio López (Puebla)
  Escuela de Artes Plásticas y Audiovisual BUAP

### 2016

#### Coordinación:

* Afra Mejía (Jalisco)
* Yaizu Vázquez (Puebla)

#### Colaboradores:

* Benjamín Contreras (Zacatecas)
* Itzmalin Benítez (Jalisco)

### 2014

#### Coordinación:

* Yaizu Vázquez (Puebla)

#### Colaboradores:

* Adolfo Soto (Baja California Norte)
* Benjamín Contreras (Zacatecas)
* Carlos Cárdenas (Colima)
* Fernando Valencia (Jalisco)
* Nicolás Défossé (Chiapas)
* Nicté Hernández (Ciudad de México)
* Roberto Levy (Colima)
