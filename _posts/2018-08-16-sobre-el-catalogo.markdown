---
titulo_en_ingles: Sobre el catálogo
layout: post
permalink: "/catalogo/"
order: 11
title: Sobre el catálogo
dir: ltr
author: g@kefir.red
---

Documentalistas Mexicanos en Red (DocRed) surge en el año 2012 como un red nacional de realizadores, productores, guionistas, investigadores y gestores culturales interesados en impulsar el cine documental mexicano. Desde nuestros primeros encuentros, al compartir nuestras experiencias provinientes de los diferentes estados de la República, nos dimos cuenta que gracias a las revolución digital se han abaratado costos y se ha transformado la manera como producimos, distribuimos y exhibimos el cine documental, por lo que la producción documental ha crecido signficativamente. Sin embargo, paradójicamente, los espacios de exhibición son muy pocos y es muy difícil conocer cuál es la producción documental que se realiza en todos los confines de nuestro país. El Instituto Mexicano de Cinematografía (IMCINE) realiza un anuario con estadísticas entorno a los datos que arroja la industria cinematográfica, pero como ellos mismos lo mencionan, es muy difícil darle seguimiento a la producción documental de todos los estados de la República, ya que muchas de estas películas se realizan con apoyos locales y sin gran infraestructura industrial.

Por lo anterior, DocRed hemos hecho una apuesta por impulsar este Catálogo de Documental Mexicano con la finalidad de ofrecer un espacio interactivo y digital que concentre y visibilice la producción de cine documental en nuestro país. Uno de los objetivos es que el Catálogo se convierta en una herramienta de difusión y de exhibición del cine documental mexicano. 

La apuesta por hacer de éste un sitio interactivo, es que además de que se puedan realizar búsquedas de documentales por su temática, por su duración, por el estado de la República donde se produjeron, etcétera, también tiene la finalidad de que sean los misms directores o productores los que puedan ingresar las fichas técnicas de sus propias películas para que aparezcan en el Catálogo, así como los datos que cada uno de ustedes considere necesarios para ser contactados por posibles programadores o por personas interesadas en el mismo. También ofrecemos la opción de que aquellos documentales que tengan un tráiler y/o estén disponibles en internet puedan ser vistos libremente a través de nuestro sitio. 

Por todo lo anterior, el Catálogo de Documental Mexicano, es de todos!

Los invitamos a subir las fichas de sus películas y esperamos que a través del Catálogo, sigamos tejiendo redes. 

Atentamente

Afra Mejía y Yaizu Vásquez
Coordinadoras del Catálogo de Documental Mexicano.


