---
title: "Los Chayacates\t\t\t\t\t\t"
poster: "/public/images/94c0c8ce-6cdf-4705-8b8d-8900959988d0.jpg"
stills:
- "/public/images/b627cbce-be20-4ee8-b6c1-c26416ba3056.jpg"
- "/public/images/b627cbce-be20-4ee8-b6c1-c26416ba3056.jpg"
- "/public/images/b627cbce-be20-4ee8-b6c1-c26416ba3056.jpg"
pais: México
estado: Colima
municipio: Colima
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '50'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- tradiciones
- religión
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - colima
sinopsis_espaniol: "Los Chayacates es una tradición profano religiosa que se celebra
  en Ixtlahuacán, Colima los días 5 y 6 de enero, en la cual participan 4 personajes
  llamados Chayacates, quienes andan en búsqueda del niño Dios para protegerlo de
  Herodes. El festejo de Los Chayacates es único en el mundo y es la tradición más
  antigua de Colima surgida antes de la llegada de los españoles a estas tierras.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: Festival Pantalla de Cristal
  año_de_edicion: '2010'
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: '2010'
- festival: Festival Internacional de Cine y Video Indígena Mirando desde nuestra
    raíz
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: Festival Pantalla de Cristal
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Bogart Rodríguez
  produccion:
  - Bogart Rodríguez / Alejandra Santana
  guion:
  - Bogart Rodríguez
  fotografia:
  - Bogart Rodríguez / Alejandra Santana
  sonido:
  - Bogart Rodríguez
  edicion:
  - Bogart Rodríguez / Alejandra Santana
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Bogart Rodríguez\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-04-22'
  telefono_fijo:
  - "(045) 3123199255"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(045) 3125508283"
  telefono_celular_publico: 'false'
  correo_electronico: bogart22@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Bogart Rodríguez
  telefono: "(045) 3125508283\t"
  correo_electronico: bogart22@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: 30-50 mil pesos
coproductores:
- coproductor: "Secretaría de Cultura del Gobierno de Colima\t\t\t\t\t"
  pais: México
  estado: Colima
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "Los Chayacates\t\t\t\t\t\t"
order: 58
---

