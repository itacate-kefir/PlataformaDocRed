---
title: "La petatera\t\t\t\t\t\t"
poster: "/public/images/1249cd38-0b78-43ea-bab7-1127370d37c8.jpg"
stills:
- "/public/images/f0fb110c-0e01-4907-ad4c-392419ff5fce.jpg"
- "/public/images/f0fb110c-0e01-4907-ad4c-392419ff5fce.jpg"
- "/public/images/f0fb110c-0e01-4907-ad4c-392419ff5fce.jpg"
pais: México
estado: Colima
municipio: Colima
año_de_inicio_de_la_produccion: '2008'
año: '2008'
duracion: '42'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- toros
- construcción
- colectivo
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - colima
sinopsis_espaniol: "El documental, describe el sistema constructivo artesanal de una
  plaza de toros ubicada en Villa de Álvarez, Colima, México, con una antigüedad de
  más de 150 años. Es única en su tipo en todo el mundo, ya que está hecha con maderas,
  petates y sogas, dicha plaza, cuenta con un diámetro en el ruedo de 55 metros, además
  las personas aprenden el sistema constructivo, no convencional, de generación en
  generación entre los mismos habitantes de Villa de Álvarez.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: Zanate, Festival de Cine y Video Documental
  año_de_edicion: '2008'
- festival: Festival Pantalla de Cristal
  año_de_edicion: '2008'
- festival: Festival Internacional de Cine y Video Indígena "Mirando desde nuestra
    raíz"
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: Festival Pantalla de Cristal
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Bogart Rodríguez
  produccion:
  - Bogart Rodríguez
  guion:
  - Bogart Rodríguez
  fotografia:
  - Bogart Rodríguez / Alejandra Santana / Miguel Ángel Tovar / Javier Zepeda / Zoila
    Valencia
  sonido:
  - Bogart Rodríguez
  edicion:
  - Bogart Rodríguez
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Bogart Rodríguez\t\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-04-22'
  telefono_fijo:
  - "(045) 3123199255"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '0000000000'
  telefono_celular_publico: 'false'
  correo_electronico: bogart22@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Bogart Rodríguez\t\t\t\t\t"
  telefono: "(045) 3123199255\t"
  correo_electronico: bogart22@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: "Secretaría de Cultura del Gobierno de Colima\t\t\t\t\t"
  pais: México
  estado: Colima
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "La petatera\t\t\t\t\t\t"
order: 60
---

