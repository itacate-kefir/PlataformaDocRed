---
title: "Arte Público Yonke, Ciudad a Cuerpo.\t\t\t\t\t\t"
poster: "/public/images/0c46241f-027c-49ff-9816-9312074be703.jpg"
stills:
- "/public/images/6f9bb1f6-b00a-4ed1-8fd8-e11a863943b9.jpg"
- "/public/images/8ce0c2c1-670e-4944-9cb9-b7630ea7d373.jpg"
- "/public/images/e7d0f725-5361-4403-b55f-757ccd74d8f9.jpg"
pais: México
estado: Sonora
municipio: Sonora
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '45'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- Cultura
- arte urbano
- Nogales
genero_y_estilo:
- 'De autor /De creación: Su apuesta se basa en la mirada personal del autor.'
locaciones:
  paises:
  - México
  estados:
  - sonora
sinopsis_espaniol: 'En Nogales Sonora aun en tiempos de fuerte crisis de identidad
  aparece un movimiento de arte urbano que impactó desde sus inicios a la sociedad
  de esta frontera dura, un movimiento artístico conocido como Taller Yonke . Fundado
  por uno de los artistas plásticos más reconocidos de la ciudad Alberto Morackis,
  la obra de este grupo se ha destacado por su propuesta transgresora y pluricultural
  en la búsqueda de un ¨ser¨ nogalense. '
link_para_visionado: http://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Tino Varela
  produccion:
  - Tino Varela
  guion:
  - Tino Varela
  fotografia:
  - Tino Varela
  sonido:
  - Tino Varela
  edicion:
  - Tino Varela
  musica_original:
  - Mario Rzeslawski, Ricky Shimo, Ivan Rzeslawski.
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Agustín Roberto Varela Echeverría\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1983-08-27'
  telefono_fijo:
  - 631-3137258
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6311300300'
  telefono_celular_publico: 'false'
  correo_electronico: tinovar1@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Agustín Roberto Varela Echeverría\t\t\t\t\t"
  telefono: "631-3137258\t\t"
  correo_electronico: tinovar1@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 10-30 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "Arte Público Yonke, Ciudad a Cuerpo.\t\t\t\t\t\t"
order: 31
---

