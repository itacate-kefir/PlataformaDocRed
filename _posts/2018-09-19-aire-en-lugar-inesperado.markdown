---
title: Aire en lugar inesperado
poster: "/public/images/c2161166-7fa2-457d-8800-83c1b6d5c49c.jpg"
stills:
- "/public/images/f7001edb-1bce-4c71-992e-655075b4c01b.JPG"
sitio_web_de_la_pelicula: http://www3.centro.edu.mx
pais: México
estado: Ciudad de México
municipio: Miguel Hidalgo
año_de_inicio_de_la_produccion: '2011'
año: '2012'
duracion: '70'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Estudiantil: Producido en el marco de una institución educativa, escuela o universidad'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Biográfico e histórico: Sucesos y personajes históricos'
palabras_clave:
- Enfermedad
- Lepra
- aislamiento
- hospital
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
- 'De investigación: El hilo conductor se sustenta en la investigación de un tema.'
locaciones:
  paises:
  - México
  estados:
  - Estado de México
sinopsis_espaniol: Mexico 2011, en un lugar alejado de la ciudad se encuentran las
  últimas personas enfermas de lepra, personas que fueron aisaldas de la sociedad
  desde 1940 con el fin de erradicar esa milenaria enfermedad. Ese lugar más que un
  hospital o un asilo, se mira ya como el recuerdo de lo que fue esa enfermedad, y
  junto con las últimas 8 personas, esta historia retrata sus vidas, sus memorias,
  sus amores.
sinopsis_ingles: Mexico 2011 in a place away from the city are the last people suffering
  from leprosy, people were aisolated from society since 1940 in order to eradicate
  this ancient disease. That place more than a hospital or nursing home, it´s seems
  already like the memory of what the disease was, and along with the last 8 people,
  this story portrays their lives, their memories, their loves.
link_al_trailer: http://www.youtube.com/watch?v=aUQfCIvTe9U
link_para_visionado: http://www.youtube.com/watch?v=aUQfCIvTe9U
festivales:
- festival: Contra el Silencio de todas las Voces
  año_de_edicion: VII Edición, 2012
- festival: Festival Internacional de Cine Documental DocsDF.
  año_de_edicion: '2012'
- festival: Ambulante Gira de Documentales
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Premio del jurado
  institución: Contra el Silencio Todas las Voces
becas_y_apoyos:
- beca: Ninguno
  monto: '0'
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Oliver Victoria
  produccion:
  - Jorge Bolado
  - Aline Ledersnaider
  casa_productora:
  - Centro de Diseño Cine y Televisión
  guion:
  - Oliver Victoria
  investigacion:
  - Melisa Aranzazu
  - Oliver Victoria
  fotografia:
  - Oliver Victoria
  sonido:
  - Axel Muñoz Espino
  edicion:
  - José Pablo Escamilla González Aragón
  otros:
  - nombre: Axel Muñoz Espino
    rol: Postproducción de sonido
  - nombre: Carlos González
    rol: Música Original
  - nombre: Misha Marks
    rol: Postproducción de sonido
  - nombre: Manuel Zavala
    rol: Animación
directorxs:
- rol:
  - Director(a)
  nombre: Oliver Victoria
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1984-06-16'
  telefono_fijo:
  - "(55) 56714246"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(55) 56714246"
  telefono_celular_publico: 'false'
  correo_electronico: sihubo@hotmail.com
  correo_electronico_publico: 'true'
  grados_academicos:
  - grado_academico: Licenciatura
    escuela: Centro de Diseño Cine y Televisión
distribuidoras:
- nombre: Oliver Victoria Castillo
  nombre_publico: 'true'
  telefono: "(55) 56714246"
  telefono_publico: 'false'
  correo_electronico: sihubo@hotmail.com
  correo_electronico_publico: 'true'
  sitio_web: http://www.youtube.com/watch?v=aUQfCIvTe9U
  sitio_web_publico: 'true'
datos_de_contacto:
- nombre: Oliver Victoria Castillo
  telefono: "(55) 56714246"
  correo_electronico: sihubo@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Salas de cine culturales y/o independientes
- Televisión cultural
- Festivales / Muestras mexicanas
- Escuelas y universidades
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: 30-50 mil pesos
coproductores:
- coproductor: Jorge Bolado
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '80'
  propia: '30'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "Aire en lugar inesperado\t\t\t"
order: 80
---

