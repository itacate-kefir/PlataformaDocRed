---
title: " Félix: Autoficciones de un traficante"
poster: "/public/images/d047e5db-fe8b-4cd7-8e16-a0070bf8edd3.jpg"
stills:
- "/public/images/f36e60b3-07aa-493f-8f58-64f3d0aad62b.jpg"
- "/public/images/1f4eb44b-1b9d-4f6e-9bb3-2d72481a837e.jpg"
- "/public/images/13e9f86a-a2a6-419d-b2c5-ebcbd198b5ff.jpg"
pais: México
estado: Baja California Norte
municipio: Tijuana
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '75'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- frontera
- Tráfico de personas
- Migración
- Delincuencia
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Baja California Norte
sinopsis_espaniol: 'Félix: Autoficciones de un traficante, muestra por primera vez
  el mundo del contrabando de personas desde los ojos de un traficante y a partir
  de fragmentos de películas  en las que Félix representa su vida como coyote. El
  documental es la construcción de un personaje que a la vez es actor de su propia
  persona.'
sinopsis_ingles: 'Felix: Autoficciones of a trafficker, shows for the first time the
  world of contraband of people from the eyes of a trafficker and from fragments of
  films in which Felix represents his life as coyote. The documentary is the construction
  of a character who at the same time is an actor of his own person.'
link_al_trailer: https://www.youtube.com/watch?v=zmct-BHoBy4
link_para_visionado: https://www.youtube.com/watch?v=zmct-BHoBy4
festivales:
- festival: Festival Internacional de Cine de Guadalajara
  año_de_edicion: '2011'
- festival: Festival Internacional de Cine Documental de la Ciudad de México
  año_de_edicion: '2012'
- festival: Festival Internacional de Cine de Guanajuato
  año_de_edicion: '2012'
- festival: Festival Internacional de Cine de Monterrey
  año_de_edicion: '2012'
- festival: Hola México Film Festival
  año_de_edicion: '2012'
- festival: Festival Internacional de Cine de Puebla
  año_de_edicion: '2012'
- festival: Muestra de Cine del Segundo Congreso de Antropología
  año_de_edicion: '2012'
- festival: TESTIGO Festival Internacional de Derechos Humanos
  año_de_edicion: '2012'
- festival: Exploring Memories of Self Exhibition en China
  año_de_edicion: '2012'
- festival: Festival de Mexico, Museum of Latin American Art
  año_de_edicion: '2012'
- festival: IV Encuentro sobre Memoria Visual
  año_de_edicion: '2012'
- festival: Border Film Week en Transborder Institute California
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: FOPROCINE
  monto: ''
  institución: IMCINE
  tipo: Privada
- beca: Fondo Nacional para la Cultura y las Artes
  monto: ''
  institución: CONACULTA
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Adriana Trujillo
  produccion:
  - José Inerzia
  casa_productora:
  - Polen Audiovisual S de RL de CV
  guion:
  - Adriana Trujillo
  investigacion:
  - Adriana Trujillo
  fotografia:
  - Fernando Ortega
  sonido:
  - José Inerzia
  edicion:
  - Adriana Trujillo
  musica_original:
  - Luis Blanco
  - José Inerzia
  otros:
  - nombre: Polen Audiovisual S de RL de CV
    rol: Postproducción de imagen
  - nombre: Polen Audiovisual S de RL de CV
    rol: Postproducción de sonido
  - nombre: Sísmica Studio
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  nombre: José Inerzia
  nacionalidad: Española
  fecha_de_nacimiento: '1980-01-01'
  telefono_fijo:
  - '6647008192'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6646699378'
  telefono_celular_publico: 'false'
  correo_electronico: inerzia@polenaudiovisual.org
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Adriana Trujillo
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1981-01-02'
  telefono_fijo:
  - '6647008192'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6643295166'
  telefono_celular_publico: 'false'
  correo_electronico: adriana@polenaudiovisual.org
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Polen Audiovisual S de RL de CV
  nombre_publico: 'true'
  telefono: '664-7008192 '
  telefono_publico: 'false'
  correo_electronico: inerzia@polenaudiovisual.org
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'true'
datos_de_contacto:
- nombre: " Iván Díaz Robledo"
  telefono: " 664-7008192"
  correo_electronico: info@polenaudiovisual.org
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Salas de cine culturales y/o independientes
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '50'
  privada: '0'
  propia: '50'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: " Félix: Autoficciones de un traficante"
order: 39
---

