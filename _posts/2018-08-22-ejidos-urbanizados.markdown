---
title: "Ejidos Urbanizados\t\t"
poster: "/public/images/caa0d674-5e79-45b7-b5e3-e18a64d4de6b.jpg"
stills:
- "/public/images/a9090a4a-7159-4ec6-9381-c445b25f3547.jpg"
- "/public/images/50dcf324-c434-4470-ad1d-0e1348c9c764.jpg"
- "/public/images/7658def7-c5a1-4847-a233-9d942c51f41c.jpg"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2006'
año: '2006'
duracion: '49'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
- 'Territorio: Procesos correspondientes a la organización territorial de una población.'
palabras_clave:
- Transformación territorial
- Urbanismo
- Campo
- Territorios ejidales
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Morelos
sinopsis_espaniol: La historia del crecimiento urbano de Cuernavaca siempre se ha
  calificado de desordenada, en este documental descubrimos como ese crecimiento aparentemente
  entrópico obedece la  lógica del capital que en función de los proyectos de unos
  cuantos va determinando el desarrollo de la mancha urbana con el subsecuente deterioro
  de la ecología y la economía popular de los cuernavacences y especialmente de los
  ejidatarios que aún luchan por conservar su tierra.
sinopsis_ingles: The history of the urban growth of Cuernavaca has always been described
  as disorderly, in this documentary we discover how this seemingly entropic growth
  obeys the logic of capital that, depending on the projects of a few, determines
  the development of the urban sprawl with the subsequent deterioration of the ecology
  and the popular economy of the cuernavacences and especially of the ejidatarios
  who still struggle to conserve their land.
link_al_trailer: https://www.youtube.com/watch?v=YIEuVevovpw&index=1&list=PL5ED8BDBA90D22C86
link_para_visionado: https://www.youtube.com/watch?v=YIEuVevovpw&index=1&list=PL5ED8BDBA90D22C86
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pablo Gleason González
  produccion:
  - Pablo Gleason González
  guion:
  - Pablo Gleason González
  - Armando Villegas Contreras
  investigacion:
  - Pablo Gleason González
  - Armando Villegas Contreras
  fotografia:
  - Pablo Gleason González
  sonido:
  - Pablo Gleason González
  edicion:
  - Pablo Gleason González
  musica_original:
  - Humberto Álvarez
  otros:
  - nombre: Pablo Gleason González
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Pablo Gleason González\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1978-04-11'
  telefono_fijo:
  - '603273235'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '33603273235'
  telefono_celular_publico: 'false'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Pablo Gleason González\t\t\t\t\t"
  telefono: "+33603273235\t\t\t\t"
  correo_electronico: pablo.gleason@filmsdalterite.fr
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "Ejidos Urbanizados\t\t\t\t\t"
order: 47
---

