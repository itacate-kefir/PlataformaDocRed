---
title: Cantando aramos camino
poster: "/public/images/37d38dca-49ec-4fd7-b64e-b6295fa87bdb.jpg"
stills:
- "/public/images/0fe864f8-edf7-4a41-838a-e75bc2ced14f.jpg"
- "/public/images/0fe864f8-edf7-4a41-838a-e75bc2ced14f.jpg"
- "/public/images/0fe864f8-edf7-4a41-838a-e75bc2ced14f.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '20'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Musical: Artistas, agrupaciones, giras, fenómenos musicales'
palabras_clave:
- Música
- Niños
- Canto
- Sierra
- Zapotitlán Salinas
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Tradición, costumbres, tonos musicales y la historia de niños,
  jóvenes y adultos en la sierra mixteca que relatan cómo cantando muestran al mundo
  la cuna cultural de Zapotitlán Salinas. Aquí se entona El Gloria de Vivaldi con
  el mismo fervor con el que se vocaliza el himno a la sierra Tierra Mixteca. Extractos
  reales con tonos musicales y vivenciales que afinan hacia un mismo camino para afirmar
  con un sueño que en esta tierra… Cantando aramos camino.
sinopsis_ingles: Tradition, customs, musical tones and the history of children, youth
  and adults in the Sierra Mixteca recounting how singing show the world the cultural
  cradle of Zapotitlán Salinas. Here sings Vivaldi's Gloria with the same fervor with
  which the hymn to Earth is vocalized Sierra Mixteca. Extracts actual musical tones
  and experiential tuned to the same path with a dream to say that in this world;
  Cantando aramos camino.
link_al_trailer: http://vimeo.com/66256068
link_para_visionado: https://vimeo.com/124263380
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - ADRIANA DURÁN GUERRERO
  produccion:
  - ADRIANA DURÁN GUERRERO
  guion:
  - Adriana Durán Guerrero
  investigacion:
  - Adriana Durán Guerrero
  fotografia:
  - Adriana Durán Guerrero
  sonido:
  - José Duanyar Cruz Estay
  edicion:
  - Adriana Durán Guerrero
  musica_original:
  - Arturo Rivarez y canciones de varios autores interpretadas por el coro de Zapotitlan
    Salinas
  otros:
  - nombre: ADRIANA DURÁN GUERRERO
    rol: Postptoducción de Imagen
  - nombre: ADRIANA DURÁN GUERRERO
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Adriana Durán Guerrero
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1988-08-18'
  telefono_fijo:
  - "(222) 2 25 56 97"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(222) 2 25 56 97"
  telefono_celular_publico: 'false'
  correo_electronico: a.duran.adg@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Adriana Durán Guerrero
  telefono: "(222) 2255697"
  correo_electronico: a.duran.adg@gmail.com
  sitio_web: http://filantropiaaudiovisual.blogspot.com/
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: Cantando aramos camino
order: 48
---

