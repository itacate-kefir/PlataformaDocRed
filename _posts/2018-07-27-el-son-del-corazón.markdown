---
title: El son del corazón
stills:
- "/public/images/ad5c8453-6981-4118-a1af-c8d91900e554.JPG"
- "/public/images/fdd2e237-5bf9-462e-8c64-3b81c19c26e1.jpg"
- "/public/images/6b30a002-06de-482b-a0ea-363571664c75.JPG"
pais: México
estado: Querétaro
municipio: Querétaro
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '60'
idioma_original: Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- Huasteca
- Música
- Fiesta
- Niños
- Niñas
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Guanajuato
sinopsis_espaniol: "Niños y niñas de la región de la Huasteca son protagonistas
  de una destacada cultura y tradición, retratada desde la\r\nprofundización desde
  la visión documental. Música y fiesta se convierten en una labor de difusión donde
  se es testigo para\r\npoder dar a los demás lo que se puede captar con la cámara."
link_para_visionado: https://www.youtube.com/watch?v=-MY19qLklXA
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Héctor Delgado
  produccion:
  - Carmen Camarena
  casa_productora:
  - Querreque Films
  guion:
  - Carmen Camarena
  investigacion:
  - Marisa Gómez
  fotografia:
  - Angel Ibis Ortiz
  sonido:
  - Gil Pérez
  edicion:
  - Héctor Delgado
  musica_original:
  - Huapango de Querétaro
directorxs:
- rol:
  - Productor(a)
  nombre: Carmen Camarena
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '4426900830'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '4426900830'
  telefono_celular_publico: 'false'
  correo_electronico: querrequefilms@hotmail.com
  correo_electronico_publico: 'false'
- rol:
  - Director(a)
  nombre: Héctor Delgado
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '4426900830'
  telefono_fijo_publico: ''
  telefono_celular:
  - '4426900830'
  telefono_celular_publico: ''
  correo_electronico: querrequefilms@hotmail.com
  correo_electronico_publico: ''
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '4426900830'
  telefono_publico: 'false'
  correo_electronico: querrequefilms@hotmail.com
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Héctor Delgado
  telefono: '4426900830'
  correo_electronico: querrequefilms@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: El son del corazón
order: 7
---

