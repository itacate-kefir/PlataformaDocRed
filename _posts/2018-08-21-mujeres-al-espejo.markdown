---
title: Mujeres al espejo
poster: "/public/images/78005e8d-50d1-4d0f-b7e7-c1eaf08f5bae.jpg"
stills:
- "/public/images/331c95cc-c025-425c-a6fa-bf03547b115e.jpg"
- "/public/images/552ef6af-bf91-4cdd-a0f2-eba410f80e12.jpg"
- "/public/images/94f52134-5022-4ce3-b5f8-c3c307fa249f.jpg"
pais: México
estado: Estado de México
municipio: Texcoco
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '90'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- Mujeres
- Violencia
- Derechos Humanos
- Testimonios
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Estado de México
sinopsis_espaniol: 'Mujeres al Espejo, reflexiona sobre un fenómeno muy grave de nuestra
  sociedad: La violencia contra la mujer. El documental define el proceso denominado:
  ciclo y escalada de la violencia para que la mujer pueda identificar si lo está
  viviendo, tome conciencia de ello y busque soluciones, el trabajo presenta testimonios
  que hablan de mujer a mujer, donde se busca establecer una comunicación que haga
  pensar, sentir, reflexionar y hablar.'
sinopsis_ingles: 'The reason to film Women in the Mirror is the need of reflection
  and to become aware of a seriously problem in our society: “gender violence”. This
  documentary defines and analyzes Cycle of genre violence, so women can identify
  if they are living through this situation and search for solutions. this film presents
  woman to woman testimony so all the public can think, feel, reflect on it and also
  talk about it.'
link_al_trailer: https://www.youtube.com/watch?v=dKVLa63hpDg
link_para_visionado: https://www.youtube.com/watch?v=dKVLa63hpDg
festivales:
- festival: Festival de Cine Político, Social y de los Derechos Humanos de Valparaíso
  año_de_edicion: Edición 7 (2013)
premios_y_reconocimientos:
- reconocimiento: Medalla "Salvador Allende" a Mejor Película Documental
  institución: Festival de Cine Político, Social y de los Derechos Humanos de Valparaíso
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Jaime Jamaica Castro
  produccion:
  - Mari Carmen Jamaica Castro
  casa_productora:
  - JICA Producciones
  guion:
  - Jaime Jamaica Castro
  investigacion:
  - Jaime Jamaica Castro
  fotografia:
  - Ivan Guerra, Jaime Jamaica
  sonido:
  - Oscar Chavolla
  edicion:
  - Jaime Jamaica Castro
  musica_original:
  - Amparo Sánchez
  otros:
  - nombre: Ivan Guerra
    rol: Postproducción de imagen
  - nombre: Oscar Chavolla
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  nombre: Mari Carmen Jamaica Castro
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1967-01-24'
  telefono_fijo:
  - 01 595 9540649
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 01 595 9540649
  telefono_celular_publico: 'false'
  correo_electronico: litajam39@hotmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Jaime Jamaica Castro
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1968-06-09'
  telefono_fijo:
  - 01 595 9540458
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 55 18640501
  telefono_celular_publico: 'false'
  correo_electronico: jaimejam@jicaproducciones.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Jaime Jamaica Castro
  telefono: 01 595 9540458
  correo_electronico: jaimejam@jicaproducciones.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Mujeres al espejo
order: 14
---

