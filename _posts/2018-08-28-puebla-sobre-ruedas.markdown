---
title: "“Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas”"
poster: "/public/images/ab4d56d9-16fd-489e-872f-6965a91e0ac2.JPG"
stills:
- "/public/images/63e25258-5b9f-4009-899f-fc82ab718491.jpg"
- "/public/images/63e25258-5b9f-4009-899f-fc82ab718491.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '42'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Puebla
- discapacidad
- deporte
- Sillas de ruedas
- sueños
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Historia narrada por sus protagonistas, Gabriel, Renata, Julio
  y Bernardo  que por conseguir sus sueños de hacer deporte y representar a Puebla
  botean en las calles para  conseguir recursos, ya que ellos enfrentan algún tipo
  de discapacidad. Un documental donde las ganas de vivir y luchar se proyectan con
  cada uno de los roles que ellos realizan para enfrentar su situación.
sinopsis_ingles: Story told by its protagonists, Gabriel, Renata, July and Bernardo
  that to achieve their dreams of playing sports and represent botean Puebla on the
  streets for resources, as they face a disability. A documentary in which the will
  to live and fight are projected to each of the roles they perform for coping.
link_al_trailer: http://www.youtube.com/watch?v=1aEFK0n9yco
link_para_visionado: http://www.youtube.com/watch?v=1aEFK0n9yco
festivales:
- festival: Encuentro Contra el Silencio Todas las Voces
  año_de_edicion: VIII
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Luis Vicente Rodríguez Esponda
  produccion:
  - Luis Vicente Rodríguez Esponda
  casa_productora:
  - FomArte
  guion:
  - Luis Vicente Rodríguez Esponda
  fotografia:
  - Luis Vicente Rodríguez Esponda
  - Ignacio Vega
  sonido:
  - Luis Vicente Rodríguez Esponda
  edicion:
  - Luis Vicente Rodríguez Esponda
  musica_original:
  - Jonathan Alarcón Reyes Varela
  - Cristian Mercado Rodríguez
  - The New
  - The Diantres
  - Jorge Bonilla Torres
  otros:
  - nombre: Nadia Zuhaam del Pozo Kuri
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Luis Vicente Rodríguez Esponda
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1981-12-22'
  telefono_fijo:
  - "(222) 582.99.38"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2223.757585'
  telefono_celular_publico: 'false'
  correo_electronico: fom_arte@yahoo.com.mx
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Luis Vicente Rodríguez Epsonda
  telefono: "(222) 582.99.38"
  correo_electronico: fom_arte@yahoo.com.mx
  sitio_web: http:www.fomarte.com
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "“Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas Puebla Sobre Ruedas”"
order: 73
---

