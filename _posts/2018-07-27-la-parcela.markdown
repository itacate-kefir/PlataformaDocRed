---
title: La Parcela
poster: "/public/images/17f0710d-a84f-453f-9a2f-6fd087fde646.JPG"
stills:
- "/public/images/ede9255f-0b1d-4634-b0f9-aec1d4b2a5d8.JPG"
- "/public/images/25e38fa3-6e58-4591-a5cd-8838bad45bfa.JPG"
- "/public/images/7fce1542-aa7d-4a08-bd6d-59d79407834c.JPG"
pais: México
estado: Querétaro
municipio: Querétaro
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '29'
idioma_original: Otomí
idiomas_de_los_subtitulos:
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Indígenas
- Territorio
- Cooperativas
- EconomíaSolidaria
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Querétaro
sinopsis_espaniol: 'Con entusiasmo y vocación de servicio, miembros de la Unión
  de Cooperativas Ñöñho de San Ildefonso -una organización indígena- procuran la preservación
  del Medio ambiente de San Ildefonso con su territorio y la cultura ligada al mismo.
  Muestran el estado del área natural de su pueblo, los lugares sagrados, históricos,
  sitios arqueológicos y platican sobre el trabajo que han hecho en “La Parcela”. '
link_para_visionado: https://www.youtube.com/watch?v=OZhjhMz_kXg
festivales:
- festival: Festival de cine indígena de Querétaro
  año_de_edicion: '2012'
- festival: Festival Internacional de Cine y Video Indígena de Puebla
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Miguel Alfredo Rosales Vázquez
  produccion:
  - Trinidad Nava Vega
  guion:
  - Miguel Alfredo Rosales Vázquez
  investigacion:
  - Miguel Alfredo Rosales Vázquez
  fotografia:
  - Miguel Alfredo Rosales Vázquez
  - Pedro Nava
  sonido:
  - Sergio Becerril
  edicion:
  - Miguel Alfredo Rosales Vázquez
  musica_original:
  - Dueto Águila
directorxs:
- rol:
  - Productor(a)
  nombre: Trinidad Nava Vega
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '4424142491'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '4481112241'
  telefono_celular_publico: 'false'
  correo_electronico: trinidad.nava@decora.org.mx
  correo_electronico_publico: 'false'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '4421355457'
  telefono_publico: 'false'
  correo_electronico: miguelrosales72@gmail.com
  correo_electronico_publico: 'false'
  sitio_web: https://nonho.wordpress.com/
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Miguel Alfredo Rosales Vázquez
  telefono: '4421447778'
  correo_electronico: miguelrosales72@gmail.com
  sitio_web: https://nonho.wordpress.com/
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: La Parcela
order: 5
---

