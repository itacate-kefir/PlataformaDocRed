---
title: "Un sueño mexicano\t\t\t\t\t\t"
poster: "/public/images/803a32b7-033a-49f2-b52c-518133ad7c51.png"
stills:
- "/public/images/d977ac78-2f7d-47cc-978c-614b3e52d8b1.png"
- "/public/images/a1933ed0-3aae-4827-9f06-c3178a0f67e6.png"
- "/public/images/dcbb89f9-baf1-45e7-bd62-0c57312ac8e8.png"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '25'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- Migrantes
- Aspiraciones
- Calidad de Vida
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  - Estados Unidos
  estados:
  - Cuernavaca
  - California
sinopsis_espaniol: "Una visión que desmitifica el tipo de vida que llevan las personas
  que emprenden el viaje, no sólo geográfico sino también existencial y en búsqueda
  de una mejor vida en los Estados Unidos.\t\t\t\t\t\t\r\n"
sinopsis_ingles: A vision that demystifies the kind of life that the people who undertake
  the journey take, not only geographically but also existentially and in search of
  a better life in the United States.
link_al_trailer: https://vimeo.com/41726258
link_para_visionado: https://vimeo.com/41726258
festivales:
- festival: Festival In Memoriam Santiago Álvarez, Cuba.
  año_de_edicion: '2010'
- festival: Riviera Mayan Underground Film Festival, Playa del Carmen México.
  año_de_edicion: '2010'
- festival: Festival Internacional de cine documental DOCMX
  año_de_edicion: '2010'
- festival: Rakk Focus Tabasco
  año_de_edicion: '2011'
- festival: Festival de la Memoria Tepoztlán
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: UABC filme festival, Baja California México.
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Yair Ponce
  produccion:
  - Javier Silva
  guion:
  - Yair Ponce
  investigacion:
  - Yair Ponce
  fotografia:
  - Yair Ponce
  - Gregory Berger
  sonido:
  - Antonio Musek
  edicion:
  - Yair Ponce
  musica_original:
  - Matiss Ocampo
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Yair Ponce
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1980-02-01'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '0000000000'
  telefono_celular_publico: 'false'
  correo_electronico: wat.filmmakers@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Yair Ponce
  telefono: '000000000'
  correo_electronico: wat.filmmakers@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "Un sueño mexicano\t\t\t\t\t\t"
order: 45
---

