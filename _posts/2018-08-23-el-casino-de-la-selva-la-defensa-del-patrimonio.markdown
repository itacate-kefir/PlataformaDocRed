---
title: "El casino de la Selva: La defensa del patrimonio\t\t\t\t\t\t"
poster: "/public/images/788ac26c-a85b-4ccf-9688-9488af51cc63.jpg"
stills:
- "/public/images/67fdf0b4-3cfc-42ca-af27-b8050865a899.jpg"
- "/public/images/67fdf0b4-3cfc-42ca-af27-b8050865a899.jpg"
- "/public/images/67fdf0b4-3cfc-42ca-af27-b8050865a899.jpg"
- "/public/images/67fdf0b4-3cfc-42ca-af27-b8050865a899.jpg"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2002'
año: '2002'
duracion: '49'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Defensa del territorio
- Organizacion social
- Patrimonio natural
- Resistencia
- Represión
- Transnacionales
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Morelos
sinopsis_espaniol: "Narra la defensa por parte de activistas del espacio denominado
  Casino de la Selva, contra la destrucción de la ecología, la arqueología y del espacio
  cultural, perpetuado por la firma comercial COSTCO-Comercial Mexicana y el Partido
  Acción Nacional en Morelos (encabezado por el entonces gobernador del estado Sergio
  Estrada Cajigal).\t\t\t\t\t\t\t\r\n"
sinopsis_ingles: "It narrates the defense by activists of the space called Casino
  de la Selva, against the destruction of ecology, archeology and cultural space,
  perpetuated by the commercial firm COSTCO-Comercial Mexicana and the National Action
  Party in Morelos (headed by the then governor of the state Sergio Estrada Cajigal).\r\n"
link_al_trailer: https://www.youtube.com/watch?v=7GTcB7PCWR8&t=1s
link_para_visionado: https://www.youtube.com/watch?v=7GTcB7PCWR8&t=1s
festivales:
- festival: Encuentro contra el silencio de todas las voces
  año_de_edicion: 3era edición
premios_y_reconocimientos:
- reconocimiento: Primer lugar (Compartido)
  institución: Encuentro contra el silencio de todas las voces
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pablo Gleason González
  produccion:
  - Pablo Gleason González
  casa_productora:
  - Push and Play
  guion:
  - Pablo Gleason González
  investigacion:
  - Colectivo la neta
  fotografia:
  - Lázaro Sandoval M.
  - Rosa Elena
  - Gómez de la Torr
  - Jorge Aguilar
  sonido:
  - Pablo Gleason González
  edicion:
  - Pablo Gleason González
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Pablo Gleason González\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1978-04-11'
  telefono_fijo:
  - '603273235'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+33603273235"
  telefono_celular_publico: 'false'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Pablo Gleason González\t\t\t\t\t"
  telefono: "+33603273235\t\t\t\t"
  correo_electronico: pablo.gleason@filmsdalterite.fr
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "El casino de la Selva: La defensa del patrimonio\t\t\t\t\t\t"
order: 62
---

