---
title: Belleza reciclada
poster: "/public/images/c422b54f-f702-4719-bc79-f5956f2a85d0.jpg"
stills:
- "/public/images/ba4d5005-fca7-4eb5-81e5-52b5adbb732b.jpg"
- "/public/images/c116e7ed-93f0-4743-bc43-f005b35a4f54.jpg"
- "/public/images/126b76b9-17c7-42cd-8ec5-18f834456f8f.jpg"
- "/public/images/44a0c095-0103-4a88-92fd-90efa56dcad3.jpg"
pais: México
estado: Puebla
municipio: San Andrés Cholula
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '6'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
palabras_clave:
- Hombre
- Reciclada
- México
- Relaciones
- Sentimiento
- Entorno
genero_y_estilo:
- 'Ensayo / reflexivo: Narración reflexiva en primera persona'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: "Unas bolsas de plástico danzan con el viento mientras un lienzo
  humano desnudo es cubierto por \r\nhojas de buganvilia y el “progreso” se abre paso
  en distintos rincones de México. Un ensayo \r\naudiovisual que transmite un sentimiento,
  una reflexión sobre la relación del hombre y su entorno y \r\nsobre actitudes arraigadas
  en la mente colectiva mexicana."
sinopsis_ingles: Progress continues to extend its power all along the fertile Mexican
  territory while plastic bags dance to the wind and a human canvas is covered with
  Bougainvillea petals. An essay in audiovisual format that shares a feeling and argues
  about the Human nature, its relationship to the planet and its Mexican representatives.
link_para_visionado: https://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: Ninguno
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Alonso Pérez Fragua
  - Alfredo Atala
  produccion:
  - Alonso Pérez Fragua
  casa_productora:
  - Casa Nueve Producciones
  guion:
  - Alonso Pérez Fragua
  - Alfredo Atala
  investigacion:
  - Alfredo Atala
  - Alonso Pérez Fragua
  fotografia:
  - Alonso Pérez Fragua
  sonido:
  - Alonso Pérez Fragua
  edicion:
  - Emilio Zunzunegu
  - Alfredo Atala
  otros:
  - nombre: Alfredo Atala
    rol: Postproducción de imagen
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Alonso Fernando Pérez Fernández Fragua
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1982-05-04'
  telefono_fijo:
  - "(222) 2422808"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2226 692618
  telefono_celular_publico: 'false'
  correo_electronico: fraguando@hotmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Alfredo Atala Layún
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1984-04-07'
  telefono_fijo:
  - "(222) 321 1414"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2226 692618
  telefono_celular_publico: 'false'
  correo_electronico: alfredo_atala@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Alonso Fernando Pérez Fernández Fragua
  telefono: 2226 692618
  correo_electronico: fraguando@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: Belleza reciclada
order: 43
---

