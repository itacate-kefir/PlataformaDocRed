---
title: Una máscara mexicana
poster: "/public/images/da0fbc53-512a-4c2d-ab44-358d209a8cba.png"
stills:
- "/public/images/2d41c6be-d759-4768-99a0-a153140f3468.png"
- "/public/images/2d41c6be-d759-4768-99a0-a153140f3468.png"
- "/public/images/2d41c6be-d759-4768-99a0-a153140f3468.png"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '24'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- Cultura
- México
- Lucha libre
- máscara
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Una máscara mexicana es el primer documental de la realizadora
  Joselyn de los Santos, en el veremos como la lucha libre forma parte importante
  de la cultura mexicana y como la gente se siente identificada con ella y esta encuentra
  su máximo ícono en la máscara. Un viaje guiado por la música nos lleva a través
  de este documental, demostrando que podemos apreciar la lucha al igual que la música,
  de las más variadas formas y que todos podemos encontrar en ella algo con que sentirnos
  identificados.
sinopsis_ingles: A Mexican mask is the first documentary by the director Joselyn de
  los Santos, in which we will see how wrestling is an important part of Mexican culture
  and how people feel identified with it and finds its maximum icon in the mask. A
  journey guided by music takes us through this documentary, showing that we can appreciate
  the struggle as well as music, in the most varied ways and that we can all find
  in it something to feel identified with.
link_para_visionado: https://www.youtube.com
festivales:
- festival: Festival de Acapulco
  año_de_edicion: ''
- festival: Festival de Chiapas
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Joselyn de los Santos González
  produccion:
  - Jose Fernando Cum Marín
  guion:
  - Joselyn de los Santos González
  investigacion:
  - Joselyn de los Santos González
  fotografia:
  - Jose Fernando Cum Marín
  - Alfredo González Rodríguez
  sonido:
  - Alan Humberto Avena
  edicion:
  - Jose Fernando Cum Marín
  otros:
  - nombre: Jose Fernando Cum Marín
    rol: Postproducción de Imagen
directorxs:
- rol:
  - Productor(a)
  nombre: Jose Fernando Cum Marín
  nacionalidad: Guatemalteco
  fecha_de_nacimiento: '1988-08-25'
  telefono_fijo:
  - '2225357571'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2225357571'
  telefono_celular_publico: 'false'
  correo_electronico: ferlg51@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Joselyn de los Santos González
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1994-02-10'
  telefono_fijo:
  - "(222)2835457"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2223067935'
  telefono_celular_publico: 'false'
  correo_electronico: joselyndelossantos869@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Joselyn de los Santos González
  telefono: '2223067935'
  correo_electronico: joselyndelossantos869@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: Una máscara mexicana
order: 54
---

