---
title: De tres, uno.
poster: "/public/images/66e6186b-b2af-43fe-9fd5-30790b81acd1.jpg"
stills:
- "/public/images/55b66352-076e-4705-aaef-c4ba4a4a8238.jpg"
- "/public/images/211c1c8b-fb8e-41f2-aed1-62f8b5c48622.jpg"
- "/public/images/75584deb-c7c2-4802-b6d8-69a7a1008d62.jpg"
pais: México
estado: Campeche
municipio: Campeche
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '26'
idioma_original: Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Jóvenes
- Desarrollo Social
- Vida Rural
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Campeche
sinopsis_espaniol: El documental aborda la incopatibilidad entre el perfil de las
  carreras técnicas que tres jóvenes no tuvieron más remedio que elegir, y la realidad
  de su ambiente rural, así como las frustraciones que se generan cuando no existen
  oportunidades para el desarrollo personal.
sinopsis_ingles: This documentary addresses the incompatibility between the technical
  careers of three young people, which were their only options, and the reality of
  their rural environment. It explores the frustrations that arise when opportunities
  for personal development are not available.
link_para_visionado: https://www.youtube.com/watch?v=OVQabzf4Hwg
festivales:
- festival: Ambulante Gira de Documentales
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ambulante Más Allá
  monto: ''
  institución: Ambulante Más Allá A.C.
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Eloi Antonio Chávez Carrreño
  produccion:
  - Natalia del Carmen Matus Yepes
  casa_productora:
  - Ambulante Más Allá A.C.
  guion:
  - Eloi Antonio Chávez Carrreño
  - Nicolás Feldman
  investigacion:
  - Eloi Antonio Chávez Carrreño
  fotografia:
  - Francisco Javier Bautista de la Cruz
  sonido:
  - Yanir Jiménez Martínez
  edicion:
  - Nicolás Feldman
  musica_original:
  - Juan Carlos Corpus
directorxs:
- rol:
  - Productor(a)
  nombre: Natalia Matus Yepex
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1993-07-27'
  telefono_fijo:
  - '9824348231'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '9824348231'
  telefono_celular_publico: 'false'
  correo_electronico: nmatus-yepes@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Eloi Antonio Chávez Carreño
  nombre_publico: 'false'
  telefono: '9824348228'
  telefono_publico: 'false'
  correo_electronico: simplemente.eloi@gmail.com
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Eloi Antonio Chávez Carreño
  telefono: '9824348228'
  correo_electronico: simplemente.eloi@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 100-300 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: De tres, uno.
order: 2
---

