---
title: Restaura mi Pueblo
stills:
- "/public/images/28acc7eb-5cea-422d-a167-08d33cc2ce93.jpg"
- "/public/images/6554fc97-2c52-41dc-b780-d52c9085eadb.jpg"
- "/public/images/3b0198c8-5611-46d7-8709-6c93c4ee7a56.JPG"
pais: México
estado: Querétaro
municipio: Querétaro
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '42'
idioma_original: Otomí
idiomas_de_los_subtitulos:
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- Indígenas
- Pintura
- SantoPatrono
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Querétaro
sinopsis_espaniol: La restauración del Santo Patrono San Ildefonso, pintura antigua
  de valor histórico cultural y religioso para la comunidad de San Ildefonso Tultepec
  da ocasión para retomar su significado vital y así como "el Santo" es restaurado
  en la población buscamos restaurar lo que ha ido degranando a través de la historia,
  es preciso volver a la esencia para poder continuar revalorando la forma de vivir
  de un pueblo entero que se remonta a tiempos pasados y tira hacia adelante.
link_para_visionado: https://www.youtube.com/watch?v=fo2x80aI_tM
festivales:
- festival: Festival de cine indígena de Querétaro
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Miguel Alfredo Rosales Vázquez
  produccion:
  - Miguel Alfredo Rosales Vázquez
  guion:
  - Miguel Alfredo Rosales Vázquez
  investigacion:
  - Marisa Gómez
  fotografia:
  - Miguel Alfredo Rosales Vázquez
  sonido:
  - Miguel Alfredo Rosales Vázquez
  edicion:
  - Miguel Alfredo Rosales Vázquez
  musica_original:
  - Emilio Mejía
  - Isabel Justine Trejo
  - Quinta Huapanguera
directorxs:
- rol:
  - Director(a)
  nombre: Miguel Alfredo Rosales Vázquez
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1972-11-24'
  telefono_fijo:
  - '4421355457'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '4421447778'
  telefono_celular_publico: 'false'
  correo_electronico: miguelrosales72@gmail.com
  correo_electronico_publico: 'false'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '4421355457'
  telefono_publico: 'false'
  correo_electronico: miguelrosales72@gmail.com
  correo_electronico_publico: 'true'
  sitio_web: https://nonho.wordpress.com/
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Miguel Alfredo Rosales Vázquez
  telefono: '4421447778'
  correo_electronico: miguelrosales72@gmail.com
  sitio_web: https://nonho.wordpress.com/
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Restaura mi Pueblo
order: 6
---

