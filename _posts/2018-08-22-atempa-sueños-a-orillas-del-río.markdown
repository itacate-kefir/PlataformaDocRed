---
title: Atempa -Sueños a orillas del río
poster: "/public/images/030da884-8556-4c7d-a4de-7e1bb0deb796.jpg"
stills:
- "/public/images/ff684428-a9ac-44d5-8715-a6a723cdf33c.jpg"
- "/public/images/d2633d5f-b067-46cd-8866-04ef28b64f68.jpg"
- "/public/images/1466a7bc-cd9d-4ba0-b8f3-a82eda75202f.jpg"
sitio_web_de_la_pelicula: https://atempadocumental.wordpress.com/
pais: México
estado: Oaxaca
municipio: Oaxaca
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '86'
idioma_original:
- Español
- Zapoteco
idiomas_de_los_subtitulos:
- Inglés
- Español
formatos:
- DVD
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
palabras_clave:
- Atempa
- Muxe
- Istmo
- Tehuantepec
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Oaxaca
sinopsis_espaniol: 'Tino desea lograr su sueño: ser reina muxe. Siendo niño de niña
  para desarrollar su feminidad adolescente, busca solucionar su pasado y su economía
  pese a las carencias sociales. Esta película es un retrato del Istmo de Tehuantepec
  Oaxaca, México, a partir de las historias de tres de sus habitantes, en el cual
  existe la identidad de la dualidad sexo-genérica, que no es hombre ni mujer, es
  Muxe (homosexual zapoteco). Región que también es conocida por la fortaleza de sus
  mujeres.'
sinopsis_ingles: "Tino hopes his dream of becoming a muxe queen will come true. As
  a homosexual child, he takes on the role of a girl in order to develop his adolescent
  female identity, and struggles to resolve his past and overcome his poverty.\r\nThis
  film is an intimate journey to the heart of the Isthmus of Tehuantepec Oaxaca, Mexico,
  viewed through the stories of three of its residents, where there exists a third
  gender identity, which is neither a man nor a woman, but a Muxe (a Zapotec homosexual)."
link_al_trailer: https://vimeo.com/78394318
link_para_visionado: https://vimeo.com/78394318
festivales:
- festival: Festival Internacional de Cine de Morelia
  año_de_edicion: '2013'
- festival: Festival Internacional de Cine de Puebla
  año_de_edicion: '2013'
- festival: Festival Internacional de Guadalajara
  año_de_edicion: '2013'
- festival: Festival SCOPE online de París
  año_de_edicion: '2013'
- festival: Festival Internacional de Cine de No Violencia  y Medio Ambiente
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Los Angeles Movie Awards 2011
becas_y_apoyos:
- beca: Premio Klic al mejor Documental Mexicano
  monto: ''
  institución: Festival Internacional de Cine de Morelia
  tipo: Privada
- beca: Premio al Mejor Documental Mexicano
  monto: ''
  institución: Festival Internacional de Cine de Puebla
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Edson Caballero Trujillo
  produccion:
  - Edson Caballero Trujillo
  - John Dickie
  casa_productora:
  - INFRATIERRA PRODUCTORA
  guion:
  - Edson Jair Caballero Trujillo
  investigacion:
  - Edson Jair Caballero Trujillo
  fotografia:
  - Edson Jair Caballero Trujillo
  sonido:
  - Edson Jair Caballero Trujillo
  - Omar Juárez
  edicion:
  - Edson Caballero Trujillo
  - Luis Urrutia
  musica_original:
  - Atilano Morales Jiménez
  - Abel Jiménez Osorio
  otros:
  - nombre: Juan Ignacio Sánchez
    rol: 'Postproducción de Imagen:'
  - nombre: Pepe Acosta
    rol: Postproducción de Imagen
  - nombre: Edson Caballero Trujillo
    rol: Postproducción de Imagen
  - nombre: Omar Juárez
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Edson Jair Caballero Trujillo
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-10-01'
  telefono_fijo:
  - "(+951)5170207"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '9511123492'
  telefono_celular_publico: 'false'
  correo_electronico: caballeroedson@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Edson Caballero Trujillo
  telefono: '9511123492'
  correo_electronico: caballeroedson@hotmail.com
  sitio_web: https://edsoncaballero.wordpress.com/
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 1-2 millones de pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 30-50 mil pesos
layout: ficha_documental
titulo_en_ingles: Atempa - Sueños a orillas del río
order: 26
---

