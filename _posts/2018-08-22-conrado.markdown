---
title: "conrado\t\t\t\t\t\t"
poster: "/public/images/06944c6d-4335-42e3-8aef-08791fdf62a1.jpg"
stills:
- "/public/images/af62cbb4-0484-48d8-b365-73300a0b00dc.jpg"
- "/public/images/5598b87d-2842-4840-a96c-e230b9411b9e.jpg"
- "/public/images/55a8b1e4-a01b-48f4-a50f-5f9e489effe0.jpg"
pais: México
estado: Sinaloa
municipio: Sinaloa
año_de_inicio_de_la_produccion: '2009'
año: '2009'
duracion: '12'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- calle
- reglas
- contracultura
genero_y_estilo:
- 'De autor /De creación: Su apuesta se basa en la mirada personal del autor.'
locaciones:
  paises:
  - México
  estados:
  - sinaloa
sinopsis_espaniol: "¿Por qué el ser humano tiene que apegarse a las reglas de una
  sociedad establecida? este hombre rompió esas reglas y vive feliz, ahora es juzgado
  por su aspecto y su ritmo de vida. De la viva voz de Conrado y un amigo entrañable
  se relata su historia.\r\n\t\t\t\t\t\t\t"
link_al_trailer: "http://www.youtube.com/watch?v=avmDoX8wL-c\t\t\t\t\t\t"
link_para_visionado: "http://www.youtube.com/watch?v=avmDoX8wL-c\t\t\t\t\t\t"
festivales:
- festival: Festival de Cortometrajes Cine a las calles
  año_de_edicion: '2010'
- festival: Cineseptiembre, Sinaloa
  año_de_edicion: '2010'
- festival: Short Films Mexico
  año_de_edicion: '2010'
- festival: Encuentro Hispanoamericano de cine y video documental independiente "Voces
    contra el silencio"
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Tercer Lugar
  institución: Concurso estatal "Jóvenes en Corto"
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - José eduardo esparza tovar
  produccion:
  - José eduardo esparza tovar
  - Misael López Nevarez
  guion:
  - José eduardo esparza tovar
  fotografia:
  - José eduardo esparza tovar
  sonido:
  - Omar Schober
  edicion:
  - José eduardo esparza tovar
  musica_original:
  - Omar Schober
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "José Eduardo Esparza Tovar\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1985-09-17'
  telefono_fijo:
  - "(669) 668 86 28"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(6691) 47 10 51"
  telefono_celular_publico: 'false'
  correo_electronico: eduardoesparzamzt@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "José Eduardo Esparza Tovar\t\t\t\t\t"
  telefono: "(669) 668 86 28\t"
  correo_electronico: eduardoesparzamzt@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 10-30 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "conrado\t\t\t\t\t\t"
order: 32
---

