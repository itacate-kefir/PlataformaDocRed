---
title: "El mineral o la vida\t\t\t\t\t"
poster: "/public/images/76ad81cb-476c-4806-a8b7-3db72f1bcc1e.jpg"
stills:
- "/public/images/ed98044e-0fad-48a7-afd7-46a8780000bd.jpg"
pais: México
estado: Guerrero
municipio: Chilpancingo
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '29'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Pueblos Originarios
- Defensa del territorio
- Minería Transnacional
- Extractivismo
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'De investigación: El hilo conductor se sustenta en la investigación de un tema.'
locaciones:
  paises:
  - México
  estados:
  - Guerrero
sinopsis_espaniol: "Esta es una historia por la permanencia de los pueblos originarios
  dentro del engranaje capitalista del siglo XXI.  Es una lucha entre los intereses
  colectivos  de los pueblos originarios de la montaña de guerrero y los intereses
  de una elite en el poder; son los pueblos me'phaa (tlapanecos) y nu savi (mixtecos)
  que defienden a la madre tierra y a sus familias de las grandes empresas mineras
  canadienses e inglesas que son apoyadas por el gobierno mexicano.\t"
sinopsis_ingles: "This is a story of the permanence of the original peoples within
  the capitalist gear of the twenty-first century. It is a struggle between the collective
  interests of the original peoples of the warrior mountain and the interests of an
  elite in power; they are the Me'phaa (Tlapanec) and Nu Savi (Mixtecos) peoples who
  defend the mother land and their families from the large Canadian and English mining
  companies that are supported by the Mexican government.\r\n"
link_al_trailer: https://www.youtube.com/watch?v=z-zKQBn4DAU
link_para_visionado: https://www.youtube.com/watch?v=z-zKQBn4DAU
festivales:
- festival: Festival de cine y video indígena de Michoacán
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Festival de cine y video indígena de Michoacán
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - José Luis Matías Alonso
  produccion:
  - José Luis Matías Alonso
  casa_productora:
  - Ojo de tigre / Comunicación Comunitaria
  guion:
  - José Luis Matías Alonso
  investigacion:
  - José Luis Matías Alonso
  fotografia:
  - José Luis Matías Alonso
  sonido:
  - Gilberto Tecolapa Casarrubias
  edicion:
  - Gilberto Tecolapa Casarrubias
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: José Luis Matías Alonso
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1969-01-08'
  telefono_fijo:
  - 01 756 47 33195
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '7561009551'
  telefono_celular_publico: 'false'
  correo_electronico: ojodetigrevideo@yahoo.com.mx
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: José Luis Matías Alonso
  telefono: "01 756 47 33195\t"
  correo_electronico: ojodetigrevideo@yahoo.com.mx
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Televisión cultural
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "El mineral o la vida\t\t\t\t\t"
order: 66
---

