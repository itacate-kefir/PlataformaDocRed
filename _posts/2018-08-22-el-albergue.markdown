---
title: El albergue
poster: "/public/images/0497e67c-55ce-41b5-8064-6b38fc5a1198.jpg"
stills:
- "/public/images/3d50594e-0e93-4737-998b-71aeb57d4535.jpg"
- "/public/images/e107c074-84a4-48d0-b824-4c137ccef438.jpg"
- "/public/images/07d25fb5-de4a-4922-bb7d-7a8db345b5c3.jpg"
- "/public/images/e4145b40-2ea0-4238-a12d-04d4ae70e893.JPG"
pais: México
estado: Morelos
municipio: Tepoztlán
año_de_inicio_de_la_produccion: '2008'
año: '2012'
duracion: '86'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- Mujeres
- Migrantes
- Albergue
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Oaxaca
sinopsis_espaniol: Frente a las vías del tren, el sacerdote Alejandro Solalinde funda
  y construye un albergue en Ixtepec, Oaxaca para dar refugio y alivio espiritual
  a inmigrantes centroamericanos. La lucha de Solalinde se entrecruza con las cambiantes
  vidas de mujeres y hombres que movidos por la fe y la necesidad de trabajo digno,
  aguardan el momento de desafiar los peligros de su viaje al norte.
sinopsis_ingles: Across from the railroad tracks, priest Alejandro Solalinde founds
  and builds a shelter in Ixtepec, Oaxaca to provide refuge and spiritual relief to
  Central American migrants. Solalinde’s struggle is interwoven with the changing
  lives of women and men who, motivated by their faith and need for dignified work,
  wait for their moment to defy danger and continue their journey to the north.
link_al_trailer: https://vimeo.com/37800705
link_para_visionado: https://vimeo.com/37800705
festivales:
- festival: Festival Internacional de Cine de Guadalajara
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Alejandra Islas
  produccion:
  - Eli Bartolo
  casa_productora:
  - Rabacanda films
  - Utopía video producciones
  guion:
  - Alejandra Islas
  investigacion:
  - Alejandra Islas
  fotografia:
  - Alejandra Islas
  sonido:
  - Jaime Pavón
  edicion:
  - Alejandro Quesnel
  - Alejandra Islas
  musica_original:
  - Fernando Mariña
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Alejandra islas
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1962-01-01'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '0000000000'
  telefono_celular_publico: 'false'
  correo_electronico: alejandra.islas@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: 'Alejandra Islas '
  telefono: '0000000000'
  correo_electronico: alejandra.islas@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: IMCINE
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '50'
  privada: '0'
  propia: '50'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: El albergue
order: 41
---

