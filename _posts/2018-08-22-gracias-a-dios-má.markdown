---
title: Gracias a Dios má
poster: "/public/images/37eb8d1a-b9f5-4276-bb44-ac86007fdbbd.jpg"
stills:
- "/public/images/abc939c9-9e49-4c24-a43f-cb80ce591e58.jpg"
- "/public/images/abc939c9-9e49-4c24-a43f-cb80ce591e58.jpg"
- "/public/images/abc939c9-9e49-4c24-a43f-cb80ce591e58.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '19'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Jóvenes
- Vida Rural
- Niños
- Pobreza
- superación
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Héctor tiene ocho años. Héctor junto con sus hermanos se ven obligados
  a trabajar para conseguir comida y sustento, aunque esto implique sacrificar su
  educación. Su madre esta preocupada porque Héctor dedica más tiempo al trabajo que
  la escuela. Mientras tanto su hermano mayor (Luis) sufre un accidente  trabajando
  con su padre en la construcción. Héctor se siente presionado por la situación y
  busca la manera de conseguir dinero para solucionar la situación Familiar.
sinopsis_ingles: Hector is eight years old and live in a family poor. The economic
  situation of his family is very bad, accordingly Hector and his brothers have to
  work for get food and  sacrifice the education for sustain their family. Hector’s
  mother  is very concerned because Hector spend more time in the job while Hector's
  brother (Luis) has a injury in his leg in consequence for a accident in his job.
  Hector feel very stress for the economic situation  and find the solution to help
  his family.
link_para_visionado: https://ww.youtube.com
festivales:
- festival: Festival Zanate
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Felipe Alejandro Castillo García
  produccion:
  - Felipe Alejandro Castillo García
  guion:
  - Felipe Alejandro Castillo García
  investigacion:
  - Felipe Alejandro Castillo García
  fotografia:
  - Felipe Alejandro Castillo García
  sonido:
  - Felipe Alejandro Castillo García
  edicion:
  - Felipe Alejandro Castillo García
  otros:
  - nombre: Felipe Alejandro Castillo García
    rol: Postproducción de imagen
  - nombre: Felipe Alejandro Castillo García
    rol: Postproducción de sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Felipe Alejandro Castillo García
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '3959054'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "(044)2221856859"
  telefono_celular_publico: 'false'
  correo_electronico: nike_boy97@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Felipe Alejandro Castillo García
  telefono: "(044)2221856859"
  correo_electronico: nike_boy97@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: Gracias a Dios má
order: 51
---

