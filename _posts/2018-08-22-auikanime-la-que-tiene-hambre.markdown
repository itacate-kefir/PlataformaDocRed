---
title: "Auikanime \"La que tiene hambre\"\t\t\t\t\t\t"
poster: "/public/images/c560cfd2-8640-47a2-ba97-db9ff6677345.JPG"
stills:
- "/public/images/e4581eab-0490-4e0c-a3cd-b5b705316223.JPG"
- "/public/images/df24410a-5849-48de-b43d-533c95caa370.JPG"
- "/public/images/e1626207-eae1-420c-945a-c4626ac29b2a.JPG"
pais: México
estado: Michoacán
municipio: Michoacán
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '37'
idioma_original:
- Español
- purhepecha
idiomas_de_los_subtitulos:
- Inglés
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
palabras_clave:
- Indígenas
- Vida Rural
genero_y_estilo:
- 'Falso documental: Película que documenta una falsa hipótesis.'
locaciones:
  paises:
  - México
  estados:
  - Michoacán
sinopsis_espaniol: "En el año de 1530 el yugo de los españoles invadores y los afanes
  evangelizadores de los frailes franciscanos pesa fuertemente sobre el pueblo P'urhepecha
  y sus creencias. Hopotaku y Tsïpa, un joven matrimonio, enfrentan el cruel cambio
  de orden a la vez que son visitados por la misteriosa y mitológica Auikanime, ser
  que les hará tomar deciciones que los cambiaran para siempre\t\t\t\t\t\t\t"
sinopsis_ingles: "In the year of 1530, the yoke of spanish conquistadors and Roman
  Catholic priest was heavy on the P'urhepecha people. This tale blends P'urhepecha
  history with the ancient mythology of Michoacan to tell the story of a young family
  and a mysterious being known as the Auikanime\t\t\t\t\t\t\t"
link_para_visionado: https://www.youtube.com/watch?v=OEJHB-sqN88
festivales:
- festival: Festival Internacional de Guadalajara
  año_de_edicion: 1st
- festival: X Festival de Cine de Morelia
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pavel Rodríguez Guillén
  produccion:
  - Captura Visual Producciones/Secretría de Cultura, Michoacán
  casa_productora:
  - Captura Visual Producciones
  guion:
  - Pavel Rodríguez Guillén
  fotografia:
  - Jairo Torres Gasca
  sonido:
  - Javier Wenses Estrada, Kevin Vargas Torres
  edicion:
  - Pavel Rodríguez Guillén
  musica_original:
  - Pavel Rodríguez Guillén
directorxs:
- rol:
  - Director(a)
  nombre: Pavel Rodríguez Guillén
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1973-10-26'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 4431-89-00-69
  telefono_celular_publico: 'false'
  correo_electronico: dos_conejo@yahoo.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Roberto Pavel Rodríguez Guillén\t\t\t\t\t"
  telefono: "4431-89-00-69\t\t"
  correo_electronico: dos_conejo@yahoo.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Plataformas digitales gratuitas
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '20'
  privada: '20'
  propia: '60'
recaudacion: De 30-50 mil pesos
layout: ficha_documental
titulo_en_ingles: "Auikanime \"La que tiene hambre\"\t\t\t\t\t\t"
order: 35
---

