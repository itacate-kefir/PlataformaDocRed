---
title: Tarifas justas
poster: "/public/images/6b4f5362-f139-4d8f-a56e-9e58075eb3e5.JPG"
stills:
- "/public/images/491475a7-e0fc-4e2a-b804-4e35464f1aa0.JPG"
- "/public/images/928debfd-81d0-4f0e-8dd9-d2cf78806a1f.JPG"
pais: México
estado: Chiapas
municipio: Mapastepec
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '12'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Movimientos Sociales
- Activismo social
- Luchas sociales
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Chiapas
sinopsis_espaniol: 'Ante los altas tarifas de energía eléctrica que cobra la Comisión
  Federal de Electricidad en algunas zonas humildes en Mapastepec, Chiapas, se formó
  el Frente de Defensa Popular 10 de Abril, que encontró la manera legal de pagar
  lo que considera justo. '
sinopsis_ingles: "Against the high price of energy that Federal Electricity Commission
  charge in some modest zones of Chiapas, the April 10 Popular Defense Front was formed,
  finding the legal way to pay what they consider fare.\t\t\t\t\t\t\t\r\n"
link_para_visionado: https://vimeo.com/82595953
festivales:
- festival: Los Angeles Movie Awards
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Los Angeles Movie Awards 2011
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Ezequiel Sanchez
  produccion:
  - Monica Parra
  casa_productora:
  - Ninguna
  guion:
  - Federico Denegri
  investigacion:
  - Proyecto Chakana
  fotografia:
  - Federico Denegri
  sonido:
  - Ezequiel Sanchez
  edicion:
  - Ezequiel Sanchez
  - Federico Denegri
directorxs:
- rol:
  - Productor(a)
  nombre: Mónica Parra
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1990-01-01'
  telefono_fijo:
  - 222 504 9982
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+52 1 222 504 9982"
  telefono_celular_publico: 'false'
  correo_electronico: mparrah17@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: "Ezequiel Sanchez\t\t\t\t\t"
  nacionalidad: Argentina
  fecha_de_nacimiento: '1980-02-01'
  telefono_fijo:
  - 9 11 4087 1121
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+54 9 11 4087 1121"
  telefono_celular_publico: 'false'
  correo_electronico: ezequielsanchez@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Mónica Parra\t\t\t\t\t"
  telefono: "+52 1 222 504 9982\t\t"
  correo_electronico: mparrah17@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Tarifas justas
order: 16
---

