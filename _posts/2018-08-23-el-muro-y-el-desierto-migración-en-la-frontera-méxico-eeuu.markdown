---
title: "El muro y el desierto: Migración en la frontera México-EEUU\t\t\t\t\t\t"
poster: "/public/images/21db8b35-b0f8-46c1-9114-cd48ef42fe8c.jpg"
stills:
- "/public/images/bed88766-a7a8-41d0-9486-cd555b2f37b8.jpg"
- "/public/images/bed88766-a7a8-41d0-9486-cd555b2f37b8.jpg"
- "/public/images/bed88766-a7a8-41d0-9486-cd555b2f37b8.jpg"
- "/public/images/bed88766-a7a8-41d0-9486-cd555b2f37b8.jpg"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2002'
año: '2002'
duracion: '49'
idioma_original:
- Español
- Inglés
idiomas_de_los_subtitulos:
- Inglés
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Fronteras, migraciones y exilios: Historias de migrantes y las fronteras reales
  y simbólicas que enfrentan'
palabras_clave:
- Migrantes
- Muro fronterizo
- Discriminación
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'De investigación: El hilo conductor se sustenta en la investigación de un tema.'
locaciones:
  paises:
  - México
  - Estados Unidos
  estados:
  - Morelos
sinopsis_espaniol: "México es ya un corredor migratorio. El muro en la frontera de
  México con EEUU no solo aparenta detener a la gente, es un filtro que selecciona
  la mano de obra que los EEUU quiere que llegue a trabajar a su país, los efectos
  que ocasiona este muro son aprovechados para adoctrinar y amaestrar a los trabajadores
  que al cruzar la frontera serán explotados intensamente y aprovechados hasta ya
  no serle útil a la potencia económica más importante de este siglo.\t\t\t\t\t\t"
sinopsis_ingles: Mexico is already a migratory corridor. The wall on the border of
  Mexico with the US not only seems to stop people, it is a filter that selects the
  labor that the US wants to get to work in their country, the effects caused by this
  wall are used to indoctrinate and train workers who cross the border will be intensively
  exploited and exploited until it is no longer useful to the most important economic
  power of this century.
link_al_trailer: https://www.youtube.com/watch?v=5UsdpUMaYnw
link_para_visionado: https://www.youtube.com/watch?v=5UsdpUMaYnw
festivales:
- festival: Festival de la Memoria Tepoztlán
  año_de_edicion: 'Primera '
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: Festival de la Memoria Tepoztlán
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pablo Gleason González
  produccion:
  - Pablo Gleason González
  casa_productora:
  - Push And Play
  guion:
  - Armando Villegas
  investigacion:
  - Pablo Gleason González
  - Victor Hugo Sánchez Reséndiz
  - Kim Erno
  - Armando Villegas Contreras
  fotografia:
  - Archivo Colegio de l aFrontera Norte
  sonido:
  - Pablo Gleason González
  edicion:
  - Pablo Gleason González
  musica_original:
  - Humberto Alvaréz
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Pablo Gleason González\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1978-04-11'
  telefono_fijo:
  - '603273235'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+33603273235"
  telefono_celular_publico: 'false'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Pablo Gleason González\t\t\t\t\t"
  telefono: "+33603273235\t\t\t\t"
  correo_electronico: pablo.gleason@filmsdalterite.fr
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "El muro y el desierto: Migración en la frontera México-EEUU\t\t\t\t\t\t"
order: 63
---

