---
title: "Aprendiendo a perder el miedo\t\t\t\t\t\t"
poster: "/public/images/9def6fb8-3cda-41dd-a83c-db6ab22d5d82.png"
stills:
- "/public/images/53f2f003-b8e9-4599-9eef-cfd6154997bd.png"
- "/public/images/bf3856d7-faab-4062-9289-d5abae987d04.png"
- "/public/images/cf5eb4fc-8912-4ff6-99ac-6e17de8141fd.png"
pais: México
estado: Jalisco
municipio: Guadalajara
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '4'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- Desarrollo Social
- discapacidad
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - jalisco
sinopsis_espaniol: "Un Documental Corto que explora diferentes razones por las cuales
  la gente cree que se discrimina a las personas con alguna discapacidad como son
  el miedo y la ignorancia.\t\t\t\t\t\t\t"
sinopsis_ingles: "A short documentary in which the reasons of why we discriminate
  people with some sort of disability are explored like fear or ignorance.\t\t\t\t\t\t\t"
link_al_trailer: "http://www.vimeo.com/82736293\t\t\t\t\t\t"
link_para_visionado: "http://www.vimeo.com/82736293\t\t\t\t\t\t"
festivales:
- festival: Noviembre es corto
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Tercer Lugar
  institución: Concurso "Atrévete a ver"
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Cristóbal González Camaréna
  produccion:
  - Inés Toussaint Orendáin
  casa_productora:
  - Unlimited Films
  guion:
  - Cristóbal González Camarena
  fotografia:
  - Ulises Galaviz Jiménez
  sonido:
  - Carlos de la Madrid
  edicion:
  - Jorge G. Camarena
  musica_original:
  - Dexter Britain
directorxs:
- rol:
  - Director(a)
  nombre: "Cristóbal González Camaréna\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1989-12-03'
  telefono_fijo:
  - 38 480808
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 333 570 5857
  telefono_celular_publico: 'false'
  correo_electronico: criztobal_5@hotmail.com
  correo_electronico_publico: 'true'
- rol:
  - Productor(a)
  nombre: "Inés Toussaint Orendáin\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1990-12-16'
  telefono_fijo:
  - '3331655023'
  telefono_fijo_publico: ''
  telefono_celular:
  - '3310763992'
  telefono_celular_publico: ''
  correo_electronico: inestoussaint@gmail.com
  correo_electronico_publico: ''
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Inés Toussaint Orendáin\t\t\t\t\t"
  telefono: "3331655023\t"
  correo_electronico: inestoussaint@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "Aprendiendo a perder el miedo\t\t\t\t\t\t"
order: 37
---

