---
title: El "H" documental
poster: "/public/images/2c32b204-4a5c-4a75-bcbb-acd1efbf9c73.jpg"
stills:
- "/public/images/83c81933-1e5e-454a-ba4a-495c89422df4.jpg"
- "/public/images/6117b502-f613-49f4-9f08-98edfc6b7f39.jpg"
- "/public/images/c5e2c443-dba4-4591-84f0-6a24e499f1b0.jpg"
pais: México
estado: Tlaxcala
municipio: Tlaxcala
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '29'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- fútbol
- deporte
- equipo
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - tlaxcala
sinopsis_espaniol: El H documental es la historia del H-24, un equipo de futbol llanero
  que nació en el pueblo de San Ildefonso Hueyotlipan, Tlaxcala. Con mas de 25 años
  de historia y un sinfín de anecdotas por contar, se presentan algunos de los jugadores
  que vieron el tiempo pasar en este equipo amateur de futbol y que nos muestran sus
  vivencias. Con este breve recorrido, se rinde un homenaje a los clubs deportivos
  que se crean en la provincia de México, que hacen que el futbol sea un motivo más
  para convivir co
sinopsis_ingles: "The H documentary is the story of H-24, a ranger football team born
  in the village of San Ildefonso Hueyotlipan, Tlaxcala. With over 25 years of history
  and endless anecdotes to tell, are some of the players who saw time pass on this
  amateur soccer team and show us their experiences. With this brief, a tribute is
  paid to sports clubs that are created in the province of Mexico, that make the football
  is another reason to live together as a team, as a family and as a community.\t\t\t\t\t\t\t"
link_para_visionado: http://www.youtube.com
festivales:
- festival: 2° Festival de Cortometrajes de Puebla REC 2010
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Martín Jerónimo Becerra Avelino
  produccion:
  - Martín Jerónimo Becerra Avelino
  guion:
  - Martín Jerónimo Becerra Avelino
  fotografia:
  - Martín Jerónimo Becerra Avelino
  - José Daniel Ojeda Rojas
  sonido:
  - José Pablo Bedolla Becerra
  edicion:
  - Martín Jerónimo Becerra Avelino
  - José Daniel Ojeda Rojas
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Martín Jerónimo Becerra Avelino\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1984-07-19'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2411035259'
  telefono_celular_publico: 'false'
  correo_electronico: MUBA1319@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Martín Jerónimo Becerra Avelino\t\t\t\t\t"
  telefono: '2411035259'
  correo_electronico: MUBA1319@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: El "H" documental
order: 19
---

