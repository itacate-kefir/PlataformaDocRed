---
title: Dothefootage
poster: "/public/images/d17ebcf1-2244-4ea0-aad0-a3018b420d2c.png"
stills:
- "/public/images/a310ac45-8885-4b6e-9c47-61211191a3ff.png"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '1'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- Danza
- Video
genero_y_estilo:
- 'Found footage / Reapropiación: Utiliza material de archivo y lo resignifica para
  contar una nueva historia'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: "Dothefootage\r\n"
sinopsis_ingles: "Dothefootage\r\n\r\n"
link_para_visionado: https://www.youtube.com
festivales:
- festival: Festival itinerante de video danza AGITE Y SIRVA
  año_de_edicion: '2011'
- festival: Festival Internacional de Guadalajara
  año_de_edicion: '2012'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Pública
equipo_de_produccion:
  direccion:
  - Gabriela Domínguez Ruvalcaba
  produccion:
  - Gabriela Domínguez Ruvalcaba
  casa_productora:
  - Bosque negro
  guion:
  - Gabriela Domínguez Ruvalcaba
  fotografia:
  - Gabriela Domínguez Ruvalcaba
  sonido:
  - Gabriela Domínguez Ruvalcaba
  edicion:
  - Gabriela Domínguez Ruvalcaba
  musica_original:
  - 'Todos los derechos de la música pertenecen a  Justice: DO THE D.A.N.C.E. // álbum
    A cross the universe // discográfica Atlántic Federico Vellanoweth: Gimnasia en
    su hogar vol.2 // Columbia Harmony'
  otros:
  - nombre: Gabriela Domínguez Ruvalcaba
    rol: Postproducción de imagen
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Gabriela Domínguez Ruvalcaba
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - 2221 186 430
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 2221 186 430
  telefono_celular_publico: 'false'
  correo_electronico: gabsdomru@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Gabriela Domínguez Ruvalcaba
  telefono: 2221 186 430
  correo_electronico: gabsdomru@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Dothefootage
order: 49
---

