---
title: Lo que se quedó guardado
poster: "/public/images/e7866640-c807-4cb8-9bdb-60fa56a76c57.jpg"
stills:
- "/public/images/828e8020-1ad0-4eaf-889c-ab972b55cfe8.jpg"
- "/public/images/64686cf9-08f6-447b-a127-c17aba2dc4a7.jpg"
- "/public/images/d3ecde70-afd4-4ae6-a272-471e00ac6850.jpg"
pais: México
estado: Jalisco
municipio: Jalisco
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '13'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- actriz
- descubrimiento
- terapia
- pérdida
genero_y_estilo:
- 'Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción
  del mismo para proponer nuevas formas fílmicas'
- 'De autor /De creación: Su apuesta se basa en la mirada personal del autor.'
locaciones:
  paises:
  - México
  estados:
  - jalisco
sinopsis_espaniol: En esta película, el cineasta junta el trabajo psicoterapéutico
  y el cine. Acompañó a la actriz nayarita Ana Serrano, 25, en un proceso de encontrarse  a
  sí misma. La película trata del proceso de perder a un ser querido y el cómo enfrentar
  esta pérdida.
link_para_visionado: http://www.youtube.com
festivales:
- festival: Herceg Novi Film Festival Montanegro
  año_de_edicion: '2012'
- festival: Semana del Audiovisual Buenos Aires
  año_de_edicion: '2013'
- festival: LA Meko Film Festival Landau
  año_de_edicion: '2013'
- festival: DukaFest Banja Luka, Bosnia y Herzovia
  año_de_edicion: '2011'
- festival: Filmfest Dusseldorf
  año_de_edicion: '2011'
- festival: Exhibición anual HFBK Hamburgo
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Bernhard Hetzenauer
  produccion:
  - Bernhard Hetzenauer
  - Guillén Errecalde
  casa_productora:
  - Cinescopio Films Guadalajara
  - HFBK Hamburg
  guion:
  - Bernhard Hetzenauer
  - Ana Serrano
  fotografia:
  - Bernhard Hetzenauer
  sonido:
  - Héctor Saavedra
  edicion:
  - Bernhard Hetzenauer
  musica_original:
  - Marco Antonio Lujan
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Bernhard Hetzenauer
  nacionalidad: Alemana
  fecha_de_nacimiento: '1981-02-09'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 005213318260750
  telefono_celular_publico: 'false'
  correo_electronico: bernhard.hetzenauer@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Bernhard Hetzenauer
  telefono: 005213318260750
  correo_electronico: bernhard.hetzenauer@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras internacionales
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: HFBK Hamburg
  pais: Alemania
  estado: Ciudad de México
- coproductor: Cinescopio Films
  pais: México
  estado: Guadalajara
porcentaje_de_financiacion:
  estatal: '0'
  privada: '50'
  propia: '50'
recaudacion: De 30-50 mil pesos
layout: ficha_documental
titulo_en_ingles: Lo que quedó guardado -was unausgesprochen blieb
order: 36
---

