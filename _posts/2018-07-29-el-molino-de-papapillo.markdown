---
title: El Molino de Papapillo
stills:
- "/public/images/96fb1ebd-6122-4b65-b6be-159358bcd447.jpg"
- "/public/images/408fbef6-39b7-4dbd-ac13-3dbdb50d89f5.jpg"
- "/public/images/53ea1e72-ec7d-4728-b66c-5343fff600b7.jpg"
pais: México
estado: Veracruz
municipio: Huatusco
año_de_inicio_de_la_produccion: '2001'
año: '2011'
duracion: '7'
idioma_original: Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Niñez, juventud, tercera edad: Historias marcadas por la edad de sus personajes'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Nixtamal
- Generaciones
- Abuelo
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - Veracruz
sinopsis_espaniol: 'El molino de nixtamal mi abuelo Elpidio "Papapillo" ha sido
  el escenario de la convivencia de 5 generaciones. '
sinopsis_ingles: The corn mill my abueno Elpidio "Papapillo" has been the scene of
  coexistence of 5 generations.
link_para_visionado: https://www.youtube.com/watch?v=C-XyykGfckk
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Idzin Xaca Avendaño
  - Rafael Arcos Sayago
  produccion:
  - José Emiliano Xaca Silvestre
  casa_productora:
  - Tres Cuartos de Media
  guion:
  - Idzin Xaca Avendaño
  fotografia:
  - Idzin Xaca Avendaño
  sonido:
  - Rafael Arcos Sayago
  edicion:
  - Idzin Xaca Avendaño
directorxs:
- rol:
  - Director(a)
  nombre: Idzin Xaca Avendaño
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1982-01-06'
  telefono_fijo:
  - '2289884478'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '2289884478'
  telefono_celular_publico: 'false'
  correo_electronico: idzofacto@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '2289884478'
  telefono_publico: 'false'
  correo_electronico: idzofacto@gmail.com
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Idzin Xaca Avendaño
  telefono: 228 9884478
  correo_electronico: idzofacto@gmail.com
  sitio_web: https://about.me/idzo
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: El Molino de Papapillo
order: 8
---

