---
title: "La Merma, 15 años de camino.\t\t\t\t\t\t"
poster: "/public/images/d18e1a1c-9305-4209-bc23-3a1ee418fbd6.jpg"
stills:
- "/public/images/98480efa-b943-4d48-a435-df6c0bf9bc61.jpg"
- "/public/images/945c342c-191c-4336-98d3-f5fce964a5a4.jpg"
- "/public/images/bfff08f3-c5fe-4c97-874a-208860a11d93.jpg"
pais: México
estado: Sonora
municipio: Sonora
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '63'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Musical: Artistas, agrupaciones, giras, fenómenos musicales'
palabras_clave:
- Música
- punk
- frontera
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'De autor /De creación: Su apuesta se basa en la mirada personal del autor.'
locaciones:
  paises:
  - México
  estados:
  - sonora
sinopsis_espaniol: "Una de las bandas mas representativas del punk rock en la frontera
  norte de México da testimonio de su historia, de su escandaloso recorrido, de la
  vida en una ciudad contrastante (Nogales, Sonora) que por momentos parece desvanecerse.
  Tres discos, numerosos integrantes y quince años de camino. Un acercamiento a los
  eventos que movieron a toda una generación hambrienta de identidad. Una mirada al
  desencanto, a la frontera violentada, desde la perspectiva de una banda apasionada
  por su realidad.\t"
link_al_trailer: "http://www.youtube.com/watch?v=x9fDkvc9aMU\t\t\t\t\t\t"
link_para_visionado: "http://www.youtube.com/watch?v=x9fDkvc9aMU\t\t\t\t\t\t"
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Tino Varela
  produccion:
  - Tino Varela
  guion:
  - Tino Varela
  fotografia:
  - Tino Varela
  sonido:
  - Tino Varela
  edicion:
  - Tino Varela
  musica_original:
  - La Merma Punk Rock
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Tino Varela (Agustín Roberto Varela Echeverria)\t\t\t\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1983-08-27'
  telefono_fijo:
  - 631-3137258
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '6311300300'
  telefono_celular_publico: 'false'
  correo_electronico: tinovar1@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Tino Varela (Agustín Roberto Varela Echeverria)\t\t\t\t\t"
  telefono: "631-3137258\t\t"
  correo_electronico: tinovar1@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: "La Merma, 15 años de camino.\t\t\t\t\t\t"
order: 30
---

