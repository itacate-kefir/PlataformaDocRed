---
title: Cholula, un retrato orgánico
poster: "/public/images/123d4506-b76a-476f-a399-6a5f414f03ac.jpg"
stills:
- "/public/images/9b20cb4d-f11e-49be-bcba-904e809256a3.jpg"
- "/public/images/9b20cb4d-f11e-49be-bcba-904e809256a3.jpg"
- "/public/images/9b20cb4d-f11e-49be-bcba-904e809256a3.jpg"
pais: México
estado: Ciudad de México
municipio: Ciudad de México
año_de_inicio_de_la_produccion: '2008'
año: '2008'
duracion: '10'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- Puebla
- momentos
- cholula
genero_y_estilo:
- 'Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción
  del mismo para proponer nuevas formas fílmicas'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Documental experimental que retrata desde una perspectiva distinta
  un momento y un lugar determinado. A través de una cadena de palabras similar a
  un cadáver exquisito se pretende vislumbrar el inconsciente colectivo de Cholula.
sinopsis_ingles: This is a experimental documentary which uses a chain of words to
  get a glimpse of the collective subconscious of a town named Cholula.
link_al_trailer: http://www.youtube.com/watch?v=rX3quysc7uI
link_para_visionado: http://www.youtube.com/watch?v=rX3quysc7uI
festivales:
- festival: "“EL CARRETE“ 4th festival de cortometrajes de la UDLAP"
  año_de_edicion: '2008'
- festival: "“CortoCINEMA“ Festival de Cortometrajes de la Universidad de la Salle
    Bajío"
  año_de_edicion: '2009'
- festival: Proyección en ciclo de documentales “REVOLVER“ de la BUAP
  año_de_edicion: '2010'
premios_y_reconocimientos:
- reconocimiento: Primer lugar
  institución: Festival “EL CARRETE“ 2008
- reconocimiento: Segundo Lugar
  institución: "“CortoCINEMA“ Universidad de la Salle Bajío 2009"
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Javier Avilés Chico
  produccion:
  - Javier Avilés Chico
  casa_productora:
  - Producciones Nocturno Mejor Que Nunca
  guion:
  - Javier Avilés Chico
  - Francisco Salgado
  investigacion:
  - Javier Avilés Chico
  fotografia:
  - Javier Avilés Chico
  sonido:
  - Javier Avilés Chico
  edicion:
  - Javier Avilés Chico
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Javier Avilés Chico
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1983-05-30'
  telefono_fijo:
  - '5585487415'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '5585487415'
  telefono_celular_publico: 'false'
  correo_electronico: javileschico@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Javier Avilés Chico
  telefono: '5585487415'
  correo_electronico: javileschico@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: Cholula, un retrato orgánico
order: 74
---

