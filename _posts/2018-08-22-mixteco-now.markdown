---
title: "Mixteco now\t\t\t\t"
poster: "/public/images/d504cfce-fddf-4b1a-a551-6c99ed1a75b8.jpg"
stills:
- "/public/images/f2d06c53-dcfa-466d-9da9-4603b7cf7936.jpg"
- "/public/images/9c459c98-c375-4ec5-bf72-0c64eaf40d2c.jpg"
pais: México
estado: Oaxaca
municipio: Oaxaca
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '10'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- Super 8mm
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- Cultura
- Migrante
- Mixteca
- Búsqueda
- Raíces
genero_y_estilo:
- 'Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción
  del mismo para proponer nuevas formas fílmicas'
locaciones:
  paises:
  - México
  estados:
  - Oaxaca
sinopsis_espaniol: "Cortometraje experimental desde la mirada del migrante, en una
  búsqueda por las raíces mixtecas de Oaxaca, al reencontrarse con su pueblo fantasma
  y una cultura disgregada.\r\n"
sinopsis_ingles: Short Film Experimental, from look migrant  in a search for the roots
  Mixtec’s of Oaxaca, to meet his ghost town and a disintegrated culture.
link_para_visionado: https://www.youtube.com
festivales:
- festival: Festival Internacional de Cine de Morelia
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Edson Caballero Trujillo
  produccion:
  - Edson Caballero Trujillo
  casa_productora:
  - INFRATIERRA PRODUCTORA
  guion:
  - Edson Jair Caballero Trujillo
  investigacion:
  - Edson Caballero Trujillo
  fotografia:
  - Edson Caballero Trujillo
  sonido:
  - Edgar Arrellin Rosas
  edicion:
  - Edson Caballero Trujillo
  musica_original:
  - LUZ DE RIADA
  otros:
  - nombre: Ezequiel Reyes
    rol: Postproducción de Imagen
  - nombre: Edson Caballero Trujillo
    rol: Postproducción de Imagen
  - nombre: Edgar Arrellin Rosas
    rol: Postproducción de Sonido
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Edson Caballero Trujillo
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-10-01'
  telefono_fijo:
  - "(+951)5170207"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '9511123492'
  telefono_celular_publico: 'false'
  correo_electronico: caballeroedson@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Edson Jair Caballero Trujillo\t\t\t\t\t"
  telefono: '9511123492'
  correo_electronico: caballeroedson@hotmail.com
  sitio_web: https://edsoncaballero.wordpress.com/
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 10-30 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: "Mixteco now\t\t\t\t"
order: 28
---

