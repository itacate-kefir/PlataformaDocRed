---
title: "Tierra de maíz\t\t\t\t\t\t"
poster: "/public/images/a511dcd6-49b9-4691-b223-3354bac0edad.jpg"
stills:
- "/public/images/a15415b6-d585-4967-9b67-cbf2c5a997a9.jpg"
- "/public/images/a15415b6-d585-4967-9b67-cbf2c5a997a9.jpg"
- "/public/images/a15415b6-d585-4967-9b67-cbf2c5a997a9.jpg"
- "/public/images/a15415b6-d585-4967-9b67-cbf2c5a997a9.jpg"
pais: México
estado: Morelos
municipio: Cuernavaca
año_de_inicio_de_la_produccion: '2004'
año: '2004'
duracion: '35'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Cultura: Tradiciones, patrimonio'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
palabras_clave:
- Campos de cultivo
- Rescate del Maíz
- Autonomía alimenticia
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Morelos
sinopsis_espaniol: "Realizado en los pueblos del oriente y los Altos de Morelos donde
  se busca rescatar el uso de maíz criollo como forma de mantener autonomía alimenticia
  y cultura en los Pueblos. Un acercamiento a la intimidad de su cultura, sus fiestas
  y tradiciones y el amor que se le tiene a este grano milenario. \t\t\t\t\t\t\t\r\n"
sinopsis_ingles: "Made in the towns of the east and the Altos de Morelos where it
  is sought to rescue the use of Creole corn as a way to maintain food autonomy and
  culture in the Peoples. An approach to the intimacy of its culture, its festivals
  and traditions and the love that you have for this millennial grain.\t\t\t\t\t\t\t\r\n"
link_al_trailer: https://www.youtube.com/watch?v=YLReFX8Vo8k
link_para_visionado: https://www.youtube.com/watch?v=YLReFX8Vo8k
festivales:
- festival: Encuentro Hispanoamericano de cine y video documental independiente "Voces
    contra el silencio"
  año_de_edicion: 2da edición
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Pablo Gleason González
  produccion:
  - Pablo Gleason González
  casa_productora:
  - Push and play
  guion:
  - Victor Hugo Sánchez Reséndiz
  investigacion:
  - Pablo Gleason González
  - Armando Villegas Contreras
  - Victor Hugo Sánchez Reséndiz
  fotografia:
  - Pablo Gleason González
  sonido:
  - Pablo Gleason González
  edicion:
  - Pablo Gleason González
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Pablo Gleason González
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1978-11-04'
  telefono_fijo:
  - '603273235'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+33603273235"
  telefono_celular_publico: 'false'
  correo_electronico: pablo.gleason@filmsdalterite.fr
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Pabo Gleaosn González\t\t\t\t\t"
  telefono: "+33603273235"
  correo_electronico: pablo.gleason@filmsdalterite.fr
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "Tierra de maíz\t\t\t\t\t\t"
order: 64
---

