---
title: "Flai Emiratos\t\t\t\t\t\t"
poster: "/public/images/82c3a57c-c9f1-46d4-8d72-0f599d954a41.png"
stills:
- "/public/images/90e54026-19ca-409b-b249-2ac48f640ff6.png"
- "/public/images/b99d81e4-3e33-453f-a861-aa77d037d6f0.png"
- "/public/images/2f752e72-8f11-44ec-bbd2-dd16842e79a3.png"
pais: México
estado: Tamaulipas
municipio: Tamaulipas
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '70'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- fútbol
- cárcel
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - Tamaulipas
sinopsis_espaniol: Al penalde Altamira Tamaulipas lo acompañan diversas historias
  sobre la alta peligrosidad de sus internos, por ubicarse en uno de los estados con
  mayor violencia derivada del narcotráfico en México. El futbol es un elemento de
  catarsis para la mayoría de la población penitenciaria. Flai Emiratos es el equipo
  más popular del torneo interno de esta prisión, sus jugadores encuentran su muy
  particular redención cuando saltan a la cancha. Este grupo de futbolistas-reclusos
  con un elemental equipo.
sinopsis_ingles: At Altamira Tamaulipas Jail accompany various stories about the hight
  danger of internal  , to be located in one of the states whit the highest derivate
  drugs violence in México. Football is an element of catharsis for the majority of
  prision population. Flai Emiratos is the most popular team in the tournament this
  prison, his players are very particular redemption when they jump on to the field.
  This group of players-prisoners whit basic video recording equipment made themselves
  behind bars.
link_para_visionado: http://www.youtube.com
festivales:
- festival: Festival Internacional de Guadalajara
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: EFICINE 226 / ASUR,  GRUPO AEROPORTUARIO DEL SURESTE
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Realización Colectiva por parte de los internos del Centro de Ejecución de Sanciones
    de Altamira Tamaulipas
  produccion:
  - Fabián Ramos Covarrubias
  guion:
  - Fabián Ramos Covarrubias
  fotografia:
  - Realización Colectiva por parte de los internos del Centro de Ejecución de Sanciones
    de Altamira Tamaulipas
  sonido:
  - Realización Colectiva por parte de los internos del Centro de Ejecución de Sanciones
    de Altamira Tamaulipas
  edicion:
  - Luis Eduardo Carmona / Fabián Ramos Covarrubias
  musica_original:
  - Jesús Maldonado / Don Mysterio / Ángel de Jesús Bautista
directorxs:
- rol:
  - Productor(a)
  nombre: "Fabián Ramos Covarrubias\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-09-18'
  telefono_fijo:
  - '0000000000'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 833 844 81 42
  telefono_celular_publico: 'false'
  correo_electronico: film.tamps@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Fabián Ramos Covarrubias\t\t\t\t\t"
  telefono: "833 844 81 42\t\t"
  correo_electronico: film.tamps@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: "Sociedades Fílmicas de Tamaulipas\t\t\t\t\t"
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: Flai Emiratos
order: 27
---

