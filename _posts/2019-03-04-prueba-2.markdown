---
pais_de_la_persona: En blanco
estado_de_la_persona: En blanco
municipio_de_la_persona: En blanco
consentimiento: 'false'
pais: En blanco
estado: En blanco
municipio: En blanco
link_para_visionado_publico: 'false'
festivales:
- festival: En blanco
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: En blanco
  institución: En blanco
becas_y_apoyos:
- beca: En blanco
  monto: ''
  institución: En blanco
  tipo: Privada
equipo_de_produccion:
  otros:
  - nombre: En blanco
    rol: En blanco
directorxs:
- nombre: ''
  nacionalidad: En blanco
  fecha_de_nacimiento: ''
  telefono_fijo_publico: 'false'
  telefono_celular_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  grados_academicos:
  - grado_academico: En blanco
    escuela: En blanco
distribuidoras:
- nombre: En blanco
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: ''
  telefono: ''
  correo_electronico: ''
  sitio_web: ''
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: En blanco
  estado: En blanco
porcentaje_de_financiacion:
  estatal: ''
  privada: ''
  propia: ''
recaudacion: De 50-100 mil pesos
layout: ficha_documental
title: Prueba 2
dir: ltr
incomplete: true
author: g@kefir.red
order: 87
---

