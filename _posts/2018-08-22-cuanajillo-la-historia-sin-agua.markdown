---
title: "Cuanajillo La Historia Sin Agua\t\t\t\t\t\t"
poster: "/public/images/ff23fe43-e68f-4cdf-b8ef-5f221966db72.png"
stills:
- "/public/images/1a60dd24-6f69-4ba2-9e01-53a5748ae013.png"
- "/public/images/4b6403eb-f80a-461f-9b82-e927d38f293a.png"
- "/public/images/5e811741-90b6-4387-a258-41a163611cff.png"
pais: México
estado: Michoacán
municipio: Cuajinillo
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '20'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Medio Ambiente: Naturaleza, ecología, desarrollo sustentable'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- Agua
- pueblo
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Michoacán
sinopsis_espaniol: "La comunidad de Cuanajillo del Toro, ubicada a 20 minutos de la\r\ncapital
  del Estado de Michoacán, es un pequeño pueblo en donde la lucha por la obtención
  del agua es un odisea para todos los ciudadanos de la localidad. Cuanajillo tiene
  la esperanza de cavar un pozo y encontar agua.\t\t\t\t\t\t\t"
sinopsis_ingles: "The community of Cuanajillo del Toro, located 20 minutes from the\r\nMichoacan
  state capital, is a small town where the struggle for obtaining water is an odyssey
  for all citizens of the town. Cuanajillo hopes to dig a well and finding water.\t\t\t\t\t\t\t"
link_al_trailer: https://www.youtube.com/watch?v=-5FpY3CkzMM
link_para_visionado: https://www.youtube.com/watch?v=-5FpY3CkzMM
festivales:
- festival: Festival Internacional de Cine de Morelia
  año_de_edicion: IX
- festival: Cinema Planeta
  año_de_edicion: 4ta
- festival: Festival de Acapulco
  año_de_edicion: 7mo
- festival: RencontresInternationales Eau et Cinéma (RIEC)
  año_de_edicion: 3e
- festival: Encuentro Hispanoamericano de cine y video documental independiente "Voces
    contra el silencio"
  año_de_edicion: VII
premios_y_reconocimientos:
- reconocimiento: Mención Honorífica
  institución: RackFocus Tabasco
- reconocimiento: Premio del Público
  institución: RackFocus Tabasco
- reconocimiento: Mejor Obra Michoacana
  institución: Festival Internacional de Cine de Morelia
becas_y_apoyos:
- beca: FONCA
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Stefan Guzmán Sotelo
  produccion:
  - Santiago Ortiz Monasterio
  casa_productora:
  - Crea
  guion:
  - Stefan Guzmán Sotelo
  fotografia:
  - Jerry Barrera
  sonido:
  - Adrian Bolaños
  edicion:
  - Fatima Vega
  musica_original:
  - Lorena Aragon
directorxs:
- rol:
  - Director(a)
  nombre: "Stefan Guzmán Sotelo\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1990-09-04'
  telefono_fijo:
  - '4431469152'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '0000000000'
  telefono_celular_publico: 'false'
  correo_electronico: sgs_050@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Santiago Ortiz Monasterio\t\t\t\t\t"
  telefono: "(55) 4954-4301\t"
  correo_electronico: zantiago.monasterio@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 0-10 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: "Cuanajillo La Historia Sin Agua\t\t\t\t\t\t"
order: 34
---

