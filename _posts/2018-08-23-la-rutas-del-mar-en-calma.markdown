---
title: "La rutas del mar en calma\t\t\t\t\t"
poster: "/public/images/3a88714c-a924-43d2-8a9e-d4f392e66191.jpg"
stills:
- "/public/images/8cad40f0-87d5-4aa7-b578-ceba42ee6bf5.jpg"
- "/public/images/8cad40f0-87d5-4aa7-b578-ceba42ee6bf5.jpg"
- "/public/images/8cad40f0-87d5-4aa7-b578-ceba42ee6bf5.jpg"
- "/public/images/8cad40f0-87d5-4aa7-b578-ceba42ee6bf5.jpg"
pais: México
estado: Guerrero
municipio: Acapulco
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '120'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Ninguno
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Arte: Artes escénicas, plásticas, visuales, multimedia'
palabras_clave:
- Teatro
- Artes Plásticas
- Acapulco
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Guerrero
sinopsis_espaniol: "Este es un viaje por las facetas del sol acapulqueño, en su definitivo
  peso dentro de la plástica en el puerto de Acapulco y la posibilidad de recrearlo
  mediante las imágenes en movimiento. Los creadores plásticos nos hablan de su
  labor artística desde Acapulco, como personas formadas en esta realidad y constructores
  de los símbolos que lo definen.\"\t\t\t\t\t\t\t\r\n"
sinopsis_ingles: "This is a journey through the facets of acapulqueño sun, in the
  end its weight in the plastic at the port of Acapulco and the Ability to recreate
  using moving images. \r\nPlastics Talk About Their artistic creators work from Acapulco,
  as people trained In this reality and builders of the symbols That defines it.\r\n\"\t\t\t\t\t\t\t\r\nLiga
  a Trailer:\t\t\t\t\t\t\t\r\nFestivales en los que ha participado la obra:\t\t\t\t\t\t\t\r\n"
link_al_trailer: https://www.youtube.com/watch?v=7z277Ium6t8
link_para_visionado: https://www.youtube.com/watch?v=7z277Ium6t8
festivales:
- festival: Muestra Estatal de teatro Acapulco
  año_de_edicion: '2013'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Francisco Saucedo
  produccion:
  - GabrIel Brito Camacho
  guion:
  - Francisco Saucedo
  investigacion:
  - Francisco Saucedo
  fotografia:
  - Roberto Buenfil
  - Gerardo Trani
  - Elizabeth Osorio
  sonido:
  - Roberto Buenfil
  edicion:
  - Roberto Buenfil
  musica_original:
  - Acid Victim
  otros:
  - nombre: Acid Victim
directorxs:
- rol:
  - Productor(a)
  nombre: "Gabriel Brito Camacho\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1970-01-01'
  telefono_fijo:
  - '7441243481'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '7441243181'
  telefono_celular_publico: 'false'
  correo_electronico: sanfcomx@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: "Francisco Saucedo Navarrete\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1972-06-08'
  telefono_fijo:
  - '7441243481'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '7441243181'
  telefono_celular_publico: 'false'
  correo_electronico: sanfcomx@gmail.com
  correo_electronico_publico: 'false'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Francisco Saucedo Navarrete\t\t\t\t\t"
  telefono: "7441243481\t"
  correo_electronico: sanfcomx@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: "La rutas del mar en calma\t\t\t\t\t"
order: 67
---

