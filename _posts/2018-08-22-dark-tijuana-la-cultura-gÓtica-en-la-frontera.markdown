---
title: Dark Tijuana, La cultura gótica en la frontera
poster: "/public/images/7a6f97da-3399-4459-b0e2-1b627b167892.jpg"
stills:
- "/public/images/9cfaa1e7-bdab-4a51-b076-33285b707dc7.jpg"
- "/public/images/774c00f8-b30f-47aa-8186-b1a5305c4f5e.jpg"
- "/public/images/e9ff8220-eacb-4854-86e7-c8d053f94467.jpg"
pais: México
estado: Baja California Norte
municipio: Tijuana
año_de_inicio_de_la_produccion: '2012'
año: '2012'
duracion: '68'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- Identidad
- Comunidad Gótica
- Manifestaciones artísticas
genero_y_estilo:
- 'Experimental / Poético: Apuesta por el uso poético del lenguaje o la deconstrucción
  del mismo para proponer nuevas formas fílmicas'
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Baja California Norte
sinopsis_espaniol: "Documental que presenta a la comunidad gótica tijuanense, mostrándonos
  eventos y actividades relacionadas con las manifestaciones artísticas de la ciudad.
  Músicos, artistas visuales, coreógrafos, diseñadores y promotores que se identifican
  con un estilo y una forma distinta de expresión, tienen el interés de desmitificar
  al dark como un individuo marginal.\t\t\t\t\t\t\r\n"
sinopsis_ingles: "Documentary featuring the Goth community in Tijuana, showing events
  and related art forms of city activities. Musicians, visual artists, choreographers,
  designers and developers who identify with a style and a different form of expression,
  have an interest in demystifying the \"\"dark\"\" as a marginal individual.\t\t\t\t\t\t\t"
link_al_trailer: https://www.youtube.com/watch?v=mlsRQFwLO5I
link_para_visionado: https://www.youtube.com/watch?v=mlsRQFwLO5I
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Marco Antonio Espinoza Higuera
  produccion:
  - Marco Antonio Espinoza Higuera
  casa_productora:
  - Proyecto Norte Cine
  guion:
  - Marco Antonio Espinoza Higuera
  investigacion:
  - Marco Antonio Espinoza Higuera
  fotografia:
  - Iván Gómez
  - Carlos Téllez
  - Christian Flores
  - Carlos Quezada
  sonido:
  - Irving López Báez
  edicion:
  - Marco Antonio Espinoza
  - Jonathan Levi
  musica_original:
  - Abraham Salgado
  otros:
  - nombre: Marco Antonio Espinoza
    rol: 'Postproducción de Sonido:'
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Marco Antonio Espinoza Higuera\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1967-07-14'
  telefono_fijo:
  - 01-664-969-80-71
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 664- 348- 68-96
  telefono_celular_publico: 'false'
  correo_electronico: espinoza.marcoantonio@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Marco Antonio Espinoza Higuera\t\t\t\t\t"
  telefono: "664-348-68-96\t\t"
  correo_electronico: espinoza.marcoantonio@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Plataformas digitales gratuitas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: Más de 10 millones de pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Dark Tijuana, La cultura gótica en la frontera
order: 38
---

