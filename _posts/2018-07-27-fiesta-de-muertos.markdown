---
title: Fiesta de Muertos
poster: "/public/images/9db2670b-0beb-4e4d-ae7f-f058079223ee.jpg"
stills:
- "/public/images/6a9eec40-382d-408f-a719-332f49178931.jpg"
- "/public/images/1c49f9ed-40f2-4b3f-a685-d852edc10772.jpg"
- "/public/images/46cccdbb-5114-45c6-b7b5-580b5e126a24.jpg"
pais: México
estado: Querétaro
municipio: Querétaro
año_de_inicio_de_la_produccion: '2011'
año: '2011'
duracion: '29'
idioma_original: Otomí
idiomas_de_los_subtitulos:
- Español
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
  - 'Independiente: Producido por una casa productora independiente constituida o no legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Cultura: Tradiciones, patrimonio'
palabras_clave:
- Indígenas
- Danzas
- Altares
- Rituales
- Ofrendas
genero_y_estilo:
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Querétaro
sinopsis_espaniol: 'Narración y recuento de la fiesta de muertos celebrada en la
  comunidad de San Ildefonso Tultepec, Amealco en Querétaro, que abarca el 31 de octubre,
  el 1ro y 2 de noviembre. El 31 en la noche llegan los muertos angelitos (niños y
  niñas), el 1º en la noche llegan los muertos adultos. La comunidad celebra con sus
  altares de muertos en sus casas y los rituales en el templo del pueblo, donde se
  llevan al cabo rezos, ofrendas y cantos y danzas. '
link_para_visionado: https://www.youtube.com/watch?v=dPVNnt_pmkg
festivales:
- festival: Festival de cine indígena de Querétaro
  año_de_edicion: '2011'
- festival: Festival Internacional de Cine y Video Indígena de Puebla
  año_de_edicion: '2011'
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Miguel Alfredo Rosales Vázquez
  produccion:
  - Trinidad Nava Vega
  guion:
  - Miguel Alfredo Rosales Vázquez
  investigacion:
  - Miguel Alfredo Rosales Vázquez
  fotografia:
  - Raymundo Aragón
  - Emilio Ramos
  sonido:
  - Axxel Castro
  edicion:
  - Antonio Jiménez
  - Miguel Alfredo Rosales Vázquez
  musica_original:
  - Trío Musical Colmillo Huasteco
directorxs:
- rol:
  - Productor(a)
  nombre: Trinidad Nava Vega
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - '4424142491'
  telefono_fijo_publico: 'false'
  telefono_celular:
  - '4481112241'
  telefono_celular_publico: 'false'
  correo_electronico: trinidad.nava@decora.org.mx
  correo_electronico_publico: 'false'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: '4421355457'
  telefono_publico: 'false'
  correo_electronico: miguelrosales72@gmail.com
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Miguel Alfredo Rosales Vázquez
  telefono: '4421447778'
  correo_electronico: miguelrosales72@gmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: Ciudad de México
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 0 a mil pesos
layout: ficha_documental
titulo_en_ingles: Fiesta de Muertos
order: 4
---

