---
title: Acuérdate de Acapulco
poster: "/public/images/ef047dd7-04b3-4ef0-9ce7-e56db4da6456.jpg"
stills:
- "/public/images/c6d4dd31-8bf0-43b1-881c-69155b008152.jpg"
sitio_web_de_la_pelicula: http://acuerdatedeacapulco-film.com/
pais: México
estado: Ciudad de México
municipio: Coyoacán
año_de_inicio_de_la_produccion: '2013'
año: '2014'
duracion: '82'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
- Francés
formatos:
- DVD
- Archivo digital
- Blu Ray
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
- 'Territorio: Procesos correspondientes a la organización territorial de una población.'
palabras_clave:
- Violencia
- Acapulco
- semana santa
- Corrupción
- nostalgia
- Fama
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Found footage / Reapropiación: Utiliza material de archivo y lo resignifica para
  contar una nueva historia'
- 'De investigación: El hilo conductor se sustenta en la investigación de un tema.'
locaciones:
  paises:
  - México
  estados:
  - Guerrero
sinopsis_espaniol: Una diva cautivante, encarnación de Acapulco, se acuerda de cuando
  era la estrella mas famosa del cine mexicano. Mientras tanto, Marco, Javier, Félix,
  Alfonso y Yolanda se preparan para la Semana Santa, momento en el cual la ciudad
  revive, recuperando por unos días su fama perdida. Cada uno, a su manera, intenta
  sobrevivir en este decorado paradisíaco que diario se aparenta más al infierno.
sinopsis_ingles: A captivating diva, incarnation of Acapulco, remembers when she was
  the most famous movie star of Mexico. Meanwhile, Marco, Javier, Félix, Alfonso and
  Yolanda are getting ready for Easter Week, the period when the city revives, recovering
  for a few days its past glory. Each one tries to survive his own way in that paradisiacal
  set that everyday looks more like hell.
link_al_trailer: https://vimeo.com/71734778
link_para_visionado: https://vimeo.com/72954846
contrasena_para_visionado: ACA2013
festivales:
- festival: Festival Internacional de Cine de Puebla
  año_de_edicion: '4ta Edición, 2013 '
- festival: Festival de la Memoria
  año_de_edicion: 7ma Edición, 2013
- festival: Festival Internacional de Cine Acapulco
  año_de_edicion: 9na Edición, 2014
- festival: Contra el Silencio de todas las Voces
  año_de_edicion: VIII Encuentro, 2014
premios_y_reconocimientos:
- reconocimiento: Mención Especial
  institución: Festival de la Memoria
- reconocimiento: Mención Honorífica
  institución: Contra el Silencio Todas las Voces
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Ludovic Bonleux
  produccion:
  - Ludovic Bonleux
  casa_productora:
  - Capdalprod
  - Terra Nostra Films
  guion:
  - Ludovic Bonleux
  investigacion:
  - Ludovic Bonleux
  fotografia:
  - Ludovic Bonleux
  - Julio César Hernández
  sonido:
  - Ludovic Bonleux
  edicion:
  - Ludovic Bonleux
  - Nicolas Défossé
  musica_original:
  - Manuel Danoy
  - Lost Acapulco
  otros:
  - nombre: Manuel Danoy
    rol: Postproducción de sonido
  - nombre: Jose Luis Arroyo
    rol: Animación
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: Ludovic Bonleux
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1974-06-29'
  telefono_fijo:
  - 04455 2300 3491
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 04455 2300 3491
  telefono_celular_publico: 'false'
  correo_electronico: ludovic@capdalprod.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ludovic Bonleux
  nombre_publico: 'true'
  telefono: 04455 2300 3491
  telefono_publico: 'false'
  correo_electronico: ludovic@capdalprod.com
  correo_electronico_publico: 'true'
  sitio_web: http://acuerdatedeacapulco-film.com/
  sitio_web_publico: 'true'
- nombre: CINEMA Estudio
  nombre_publico: 'true'
  telefono: 967-6317671
  telefono_publico: ''
  correo_electronico: contacto@terranostrafilms.com
  correo_electronico_publico: 'true'
  sitio_web: http://terranostrafilms.com/
  sitio_web_publico: 'true'
datos_de_contacto:
- nombre: Ludovic Bonleux
  telefono: 04455 2300 3491
  correo_electronico: ludovic@capdalprod.com
  sitio_web: http://terranostrafilms.com/
ventanas_donde_se_ha_exhibido:
- Salas de cine culturales y/o independientes
- Centros culturales y cineclubes
- Festivales / Muestras mexicanas
- Escuelas y universidades
licencia_de_uso: 'No'
restricciones_de_distribucion: Contactar al productor para revisar cada caso (se puede
  programar en ciertos casos)
financiacion: 500 mil-1 millón de pesos
coproductores:
- coproductor: Terra Nostra Films
  pais: México
  estado: Chiapas
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 50-100 mil pesos
layout: ficha_documental
titulo_en_ingles: Acuérdate de Acapulco
order: 70
---

