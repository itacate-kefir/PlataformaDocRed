---
title: Viviendo a gritos
poster: "/public/images/8648fbf3-d496-4f5d-a917-b7b083db17c5.jpg"
stills:
- "/public/images/f826676a-7a8c-41ce-9f18-1194ce62db42.png"
- "/public/images/f904b737-4f41-444f-8eea-cf582041d1cb.png"
- "/public/images/3687f137-c6ba-4f39-91db-7a0a5a679722.jpg"
pais: México
estado: Tlaxcala
municipio: Tlaxcala
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '21'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Inglés
formatos:
- DVD
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
- 'Institucional: Producido por una institución pública o privada'
tematicas:
- 'Vida cotidiana y sociedad: Relaciones sociales, laborales, familiares, espacio
  público y privado'
palabras_clave:
- vocero
- grito
- calle
genero_y_estilo:
- 'Observacional: Documentales que observan el mundo con la mínima intervención del
  cineasta'
locaciones:
  paises:
  - México
  estados:
  - tlaxcala
sinopsis_espaniol: "“Súbale La Joya, Loma Bonita, Santa Ana”, “allá viene la central”…
  gritos que representan una calle y que marcan el modo de vida de “el Satélite” \t\t\t\t\t\t\t"
link_al_trailer: "https://vimeo.com/76548927\t\t\t\t\t\t"
link_para_visionado: "https://vimeo.com/76548927\t\t\t\t\t\t"
festivales:
- festival: Festival de Cine Online Global Zoom en Argentina
  año_de_edicion: 2° edición
- festival: festival cineMA México-Alemania en San Luis Potosí
  año_de_edicion: 2° edición
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Jesús Valdés Lima
  produccion:
  - Instituto Tlaxcalteca de la Cultura
  guion:
  - Jesús Valdés Lima
  fotografia:
  - Maximiliano Valdes Lima / Erick Ramírez Soto
  sonido:
  - Rene López Caballero / Ivan Flores Hernández
  edicion:
  - Jesús Valdés Lima
  musica_original:
  - Moby L’Homme-Yvan Arnaud / Una ciudad de holograma- Carlos de Alcausin
directorxs:
- rol:
  - Productor(a)
  - Director(a)
  nombre: "Jesús Valdés Lima\t\t\t\t\t"
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1989-07-16'
  telefono_fijo:
  - 01 246 46 60363
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 045 222 4843 882
  telefono_celular_publico: 'false'
  correo_electronico: valdes89_americ@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: "Jesús Valdés Lima\t\t\t\t\t"
  telefono: "01 246 46 60363\t"
  correo_electronico: valdes89_americ@hotmail.com
  sitio_web: ''
ventanas_donde_se_ha_exhibido:
- Festivales / Muestras mexicanas
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 10-30 mil pesos
coproductores:
- coproductor: ''
  pais: México
  estado: tlaxcala
porcentaje_de_financiacion:
  estatal: '30'
  privada: '0'
  propia: '70'
recaudacion: De 1-3 mil pesos
layout: ficha_documental
titulo_en_ingles: Viviendo a gritos
order: 24
---

