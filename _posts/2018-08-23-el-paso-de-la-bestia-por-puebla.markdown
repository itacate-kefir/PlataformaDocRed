---
title: El paso de la bestia por Puebla
poster: "/public/images/cbdadfae-fc64-4a56-ad89-a65ac41d0948.jpg"
stills:
- "/public/images/92e6247b-97ce-47cf-adfb-5591f0bba07f.jpg"
- "/public/images/92e6247b-97ce-47cf-adfb-5591f0bba07f.jpg"
- "/public/images/92e6247b-97ce-47cf-adfb-5591f0bba07f.jpg"
pais: México
estado: Puebla
municipio: Puebla
año_de_inicio_de_la_produccion: '2013'
año: '2013'
duracion: '40'
idioma_original:
- Español
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
- MiniDV
- 16mm
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Pueblos indígenas: Pueblos originarios de México, sus tradiciones, lengua, saberes,
  situación actual'
- 'Derechos humanos: Luchas o debates sobre los derechos humanos'
palabras_clave:
- Puebla
- Migrantes
- tren
- La bestia
- Madres
- Desaparecidos
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
- 'Etnográfico: Descripción fílmica del comportamiento humano.'
locaciones:
  paises:
  - México
  estados:
  - Puebla
sinopsis_espaniol: Un grupo de madres centro americanas, se embarcan en una caravana
  en busca de sus hijos migrantes desaparecidos en México. En Puebla, una de las ciudades
  visitadas, dan voz a la indignación que les dejo el pasar de la Bestia (Tren de
  carga que cruza el territorio Mexicano). Denunciando a través de diversos testigos,
  la impunidad y corrupción de la que son víctimas.
sinopsis_ingles: A group of  Center American mothers, embark on a caravan in search
  of their missing  migrant sons in Mexico. At Puebla, one of the cities visited,
  give voice to the indignation that  leave the passing of the Beast (Freight train
  crossing the Mexican territory). Denouncing through various witnesses, impunity
  and corruption of the victims.
link_para_visionado: https://ww.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: ''
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Guillermo Morales Alfonso
  - Alberto Gomez Soriano
  produccion:
  - Cefoc A.C
  casa_productora:
  - Cefoc A.C
  guion:
  - Guillermo Morales Alfonso
  investigacion:
  - Cefoc A.C
  fotografia:
  - Guillermo Morales Alfonso
  - Alberto Gomez Soriano
  sonido:
  - Guillermo Morales Alfonso
  edicion:
  - Guillermo Morales Alfonso
  - Giovanna Flores Monterrosas
  musica_original:
  - FMA (Free archive music)
directorxs:
- rol:
  - Director(a)
  nombre: Guillermo Morales Alfonso
  nacionalidad: Mexicana
  fecha_de_nacimiento: '1989-02-14'
  telefono_fijo:
  - 222 41 01 91
  telefono_fijo_publico: 'false'
  telefono_celular:
  - 044 22 25 32 98 92
  telefono_celular_publico: 'false'
  correo_electronico: memo.14.02@hotmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Guillermo Morales Alfonso
  telefono: 044 22 25 32 98 92
  correo_electronico: memo.14.02@hotmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 30-50 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '100'
  propia: '0'
recaudacion: De 3-5 mil pesos
layout: ficha_documental
titulo_en_ingles: El paso de la bestia por Puebla
order: 53
---

