---
title: C.A.C.I.T.A, pedaleando hacia la autonomía
poster: "/public/images/9236ac0d-8f9c-4f1c-9ec0-f39421f934c5.jpg"
stills:
- "/public/images/178400ad-8c6b-438e-a5d6-342f92b4d5db.jpg"
- "/public/images/5e10b3cb-01e5-492b-bb7f-3234cf760c8a.jpg"
pais: México
estado: Oaxaca
municipio: Oaxaca
año_de_inicio_de_la_produccion: '2010'
año: '2010'
duracion: '9'
idioma_original:
- Castellano
idiomas_de_los_subtitulos:
- Sin subtítulos
formatos:
- Archivo digital
tipo_de_produccion:
- 'Independiente: Producido por una casa productora independiente constituida o no
  legalmente'
tematicas:
- 'Movimientos sociales y organización ciudadana: Agrupaciones sociales que buscan
  generar un cambio social'
palabras_clave:
- trabajo
- Autónomo
- Tecnologías
- Bienestar
genero_y_estilo:
- 'Expositivo / Descriptivo: La narrativa enfatiza la exposición de un tema o problemática.'
locaciones:
  paises:
  - México
  estados:
  - Oaxaca
sinopsis_espaniol: El Centro Autónomo para la Creación Intercultural de Tecnologías
  Apropiadas, es un espacio de trabajo y actitud abierto desde el 2004 donde se realizan
  distintos prototipos de bici máquinas, vistas desde una perspectiva política y que
  pueden generar un bienestar social y económico.
sinopsis_ingles: The Centro Autónomo para la Creación Intercultural de Tecnologías
  Apropiadas is a place of work and attitude open since 2004, where many bici machines
  are made, seen from a political perspective, where they can make a social and economical
  well-being.
link_para_visionado: https://www.youtube.com
festivales:
- festival: Ninguno
  año_de_edicion: Ninguno
premios_y_reconocimientos:
- reconocimiento: Ninguna
  institución: Ninguna
becas_y_apoyos:
- beca: Ninguno
  monto: ''
  institución: Ninguna
  tipo: Privada
equipo_de_produccion:
  direccion:
  - Ezequiel Sanchez
  produccion:
  - Mónica Parra
  guion:
  - Federico Denegri
  investigacion:
  - Proyecto Chakana
  fotografia:
  - Federico Barra
  sonido:
  - Federico Barra
  edicion:
  - Ezequiel Sanchez
  - Federico Denegri
directorxs:
- rol:
  - Productor(a)
  nombre: Mónica Parra
  nacionalidad: Mexicana
  fecha_de_nacimiento: ''
  telefono_fijo:
  - "+52 1 222 504 9982"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+52 1 222 504 9982"
  telefono_celular_publico: 'false'
  correo_electronico: mparrah17@gmail.com
  correo_electronico_publico: 'true'
- rol:
  - Director(a)
  nombre: Ezequiel Sanchez
  nacionalidad: Argentina
  fecha_de_nacimiento: ''
  telefono_fijo:
  - "+54 9 11 4087 1121"
  telefono_fijo_publico: 'false'
  telefono_celular:
  - "+54 9 11 4087 1121"
  telefono_celular_publico: 'false'
  correo_electronico: ezequielsanchez@gmail.com
  correo_electronico_publico: 'true'
distribuidoras:
- nombre: Ninguna
  nombre_publico: 'false'
  telefono: ''
  telefono_publico: 'false'
  correo_electronico: ''
  correo_electronico_publico: 'false'
  sitio_web: ''
  sitio_web_publico: 'false'
datos_de_contacto:
- nombre: Mónica Parra
  telefono: "+52 1 222 504 9982"
  correo_electronico: mparrah17@gmail.com
  sitio_web: ''
licencia_de_uso: 'No'
restricciones_de_distribucion: 'Ninguna restricción: se puede programar, siempre y
  cuando se dé aviso al productor o director'
financiacion: 50-100 mil pesos
coproductores:
- coproductor: ''
  pais: Ninguno
  estado: Ninguno
porcentaje_de_financiacion:
  estatal: '0'
  privada: '0'
  propia: '100'
recaudacion: De 5-10 mil pesos
layout: ficha_documental
titulo_en_ingles: C.A.C.I.T.A, pedaleando hacia la autonomía
order: 23
---

